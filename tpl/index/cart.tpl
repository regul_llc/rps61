{include file="default/header.tpl"}

{include file="default/top.tpl"}

	<div id="content">
		<div class="container">
			<div class="layout-2-column">
			
				{include file="default/left_column.tpl"}
			
				<div class="column center">
					<div class="main-text">	
						<h1>{$strPageTitle}</h1>
						<br>
							<form method="POST" action="">
						{if $aCart|@count > 0}
								<table width="100%">
								<tr>
								<th align="left">Наименование</th>
								<th align="left">Цена</th>
								<th align="left">Кол-во</th>
								<th align="left">Сумма</th>
								<td></td>
								</tr>
							
							{$nTotalSumm=0}
							{$nTotalAmunt=0}
							{foreach from=$aCart item=item}
								<tr>
								<td valign="top" style="padding-top: 10px;">
									<img src="/public/img/{if !empty($item.good.cover)}upload/small/{$item.good.cover}{else}no_image_cart.jpg{/if}" style="float: left; margin-right: 5px;">
								<b style="margin: 0;">{$item.good.title}</b><br><small>{$item.good.company} {$item.good.model}</small></td>
								<td>{$item.good.price|number_format:0:" ":" "} руб.</td>
								<td><input style="width: 50px; text-align: center;" type="text" name="cart[{$item.id}]" value="{$item.amount}"></td>
								<td>{($item.good.price * $item.amount)|number_format:0:" ":" "} руб.</td>
								<td><a href="/cart/del/{$item.id}">удалить</a></td>
								<!-- {$nTotalSumm = $nTotalSumm + $item.good.price * $item.amount} -->
								<!-- {$nTotalAmunt = $nTotalAmunt + $item.amount} -->
								</tr>
									
							{/foreach}			
							
								<tr><td colspan="5" style="height: 30px;"></tr>
							
								<tr>
								<td colspan="2" align="left"><b>Всего:</b></td>
								<td>{$nTotalAmunt}</td>
								<td>{$nTotalSumm|number_format:0:" ":" "} руб.</td>
								<td></td>
								</tr>
								
								<tr><td colspan="5" style="height: 30px;"></tr>
								
								<tr>
                                                                <td colspan="5" align="right">
                                                                        <input type="submit" name="recount"  value="Пересчитать" />
                                                                        <input type="submit" name="order"  value="Оформить заказ" />
								</td>
								</tr>
								
								</table>		
							
							</form>
						{else if}	
							<h4 align="center">Корзина пуста</h4>
						{/if}
					</div>					
				</div>
			</div>
		</div>
	</div>
	
	
{include file="default/footer.tpl"}