{include file="default/header.tpl"}

{include file="default/top.tpl"}

	<div id="content">
		<div class="container">
			<div class="layout-2-column">
			
				{include file="default/left_column.tpl"}
				
				<div class="column center">
					<div class="main-text">
						{$strMainTextTop}
					</div>
					{*<div class="main-image">
						{foreach from=$aGallery item=item}
							<img src="/public/img/upload/main/{$item.file_name}" alt="" width="748" height="329">
						{/foreach}
					</div>*}
					<div class="shop number-1">
						<div class="slider-container">
							{foreach from=$aRandGoods item=item}
								<div class="product product--modified-height">
									<div class="header">
										<p class="name"><a href="/good/{$item.id}">{$item.title}</a></p>
										<p class="model"><a href="/good/{$item.id}">{if $item.brand}{$item.brand} {/if} {$item.model}</a></p>
									</div>
									<div class="image-block">
										<a href="/good/{$item.id}">
									
										{if $item.cover}
											<img src="/public/img/upload/list/{$item.cover}" alt="{$item.title} {$item.company} {$item.model}" width="216" height="155">
										{else}
											<img src="/public/img/no_image_small.jpg" alt="{$item.title} {$item.company} {$item.model}" width="216" height="155">
										{/if}
											
										</a>
									</div>
									<p class="price">{if $item.price}{$item.price|number_format:0:" ":" "} руб.{else}цена по запросу{/if} </p>
									{if !$item.existence || !$item.price}<div class="product-label product-label--modified-bg"></div>{else}<div class="product-label"></div>{/if}
									{if !empty($item.action_label)}<div class="product-label-{$item.action_label}"></div>{/if}
									<a good_id="{$item.id}" href="#" class="link-buy buy">{if !$item.price}заказать{else}купить{/if}</a>
								</div>
							{/foreach}							
						</div>
						<a class="prev" href="#"><span>prev</span></a>
						<a class="next" href="#"><span>next</span></a>
					</div>
                                        <div class="notPubOff">*информация о ценах и наличии товара не является офертой.</div>
					<div class="brands">
						{foreach from=$aBanners item=item}
							<div class="brand">
								<h2>{$item.title}</h2>
								{if !empty($item.url)}<a target="_blank" href="{$item.url}">{/if}<img src="/public/img/banners/{$item.image}" alt="{$item.title}" width="232" height="77">{if !empty($item.url)}</a>{/if}
							</div>
						{/foreach}						
					</div>
					<div class="more-products">
						{foreach from=$aLastWorks item=item}
							<div class="product-block">
								<h2>{$item.title}</h2>
								<a class="link-more pull-right" href="/works/{$item.id}"><img src="/public/img/upload/pwork/{$item.cover}" alt="{$item.title}" width="318" height="188"></a>
								<a class="link-more pull-right" href="/works/{$item.id}">подробнее ...</a>
							</div>
						{/foreach}
					</div>
					<div class="main-text">
						<p>{$strMainTextBottom}</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
{include file="default/footer.tpl"}