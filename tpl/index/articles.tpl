{include file="default/header.tpl"}

{include file="default/top.tpl"}

	<div id="content">
		<div class="container">
			<div class="layout-2-column">
			
				{include file="default/left_column.tpl"}
			
				<div class="column center">
					<div class="main-text">	
						{if isset($aArticles)}
							<h1>{$strPageTitle}</h1>
							<br>
                                                        <ul>
                                                            {foreach from=$aArticles item=item}
                                                                <li>
                                                                    <h4><a href="/articles/{$item.id}">{$item.title}</a></h4>
                                                                </li>
                                                            {/foreach}
                                                        </ul>
						{elseif isset($aArticle)}
							<h2>{$aArticle.title}</h2>
							{$aArticle.text}
                                                        
                                                        <div class="more-products mt30">
                                                            {if !empty($aImages)}
                                                                {foreach $aImages as $img}
                                                                    <div class="picgallBox">
                                                                        <a href="/public/img/upload/{$img.file_name}" rel="articleGroup" title="{$img.title}">
                                                                            <img src="/public/img/upload/list/{$img.file_name}" />
                                                                        </a>
                                                                            <div class="label"><b>{$img.title}</b></div>
                                                                    </div>
                                                                {/foreach}
                                                            {/if}
                                                            
                                                            {if $aArticle.additFiles}
                                                                <div id="goodSpecFiles" class="nfloat">
                                                                    <h3>Дополнительные материалы</h3>
                                                                    <ul>
                                                                        {foreach $aArticle.additFiles as $file}
                                                                            <li>
                                                                                <p><a href="/articles/{$aArticle.id}/{$file.filename}"><img width="32" height="32" src="/public/img/icon_{$file.icon}.png" />{if !empty($file.name)}{$file.name}{else}{$file.filename}{/if} (скачать)</a></p>
                                                                            </li>
                                                                        {/foreach}
                                                                    </ul>
                                                                </div>
                                                            {/if}
                                                        </div>
                                                        
							<br>
							<br>
							<a href="/articles">назад</a>
						{/if}
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
{include file="default/footer.tpl"}