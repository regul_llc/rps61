{*include file="default/header.tpl"}

{include file="default/top.tpl"}

	<div id="content">
		<div class="container">
			<div class="layout-2-column">
			
				{include file="default/left_column.tpl"}
			
				<div class="column center">
					<div class="main-text">	
						<h1>{$strPageTitle}</h1>
						{$strPageText}
					</div>					
				</div>
			</div>
		</div>
	</div>
	
	
{include file="default/footer.tpl"*}






{include file="default/header.tpl"}

{include file="default/top.tpl"}

	<div id="content">
		<div class="container">
			<div class="layout-2-column">
			
				{include file="default/left_column.tpl"}
				
				<div class="column center">
				{if isset($aActions)}
				
					<div class="caption">
						<h1>{$strPageTitle}</h1>
					</div>
					
					<div class="more-products">
						{foreach from=$aActions item=item}
							<div>
								{if $item.image}
									<img style="float: left; margin: 0 10px 10px 0;" src="/public/img/actions/preview/{$item.image}">
								{/if}
								
								<b>{$item.title}</b><br>
								<i>{$item.date|date_format:"%Y.%m.%d"}</i>
								{$item.description}<br><br>
								<a href="/actions/{$item.id}">подробнее</a>
							</div>
						{/foreach}
					</div>		
					
				{/if}		
				
				{if isset($aAction)}
				
					<div class="caption">
						<h2>{$aAction.title}</h2>
					</div>
					
					<div class="more-products">
						<i>{$aAction.date|date_format:"%Y.%m.%d"}</i><br>
						{if $aAction.image}
							<img style="float: left; margin: 0 10px 10px 0;" src="/public/img/actions/{$aAction.image}">
						{/if}
						
						{$aAction.text}
					</div>		
					
				{/if}			
				</div>
			</div>
		</div>
	</div>
	
	
{include file="default/footer.tpl"}