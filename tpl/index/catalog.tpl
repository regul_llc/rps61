{include file="default/header.tpl"}

{include file="default/top.tpl"}

	<div id="content">
		<div class="container">
			<div class="layout-2-column">
			
				{include file="default/left_column.tpl"}
				
				<div class="column center">
					<div class="caption">
						<h1>{$aCategory.title}: <span>{$aSubCategory.title}</span></h1>
					</div>				
					
					{if $aSubCategory.about}
                        {if isset($smarty.get.page) && $smarty.get.page==1}
                            <div class="shop number-2 modified-no-border">
                                    {$aSubCategory.about}
                            </div>
                        {/if}
					{/if}

					{include file="default/filters.tpl"}
                                        
					<div class="shop number-2 modified-no-border">
					
						{foreach from=$aGoods item=item}
						
							<div class="product product--modified-height">
								<div class="header">
									<p class="name"><a href="/good/{$item.id}">{$item.title}</a></p>
									<p class="model"><a href="/good/{$item.id}">{if $item.brand}{$item.brand} {/if}{$item.model}</a></p>
								</div>
								<div class="image-block">
									<a href="/good/{$item.id}">
									
									{if $item.cover}
										<img src="/public/img/upload/list/{$item.cover}" alt="{$item.title} {$item.company} {$item.model}" width="216" height="155">
									{else}
										<img src="/public/img/no_image_small.jpg" alt="{$item.title} {$item.company} {$item.model}" width="216" height="155">
									{/if}
										
									</a>
									
								</div>
								<p class="price">{if $item.price}{$item.price|number_format:0:" ":" "} руб.{else}цена по запросу{/if}</p>
								{if !$item.existence || !$item.price}<div class="product-label product-label--modified-bg"></div>{else}<div class="product-label"></div>{/if}
								{if !empty($item.action_label)}<div class="product-label-{$item.action_label}"></div>{/if}
								<a good_id="{$item.id}" href="#" class="link-buy buy">{if !$item.price}заказать{else}купить{/if}</a>
							</div>
							
						{/foreach}
						
                                                
					</div>
                                                
					{include file="default/pnavigator.tpl"}
                                        <div class="notPubOff">*информация о ценах и наличии товара не является офертой.</div>
				</div>
			</div>
		</div>
	</div>
	
	
{include file="default/footer.tpl"}