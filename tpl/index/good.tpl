{include file="default/header.tpl"}

{include file="default/top.tpl"}

	<div id="content">
		<div class="container">
			<div class="layout-2-column">
			
				{include file="default/left_column.tpl"}
				
				<div class="column center">
					<div class="product-description">
						<div class="product-slider column left">
							<h1>{$aGood.title}<br>
							{if $aGood.brand}{$aGood.brand} {/if}{$aGood.model}</h1>
							
							{if $aGallery|count > 0}
								<div class="gallery">
									{foreach from=$aGallery item=item}
										<img src="/public/img/upload/medium/{$item.file_name}" alt="{$item.title}" width="343" height="259">
									{/foreach}
								</div>
							{else}
								<div id="noImgGood" class="gallery">
									<img src="/public/img/no_image.jpg" alt="Товар">
								</div>
							{/if}
							{if !empty($aGood.action_label)}<div class="product-label-{$aGood.action_label} {if $aGallery|count > 1}gift-modified2{else}gift-modified{/if}"></div>{/if}
							
							{if $aGallery|count > 1}
								<div class="thumbnails-container">
									<div class="thumbnails"></div>
									<div class="pagination-left">
										<a href="#" class="js-no-action"><img src="/public/img/product-slider-left.png" alt="" width="19" height="40"></a>
									</div>
									<div class="pagination-right">
										<a href="#" class="js-no-action"><img src="/public/img/product-slider-right.png" alt="" width="19" height="40"></a>
									</div>
								</div>			
							{/if}						
											
						</div>
						<div class="product-info column right">
							<div class="buy-product-form-container">
								<form action="">
									<p class="price">{if $aGood.price}{$aGood.price|number_format:0:" ":" "} руб.{else}цена</br> по запросу{/if}</p>
									<p class="number">
										<span>кол-во</span>
										<input id="buy_count" type="text" value="1">
									</p>
									<p class="submit">
										<button good_id="{$aGood.id}" class="buy_wcount" type="submit"><span class="link-buy">{if !$aGood.price}заказать{else}купить{/if}</span></button>
									</p>
								</form>
							</div>
                                                        {if $aAttGoods }<a href="#slideAnchor">Сопутствующие товары</a>{/if}
							{$isIconChecked = false}
							{foreach $aGood.icons as $kicon => $icon}
								{if $icon.checked == 'checked' && !$isIconChecked}
								{$isIconChecked = true}
								<div class="iconGood">
								{/if}
									{if $icon.checked == 'checked'}<img src="/public/img/funcicons/{$icon.path}" title="{$icon.title}" />{/if}
								{if $aGood.icons|count == ($kicon + 1) && $isIconChecked}</div>{/if}
							{/foreach}
                                                        
							{if count($aValues)}
								<div class="tech-info">
									<p class="head">Технические характеристики:</p>
									{foreach from=$aValues item=item}
                                                                                <div class="row">
											<span class="right-part">{$item.value} {$item.units}</span>
											<span class="left-part">{$item.name}</span>
										</div>
									{/foreach}
									
								</div>
							{/if}
						</div>

						{$aGood.text}
                                                
                                                {if $aGood.additFiles}
                                                    <div id="goodSpecFiles">
                                                        <h3>Спецификации и инструкции</h3>
                                                        <ul>
                                                            {foreach $aGood.additFiles as $file}
                                                                <li>
                                                                    <p><a href="/good/{$aGood.id}/{$file.filename}"><img width="32" height="32" src="/public/img/icon_{$file.icon}.png" />{if !empty($file.name)}{$file.name}{else}{$file.filename}{/if} (скачать)</a></p>
                                                                </li>
                                                            {/foreach}
                                                        </ul>
                                                    </div>
                                                {/if}
					<div class="notPubOff" style="float:left;">*информация о ценах и наличии товара не является офертой.</div>	
					</div>
                                    {if $aAttGoods }
                                        
				<div class="shop number-1" id="slideAnchor">
                                    </br>
                                        <h3>Сопутствующие товары</h3>
                                        </br>
                                        </br>
						<div class="slider-container">
							{foreach from=$aAttGoods item=item}
								<div class="product product--modified-height" style="height:430px;">
									<div class="header">
										<p class="name"><a href="/good/{$item.id}">{$item.title}</a></p>
										<p class="model"><a href="/good/{$item.id}">{if $item.brand}{$item.brand} {/if} {$item.model}</a></p>
									</div>
									<div class="image-block">
										<a href="/good/{$item.id}">
									
										{if $item.cover}
											<img src="/public/img/upload/list/{$item.cover}" alt="{$item.title} {$item.company} {$item.model}" width="216" height="155">
										{else}
											<img src="/public/img/no_image_small.jpg" alt="{$item.title} {$item.company} {$item.model}" width="216" height="155">
										{/if}
											
										</a>
									</div>
									<p class="price">{if $item.price}{$item.price|number_format:0:" ":" "} руб.{else}цена по запросу{/if} </p>
									{if !$item.existence || !$item.price}<div class="product-label product-label--modified-bg"></div>{else}<div class="product-label"></div>{/if}
									{if !empty($item.action_label)}<div class="product-label-{$item.action_label}"></div>{/if}
									<a good_id="{$item.id}" href="#" class="link-buy buy">{if !$item.price}заказать{else}купить{/if}</a>
								</div>
							{/foreach}							
						</div>
						<a class="prev" href="#"><span>prev</span></a>
						<a class="next" href="#"><span>next</span></a>
					</div>	
					
				</div>
                                {/if}
			</div>
		</div>
	</div>
	
	
{include file="default/footer.tpl"}