{include file="default/header.tpl"}

{include file="default/top.tpl"}

	<div id="content">
		<div class="container">
			<div class="layout-2-column">
			
				{include file="default/left_column.tpl"}
				
				<div class="column center">
				{if isset($aNews)}
				
					<div class="caption">
						<h1>Новости</h1>
					</div>
					
					<div class="more-products">
						{foreach from=$aNews item=item}
							<div>
								{if $item.image}
									<img style="float: left; margin: 0 10px 10px 0;" src="/public/img/news/{$item.image}">
								{/if}
								
								<b>{$item.title}</b><br>
								<i>{$item.date|date_format:"%Y.%m.%d"}</i>
								{$item.text|truncate:"220"}<br><br>
								<a href="/news/{$item.id}">подробнее</a>
							</div>
						{/foreach}
					</div>		
					
				{/if}		
				
				{if isset($aNew)}
				
					<div class="caption">
						<h2>{$aNew.title}</h2>
					</div>
					
					<div class="more-products">
						<i>{$aNew.date|date_format:"%Y.%m.%d"}</i><br>
						{if $aNew.image}
							<img style="float: left; margin: 0 10px 10px 0;" src="/public/img/news/{$aNew.image}">
						{/if}
						
						{$aNew.text}
					</div>		
					
				{/if}			
				</div>
			</div>
		</div>
	</div>
	
	
{include file="default/footer.tpl"}