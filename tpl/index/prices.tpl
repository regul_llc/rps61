{include file="default/header.tpl"}

{include file="default/top.tpl"}

<div id="content">
		<div class="wrapper">
			<div class="column1">
				<div class="portfolio">
					<h1>Примеры работ</h1>
					<div class="pagination-up disabled">
						<a class="js-no-action" href="#"><img src="/public/img/pagination-up.jpg" alt="" width="42" height="12"></a>
					</div>
					<div class="work-container">
						<div class="work-moving-container">
							{foreach from=$aLastWorks item=item}
								<div class="work">
									<h3>{$item.title}</h3>
									<img src="/public/img/upload/thumbnail/{$item.cover}" alt="" width="219" height="146">
									<p class="text">{$item.text|strip_tags:false|truncate:150:'..'}</p>
									<p class="link"><a href="/works/{$item.id}">далее...</a></p>
								</div>
							{/foreach}
						</div>
					</div>
					<div class="pagination-down">
						<a class="js-no-action" href="#"><img src="/public/img/pagination-down.png" alt="" width="42" height="12"></a>
					</div>
				</div>
				<div class="links">
					<a class="order js-no-action" href="">
						<img src="/public/img/link-order.jpg" alt="" width="67" height="69">
						<p>On-line<br><span>заявка</span></p>
					</a>
					<a class="calculator js-no-action" href="">
						<img src="/public/img/link-calc.jpg" alt="" width="67" height="69">						
						<p>On-line<br><span>калькулятор</span></p>
					</a>
				</div>
			</div>
			<div class="column2">
				<div class="text">
					<h1>{$strPageTitle}</h1>
					{$strPageText}
				</div>
				<div class="sub-info">
					<form action="" name="price_theForm">
						<h2>Калькулятор</h2>
						<p><label style="display: table-row;" for="price_type">Выберите фактуру:<span></span></label></p>
						<p style="margin-top: 5px;">
							<select id="price_type" onchange="price_calc()">
								<option value="0">Выбрать</option>
								<option value="350">Россия</option>
								<option value="400">Бельгия (Бесшовные)</option>
								<option value="500">Германия 1.8 м</option>
								<option value="650">Германия 2.7 м</option>
							</select>
						</p>
						<p><label style="display: table-row;" for="price_square">Площадь:<span></span></label></p>
						<p style="margin-top: 5px;"><input class="digit" onchange="price_calc()" id="price_square" type="text" value="3"></p>
						<p><label style="display: table-row;" for="price_lustraType">Выберите тип люстры:<span></span></label></p>
						<p style="margin-top: 5px;">
							<select id="price_lustraType" onchange="price_calc()">
								<option value="0">Выбрать</option>
								<option value="350">Крючковая</option>
								<option value="500">Планочная или потолочная</option>
							</select>
						</p>
						<p><label style="display: table-row;" for="price_lustraCount">Количество люстр:<span></span></label></p>
						<p style="margin-top: 5px;"><input class="digit" onchange="price_calc()" id="price_lustraCount" type="text" value="0"></input></p>
						<p><label style="display: table-row;" for="price_tochSvetCount">Количество точеных светильников:<span></span></label></p>
						<p style="margin-top: 5px;"><input class="digit" onchange="price_calc()" id="price_tochSvetCount" class="no-margin-bottom" type="text" value="0"></p>
						<p>
							<input onchange="price_calc()" value="350" id="price_with" type="radio" name="price_with">
							<label for="price_with">С проводом</label>
							<input onchange="calc()" value="250" id="wprice_ithout" type="radio" name="price_with">
							<label for="price_without">Без провода</label>
						</p>
						<p><label style="display: table-row;" for="price_trubCount">Количество труб для отвода:<span></span></label></p>
						<p style="margin-top: 5px;"><input class="digit" onchange="price_calc()" id="price_trubCount" type="text" value="0"></p>
						<p>
							<span>Итого:</span>
							<span><span id="price_result">0</span> рублей</span>
						</p>
					</form>
				</div>
			</div>
		</div>
	</div>
	
	
{include file="default/footer.tpl"}