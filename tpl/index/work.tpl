{include file="default/header.tpl"}

{include file="default/top.tpl"}

	<div id="content">
		<div class="container">
			<div class="layout-2-column">
			
				{include file="default/left_column.tpl"}
				
				<div class="column center">
					<div class="caption">
						<h1>{$bCrumbs}</h1>
					</div>
					
					<div class="more-products">
						{foreach from=$aImages item=item}
							<div style="text-align: center; margin: 10px; float: left; min-height: 220px;">
								<a rel="example_group" href="/public/img/upload/big/{$item.file_name}" title="{$item.title}">
									<img src="/public/img/upload/list/{$item.file_name}"><br>
								</a>
								<b>{$item.title}</b>
							</div>
						{/foreach}
					</div>	
					
					<div>
						{$aWork.text}
					</div>				
				</div>
			</div>
		</div>
	</div>
	
	
{include file="default/footer.tpl"}