{include file="default/header.tpl"}

{include file="default/top.tpl"}

	<div id="content">
		<div class="container">
			<div class="layout-2-column">
			
				{include file="default/left_column.tpl"}
				
				<div class="column center">
					<div class="caption">
						<h1>{$aCategory.title}</h1>
					</div>
					
					{$aCategory.about}
				</div>
			</div>
		</div>
	</div>
	
	
{include file="default/footer.tpl"}