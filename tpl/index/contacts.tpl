{include file="default/header.tpl"}

{include file="default/top.tpl"}

	<div id="content">
		<div class="container">
			<div class="layout-2-column">
			
				{include file="default/left_column.tpl"}
			
				<div class="column center">
					<div class="main-text">	
						<h1>{$strPageTitle}</h1>
                                                <h3>Телефонный номер и почтовый адрес для Ваших вопросов и заказов</h3>
                                                <p>
													Телефон: {$aContacts[6]}
													<br/>
													Email: {$aContacts[27]}
												</p>
                                                
                                                {if $aContacts[17]}<p id="address">{$aContacts[17]}</p>{/if}
						<div lat="{$aContacts[7]}" lon="{$aContacts[8]}" zoom="{$aContacts[9]}" id="map"></div>
					</div>					
				</div>
			</div>
		</div>
	</div>
	
	
{include file="default/footer.tpl"}