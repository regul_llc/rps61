{include file="default/header.tpl"}

{include file="default/top.tpl"}

	<div id="content">
		<div class="container">
			<div class="layout-2-column">
			
				{include file="default/left_column.tpl"}
			
				<div class="column center">
					<div class="main-text">	
						<div class="caption">
							<h1>{$strPageTitle}</h1>
							{if $nAction==1}<b>Спасибо, ваш отзыв принят. {if $bIsModering}Сообщение появится после проверки модератором.{/if}</b><br>{/if}
							{if $nAction==2}<b>Поля не заполнены</b>{/if}
						</div>
						<div class="review-form-container">
							<form id="rewForm" method="POST" enctype="multipart/form-data">
								<div class="input-row">
									<label for="tfName">Ваше Имя:</label>
									<input value="{if isset($smarty.post.review.author) && $nAction != 1}{$smarty.post.review.author}{/if}" name="review[author]" id="tfName" type="text">
								</div>
								<div class="input-row">
									<label for="tfEmail">Ваш E-Mail:</label>
									<input value="{if isset($smarty.post.review.email) && $nAction != 1}{$smarty.post.review.email}{/if}" name="review[email]" id="tfEmail" type="text">
								</div>
								<div class="input-row">
									<label id="labRewMess" for="taMessage">Ваш Текст:</label>
									<textarea name="review[text]" id="taMessage" cols="30" rows="10">{if isset($smarty.post.review.text) && $nAction != 1}{$smarty.post.review.text}{/if}</textarea>
								</div>
                                                                <div class="input-row rewCaptcha">
                                                                    <label for="taCaptcha">Код с картинки:</label>
                                                                    <img id="piccaptcha" src="captcha.php" />
                                                                    <input name="review[captcha]" id="taCaptcha" type="text"  />
                                                                    <a id="refCaptcha" href="#">обновить</a>
                                                                </div>
								<div class="submit-row">
									<input type="submit" value="отправить">
									<input id="load_img" name="image" type="file" onchange="$('#fname').html($(this).val())" value="отправить" style="display: none;">
									<input id="picloaderRew" type="button" value="загрузить фото" onclick="$('#load_img').click(); "> <span style="margin-left: 90px;" id="fname"></span>
								</div>
							</form>
						</div>
						
						<div class="reviews">
							{foreach from=$aReviews item=item}
								<p class="head">{$item.author}{* <span>| отзывов: 25</span>*}</p>
								{if $item.image}
									<img src="/public/img/reviews/{$item.image}" alt="Фотография" width="130" height="129">
								{/if}
								<div class="text-block">
									<p>{$item.text|strip_tags}</p>
								</div>
							{/foreach}
						</div>

					</div>					
				</div>
			</div>
		</div>
	</div>
	
	
{include file="default/footer.tpl"}