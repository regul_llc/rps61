{include file="default/header.tpl"}

{include file="default/top.tpl"}

	<div id="content">
		<div class="container">
			<div class="layout-2-column">
			
				{include file="default/left_column.tpl"}
				
				<div class="column center">
					<div class="caption">
						<h1>{$strPageTitle}</h1>
					</div>
					
					
					
					<div class="shop number-2 modified-no-border">
						{if isset($smarty.post.search_req)}
							{if isset($aResult)}
								<h3>По запросу "{$smarty.post.search_req}" {$strCountResult}</h3>
								{foreach from=$aResult item=item}
								
									<div class="product">
										<div class="header">
											<p class="name"><a href="/good/{$item.id}">{$item.title}</p></a>
											<p class="model"><a href="/good/{$item.id}">{$item.company} {$item.model}</a></p>
										</div>
										<div class="image-block">
                                                                                    <a href="/good/{$item.id}">
                                                                                        <img src="/public/img/{if !empty($item.cover)}upload/list/{$item.cover}{else}no_image.jpg{/if}" alt="{$item.title} {$item.company} {$item.model}" width="216" height="155">
                                                                                    </a>
										</div>
										<p class="price">{$item.price|number_format:0: ' ': ' '} руб.</p>
										<a good_id="{$item.id}" href="#" class="link-buy buy">купить</a>
									</div>
									
								{/foreach}
							{else}
								По запросу "{$smarty.post.search_req}" ничего не найдено
							{/if}
                                                        
						{/if}
						
					</div>
                                                {if isset($aResult)}
                                                                <div class="notPubOff" style="">*информация о ценах и наличии товара не является офертой.</div>
                                                        {/if}
					{*<div class="pagination">
						<a href="">&lt; назад</a>
						<a href="">1</a>
						<span class="current">2</span>
						<a href="">3</a>
						<a href="">вперед &gt;</a>
					</div>*}
				</div>
			</div>
		</div>
	</div>
	
	
{include file="default/footer.tpl"}