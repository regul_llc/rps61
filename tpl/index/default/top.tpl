	<header id="header">
		<div class="sub-header"></div>
		<div class="main-header">
			<div class="container">
				<div class="logo">
					<a href="/">
						<img src="/public/img/logo.png" title="{$aOptions[1]} - купить сплит-систему в Ростове" alt="{$aOptions[1]}" width="472" height="74">
					</a>
				</div>
				<div class="contacts">
					<p>{$aOptions[12]}</p>
					<p style="margin-top: 5px;"><a style="color:#009cff" href="{$aOptions[2]}">{$aOptions[2]}</a></p>
				</div>
				<div id="cart_info" class="shopping-cart">
					<p><a href="/cart">Корзина</a>: <span><span id="cart_info_amount">{$aCartInfo.amount}</span></span></p>
					<p><span id="cart_info_summ">{$aCartInfo.summ|number_format:0:" ":" "}</span> руб.</p>
				</div>
				<div class="search">
					<form action="/search" method="POST">
						<label for="tfSearch">Поиск</label>
						<input name="search_req" id="tfSearch" type="text" placeholder="...">
						<input type="submit" value="">
					</form>
				</div>
			</div>
		</div>
		<nav class="menu-main">
			<div class="container">
				<table>
					<tbody>
						<tr>
							<td {if $strPageCode eq 'aboutus'}class="current"{/if}><a href="/aboutus">О компании</a></td>
							<td {if $strPageCode eq 'service'}class="current"{/if}><a href="/service">Услуги</a></td>
							<td {if $strPageCode eq 'works' || $strPageCode eq 'work'}class="current"{/if}><a href="/works">Портфолио</a></td>
							<td {if $strPageCode eq 'actions'}class="current"{/if}><a href="/actions">Акции</a></td>
							<td {if $strPageCode eq 'articles'}class="current"{/if}><a href="/articles">Статьи</a></td>
							<td {if $strPageCode eq 'reviews'}class="current"{/if}><a href="/reviews">Отзывы</a></td>
							<td {if $strPageCode eq 'delivery'}class="current"{/if}><a href="/delivery">Оплата и доставка</a></td>
							<td {if $strPageCode eq 'guarantee'}class="current"{/if}><a href="/guarantee">Гарантии</a></td>
							<td {if $strPageCode eq 'vacancy'}class="current"{/if}><a href="/vacancy">Вакансии</a></td>
							<td {if $strPageCode eq 'contacts'}class="current"{/if}><a href="/contacts">Контакты</a></td>
						</tr>
					</tbody>
				</table>
			</div>
		</nav>
	</header>