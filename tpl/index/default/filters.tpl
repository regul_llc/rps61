{if count($aFilter)>0}
	<div class="filters">
		<form method="POST" action="">
		{foreach from=$aFilter item=item}
			{if $item.type == 3}
				<div class="row-container select">
					<p class="label">{$item.name}</p>
					<div class="select-container">
						<input value="{if isset($aCurFilters[{$item.id}])}$aCurFilters[{$item.id}]{/if}" type="checkbox" name="filter[{$item.id}]" />
					</div>
				</div>
			{elseif $item.type == 4 || $item.type == 6}
				<div class="row-container select">
					<p class="label">{$item.name}</p>
					<label for="tfA_1">от</label>
					<input name="filter[{$item.id}][from]" value="{if isset($aCurFilters[{$item.id}].from)}{$aCurFilters[{$item.id}].from}{/if}" id="tfA_1" type="text">
					<label for="tfA_2">до</label>
					<input name="filter[{$item.id}][to]" value="{if isset($aCurFilters[{$item.id}].to)}{$aCurFilters[{$item.id}].to}{/if}" id="tfA_2" type="text">
				</div>
			{elseif $item.type == 7 || $item.type == 8}
				<div class="row-container select">
					<p class="label">{$item.name}</p>
					<div class="select-container">
						<select name="filter[{$item.id}]">
							<option value="" {if isset($aCurFilters[{$item.id}]) && ($aCurFilters[{$item.id}] == '')}selected{/if}>не выбрано</option>
							{foreach from=$item.values item=i}
								{if $i != 'Toshiba' && $i != 'Airwell' && $i != 'Midea'}<option {if isset($aCurFilters[{$item.id}]) && ($aCurFilters[{$item.id}] == $i)}selected{/if}>{$i}</option>{/if}
							{/foreach}
						</select>
					</div>
				</div>
			{/if}
			
		{/foreach}
			<div class="row-container select">
				<input type="submit" value="Фильтровать" />
                                <input name="fReset" type="submit" value="Сбросить" />
			</div>
		</form>
	</div>	
{/if}