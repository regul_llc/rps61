	<div class="container height-auto">
		<footer id="footer">
			<nav class="footer-menu">
				<ul>
					<li><a href="/aboutus">О компании</a></li>
					<li><a href="/service">Услуги</a></li>
					<li><a href="/portfolio">Портфолио</a></li>
					<li><a href="/actions">Акции</a></li>
					<li><a href="/articles">Статьи</a></li>
					<li><a href="/reviews">Отзывы</a></li>
					<li><a href="/delivery">Доставка</a></li>
                                        <li><a href="/guarantee">Гарантии</a></li>
					<li><a href="/contacts">Контакты</a></li>
                                        <br/>
                                        <li style="margin-left: 0;">&copy; ООО «РостовПрофСервис» 2013-{$smarty.now|date_format:"%Y"}</li>
                                        <li><a href="/publicoffer" target=_blank>Публичная оферта</a></li>
				</ul>
			</nav>
			<div class="developers">
                            <p>Сайт разработан: <a href="http://webregul.ru/" target="_blank">Regul, LLC</a></p>
			</div>
		</footer>
	</div>

	{literal}
		<!-- Yandex.Metrika counter -->
		<script type="text/javascript">
		(function (d, w, c) {
			(w[c] = w[c] || []).push(function() {
				try {
					w.yaCounter24920585 = new Ya.Metrika({id:24920585,
							webvisor:true,
							clickmap:true,
							trackLinks:true,
							accurateTrackBounce:true});
				} catch(e) { }
			});

			var n = d.getElementsByTagName("script")[0],
				s = d.createElement("script"),
				f = function () { n.parentNode.insertBefore(s, n); };
			s.type = "text/javascript";
			s.async = true;
			s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

			if (w.opera == "[object Opera]") {
				d.addEventListener("DOMContentLoaded", f, false);
			} else { f(); }
		})(document, window, "yandex_metrika_callbacks");
		</script>
		<noscript><div><img src="//mc.yandex.ru/watch/24920585" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
		<!-- /Yandex.Metrika counter -->
	{/literal}
	
</body>
</html>