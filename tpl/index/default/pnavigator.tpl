{if $aPNavigator.pages|count > 1}

	<div class="pagination">
		<a href="{$aPNavigator.first}">&lt;&lt;&lt; </a>
		<a href="{$aPNavigator.prev}">&lt; назад</a>
		{foreach from=$aPNavigator.pages item=item}
			{if $item.url}
				<a href="{$item.url}">{$item.title}</a>
			{else}
				<span class="current">{$item.title}</span>
			{/if}
		{/foreach}
		<a href="{$aPNavigator.next}">вперед &gt;</a>
		<a href="{$aPNavigator.last}">&gt;&gt;&gt;</a>
	</div>
	
{/if}