				<aside class="column left">
					<section class="module">
						<header>
							<h2 class="caption">Продукция</h2>
						</header>
						<nav class="menu-additional">
							<ul class="level-1">
								{foreach from=$aCatsMenu item=item}								
									
									{*if $item.id == 359 || $item.id == 360 || $item.id == 361}
										<li><a href="/category/{$item.id}">{$item.title}</a></li>
									{elseif $item.id == 356}
										<li><a href="/catalog/{$item.childs[0].id}">{$item.title}</a></li>
									{else}
										<li><a class="category_item js-no-action" cat="{$item.id}" href="#">{$item.title}</a></li>
									{/if}
									
									{if $item.id != 356 && $item.id != 359 && $item.id != 360 && $item.id != 361}
										{foreach from=$item.childs key=key item=i}
										
											{if $key eq 0}
												<ul {if isset($nCurCat) && $nCurCat eq $item.id}{else}style="display: none;"{/if} id="sub{$item.id}" class="level-2">
											{/if}
											
											<li {if isset($nCurSubCat) && $nCurSubCat eq $i.id} class="current"{/if}><a href="/catalog/{$i.id}">{$i.title}</a></li>
											
											{if $key eq ($item.childs|@count-1)}
												</ul>	
											{/if}
										
										{/foreach}
									{/if}
										
									{if $key eq ($aCatsMenu|@count)}
										</ul>	
									{/if*}
									
									{if $item.childs|@count > 1}
										<li><a class="category_item js-no-action" cat="{$item.id}" href="#">{$item.title}</a></li>
										{foreach from=$item.childs key=key item=i}
											{if $key eq 0}
												<ul {if isset($nCurCat) && $nCurCat eq $item.id}{else}style="display: none;"{/if} id="sub{$item.id}" class="level-2">
											{/if}
											
											<li {if isset($nCurSubCat) && $nCurSubCat eq $i.id} class="current"{/if}><a href="/catalog/{$i.id}">{$i.title}</a></li>
											
											{if $key eq ($item.childs|@count-1)}
												</ul>	
											{/if}
										
										{/foreach}
									{elseif $item.childs|count == 1}
										<li><a href="/catalog/{$item.childs[0].id}">{$item.title}</a></li>
									{else}
										<li><a href="/category/{$item.id}">{$item.title}</a></li>
									{/if}
									
								{/foreach}

							</ul>
						</nav>
					</section>
					<section class="module">
						<header>
							<h2 class="caption">Новости</h2>
						</header>
						{foreach from=$aLastNews item=item}
							<div class="product-info" style="overflow:hidden; margin-bottom:10px;">
								<p class="name"><a href="/news/{$item.id}">{$item.title}</a></p>
								<p class="model">{$item.date|date_format:"%d.%m.%Y"}</p>
								<div class="image-block">
									<a href="/news/{$item.id}"><img src="/public/img/news/{$item.image}" alt="{$item.title}" ></a>
								</div>
								<div class="info">
									{$item.description|strip_tags}
								</div>
								<a href="/news/{$item.id}" class="link-more pull-right">подробнее ...</a>
							</div>
						{/foreach}
					</section>
				</aside>