<!DOCTYPE html>
<!--[if IE 7 ]>    <html lang="ru" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="ru" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="ru" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="ru" class="no-js"> <!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="description" content='{$aMetaInfo.description|default:''}' />
    <meta name="keywords" content="{$aMetaInfo.keywords|default:''}" />

    <title>{$aMetaInfo.title}</title>

    <!-- HTML 5 Boilerplate -->
    <link rel="stylesheet" href="/public/css/global.css">
    <link rel="stylesheet" href="/public/css/style_patch.css" />
    <link rel="stylesheet" href="/public/upload-plugin/css/jquery.fancybox.css">
    <link rel="stylesheet" href="/public/css/jquery.jgrowl.css" />
    <link rel="stylesheet" href="/public/css/chosen.css">


    <!-- Modernizr for HTML5 and JS feature detection -->
    <script src="/public/js/libs/modernizr-2.6.1.min.js"></script>

    <script src="/public/js/libs/jquery-1.9.1.min.js"></script>
    <script src="/public/js/libs/jquery.carouFredSel-6.2.1-packed.js"></script>
    <script src="/public/js/libs/jquery.placeholder.min.js"></script>
    <script src="/public/upload-plugin/js/jquery.fancybox.pack.js"></script>
    <script src="/public/js/libs/chosen.jquery.min.js"></script>
    <script src="/public/js/libs/jquery.maskedinput-1.3.js"></script>
    <script src="/public/js/libs/jquery.jgrowl.min.js"></script>
    <script src="/public/js/script.js"></script>
</head>
<body>