{include file="default/header.tpl"}

{include file="default/top.tpl"}

	<div id="content">
		<div class="container">
			<div class="layout-2-column">
			
				{include file="default/left_column.tpl"}
			
				<div class="column center">
                                    
					<div class="main-text">	
						<h1>{$strPageTitle}</h1>
                                                <!--div class="anchors">
                                                    <ul class="company-links">
                                                        <li>
                                                            <a href="/aboutus#certificates">Сертификаты</a>
                                                        </li>
                                                        <li>
                                                            <a href="/aboutus#partners">Партнеры</a>
                                                        </li>
                                                    </ul>
                                                </div-->
						{if !empty($aPage)}{$aPage.text|stripslashes}{else}{$strPageText|stripslashes}{/if}
					</div>
                                        
                                        
					{if $isGuarantee|default:false && !empty($aImages)}
						<div class="more-products">
							{foreach $aImages as $img}
								<div class="picgallBox">
									<a href="/public/img/upload/{$img.file_name}" rel="guarGroup" title="{$img.title}">
										<img src="/public/img/upload/list/{$img.file_name}" />
									</a>
									<div class="label"><b>{$img.title}</b></div>
								</div>
							{/foreach}
						</div>
					{/if}
                                        
                                        <!--div id="certificates">
                                            </br>
                                            <h3> Сертификаты </h3>
                                            </br>
                                            <div class="picgallBox">
                                                <a title="Компания 'МикроАРТ'" rel="articleGroup" href="/public/img/certificates/diplom_microart.JPG">
                                                    <img src="/public/img/certificates/diplom_microart_demo.jpg">
                                                </a>
                                                <div class="label">
                                                    <b>Компания "МикроАРТ"</b>
                                                </div>
                                            </div>
                                        </div-->
                                        <!--div id='partners'>
                                            </br>
                                            <h3>Партнеры</h3>
                                            </br>
                                            <div class='partner'>
                                                <div  class='partner_logo'>
                                                    <img src='/public/img/partners/microart.png'>
                                                </div>
                                                <span> Компания "МикроАРТ" </span></br>
                                            </div>
                                            <div class='partner'>
                                                <div  class='partner_logo'>
                                                    <img src='/public/img/partners/nanoteh.gif'>
                                                </div>
                                                <span>ЗАО "Концерн "Наноиндустрия"</span>
                                            </div>
                                            
                                        </div-->
				</div>
			</div>
		</div>
	</div>
	
	
{include file="default/footer.tpl"}