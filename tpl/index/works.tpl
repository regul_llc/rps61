{include file="default/header.tpl"}

{include file="default/top.tpl"}

	<div id="content">
		<div class="container">
			<div class="layout-2-column">
			
				{include file="default/left_column.tpl"}
				
				<div class="column center">
					<div class="caption">
						<h1>{if !isset($bCrumbs)}{$strPageTitle}{else}{$bCrumbs}{/if}</h1>
					</div>
					
					<div class="more-products">
						{if isset($aCats)}
							{foreach from=$aCats item=item}
								<div class="product-block">
									<h2>{$item.title}</h2>
									<a class="link-more pull-right" href="/works/{$item.id}"><img border="0" src="/public/img/upload/pwork/{$item.cover}" alt="{$item.title}" width="318" height="188"></a>
									<a class="link-more pull-right" href="/works/{$item.id}">подробнее ...</a>
								</div>
							{/foreach}
						{/if}
						
						{if isset($aWorks)}
							{foreach from=$aWorks item=item}
								<div class="product-block">
									<h2>{$item.title}</h2>
									<a class="link-more pull-right" href="/work/{$item.id}"><img src="/public/img/upload/pwork/{$item.cover}" alt="{$item.title}" width="318" height="188"></a>
									<a class="link-more pull-right" href="/work/{$item.id}">подробнее ...</a>
								</div>
							{/foreach}
						{/if}
					</div>					
				</div>
			</div>
		</div>
	</div>
	
	
{include file="default/footer.tpl"}