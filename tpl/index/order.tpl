{include file="default/header.tpl"}

{include file="default/top.tpl"}

	<div id="content">
		<div class="container">
			<div class="layout-2-column">
			
				{include file="default/left_column.tpl"}
			
				<div class="column center">
					<div class="main-text">	
						<h1>{$strPageTitle}</h1>
						<br>
						{if $nAction eq 1}
							<form id="form_order" method="POST" action="/order">
							<h3>Ваш заказ:</h3>
							
							{if $aCart|@count > 0}
									<table width="100%">
									<tr>
									<th align="left">Наименование</th>
									<th align="left">Цена</th>
									<th align="left">Кол-во</th>
									<th align="left">Сумма</th>
									<td></td>
									</tr>
								
								{$nTotalSumm=0}
								{$nTotalAmunt=0}
								{foreach from=$aCart item=item}
									<tr>
									<td valign="top" style="padding-top: 10px;">
										<img src="/public/img/{if !empty($item.good.cover)}upload/small/{$item.good.cover}{else}no_image_cart.jpg{/if}" style="float: left; margin-right: 5px;">
									<b style="margin: 0;">{$item.good.title}</b><br><small>{$item.good.company} {$item.good.model}</small></td>
									<td>{$item.good.price|number_format:0:" ":" "} руб.</td>
									<td>{$item.amount}</td>
									<td>{($item.good.price * $item.amount)|number_format:0:" ":" "} руб.</td>
									<td><a href="/cart/del/{$item.id}">удалить</a></td>
									<!-- {$nTotalSumm = $nTotalSumm + $item.good.price * $item.amount} -->
									<!-- {$nTotalAmunt = $nTotalAmunt + $item.amount} -->
									</tr>
										
								{/foreach}			
								
									<tr>
									<td colspan="2" align="left"><b>Всего:</b></td>
									<td>{$nTotalAmunt}</td>
									<td>{$nTotalSumm|number_format:0:" ":" "} руб.</td>
									<td></td>
									</tr>
									
									<tr><td colspan="5" style="height: 30px;"></tr>
									
									<tr><td colspan="5"><b>Контактные данные:</b></td></tr>
									
									<tr>
										<td>Ваше Имя *</td>
										<td colspan="4">
											<input id="order_name" style="width: 100%;" type="text" name="user[name]" />
										</td>
									</tr>
									
									<tr>
										<td>Фамилия</td>
										<td colspan="4">
											<input id="order_lname" style="width: 100%;" type="text" name="user[lastname]" />
										</td>
									</tr>
									
									<tr>
										<td>Телефон *</td>
										<td colspan="4">
											<input class="phone" id="order_phone" style="width: 120px;" type="text" name="user[phone]" />
										</td>
									</tr>
									
									<tr>
										<td>Адрес *</td>
										<td colspan="4">
											<textarea id="order_address" style="width: 100%;" name="user[address]"></textarea>
										</td>
									</tr>
									
									<tr>
										<td>Комментарий к заказу</td>
										<td colspan="4">
											<textarea id="order_comment" style="width: 100%;" name="user[comment]"></textarea>
										</td>
									</tr>
									
									<tr><td colspan="5" style="height: 30px;"></tr>
									
                                                                        <tr>
                                                                            <td align="right" colspan="5">
                                                                                <div id="agreement" class="agreement">
                                                                                    <div class="border-box"></div>   
                                                                                    <input id="checkboxAgreement" type="checkbox" class="bigCheckbox" name="pubOff"  value="Подтвердить заказ"  /><label for ="checkboxAgreement">
                                                                                             
                                                                                        я даю свое согласие на обработку персональных данных на условиях и для целей, определенных в <a href="/publicoffer" target="_blank">публичной оферте</a> и подтверждаю согласие со всеми положениями описанными в ней.
                                                                                    </label>
                                                                                </div>
									</td>
                                                                        
									<tr>
                                                                            <td align="right" colspan="5">
										<input type="submit" name="confirm"  value="Подтвердить заказ" />
									</td>
										<!--<input type="submit" name="recount"  value="Пересчитатьee" />-->
									</td>
									</tr>
									
									</table>		
								
								</form>
							{/if}
							
						{elseif $nAction eq 2 && isset($nOrder)}
							<b>Ваш заказ №{$nOrder} принят! Мы свяжемся с вами в близжайшее время.</b>
                                                        <p><a href="/printorder/{("$nOrder$secretkey")|md5}" target="_blank">Распечатать счет</a></p>
						{/if}
							
					</div>					
				</div>
			</div>
		</div>
	</div>
	
	
{include file="default/footer.tpl"}