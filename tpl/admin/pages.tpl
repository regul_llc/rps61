{include file="default/header.tpl"}

<h1 id="head">Панель управления "{$strSiteName}"</h1>
		
	{include file="default/menu.tpl"}

	<div id="content" class="container_16 clearfix">
	
		<div class="grid_16">
			<h2>{$strPageName}</h2>
			{*if isset($crumbs) || isset($aPage)}
				<div class="editcat-breadcrumbs">
					<a href="{$strPath}/{$strPageCode}">Список страниц</a>
				</div>
			{/if*}
			<div class="utils">
				<a href="{$strPath}/{$strPageCode}/new" style="margin: 13px 20px 0 0;">Добавить страницу</a>
			</div>
			{if $nAction==1}<p class="success">Ваши изменения сохранены</p>{/if}
			{if $nAction==2}<p class="error">При сохранении произошла ошибка</p>{/if}
		</div>
		
		{if isset($aPages)}
			{*cписок страниц*}
			<div class="grid_16">
				<table>
					<thead>
						<tr>
							<th>Название</th>
							<th colspan="2" width="10%">Действия</th>
						</tr>
					</thead>

					<tbody>
					
			{foreach from=$aPages item=item}
			
						<tr>
							<td><a href="{$strPath}/{$strPageCode}/{$item.id}">{$item.title}</a></td>
							<td><a href="{$strPath}/{$strPageCode}/{$item.id}" class="edit">Редактировать</a></td>
							<td><a href="{$strPath}/{$strPageCode}/{$item.id}/del" class="delete">Удалить</a></td>
						</tr>
				
			{/foreach}
			
					</tbody>
				</table>
			</div>
			
		{elseif isset($aPage)}
			{*редактирование страницы*}
			<form method="POST" action="">
				<div class="grid_16">
					<p>
						<label>Заголовок <small>не длиннее 255 символов</small></label>
						<input type="text" name="page[title]" value="{$aPage.title}" />
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Текст <small>полный текст страницы</small></label>
						<textarea class="editor" name="page[text]">{$aPage.text}</textarea>
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Мета-заголовок <small>заголовок окна браузера не длиннее 255 символов</small></label>
						<input type="text" name="page[meta][title]" value="{if isset($aPage.meta.title)}{$aPage.meta.title}{/if}" />
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Мета-описание <small></small></label>
						<textarea style="height: 100px; max-width: 935px;" name="page[meta][description]">{if isset($aPage.meta.description)}{$aPage.meta.description}{/if}</textarea>
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Мета-слова <small>ключевые слова для поисковых систем</small></label>
						<textarea style="height: 100px; max-width: 935px;" name="page[meta][keywords]">{if isset($aPage.meta.keywords)}{$aPage.meta.keywords}{/if}</textarea>
					</p>
				</div>
    			
				<div class="grid_16">
					<p class="submit">
						<input type="submit" name="reset" value="Назад">
						<input type="submit" value="Сохранить" />
					</p>
				</div>
				
			</form>
			
			{if $aPage.id == 6}
				<input id="item_id" type="hidden" item_id="1" />
				<input id="item_type" type="hidden" item_type="guarantee" />
				
				<div style="clear: both; width: 100%; height: 30px;"></div>
				<div class="grid_16">
					<p>
						<label>Изображения <small>все изменения происходят сразу, не нужно нажимать кнопку "сохранить"</small></label>
						{include file="upload_plugin/loader.tpl"}
					</p>
				</div>
				{include file="upload_plugin/include_js.tpl"}
			{/if}
			
		{else}
			{*добавление страницы*}
			<form method="POST" action="">
			
				<div class="grid_16">
					<p>
						<label>Заголовок <small>не длиннее 255 символов</small></label>
						<input type="text" name="page[title]" value="" />
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Текст <small>полный текст страницы</small></label>
						<textarea class="editor" name="page[text]"></textarea>
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Мета-заголовок <small>заголовок окна браузера не длиннее 255 символов</small></label>
						<input type="text" name="page[meta][title]" />
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Мета-описание <small></small></label>
						<textarea style="height: 100px; max-width: 935px;" name="page[meta][description]"></textarea>
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Мета-слова <small>ключевые слова для поисковых систем</small></label>
						<textarea style="height: 100px; max-width: 935px;" name="page[meta][keywords]"></textarea>
					</p>
				</div>
                                                        
				<div class="grid_16">
					<p class="submit">
						<input type="submit" name="reset" value="Назад">
						<input type="submit" value="Добавить" />
					</p>
				</div>
				
			</form>
			
		{/if}
	
	</div>

{include file="default/bottom.tpl"}

{include file="default/footer.tpl"}