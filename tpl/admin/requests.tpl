{include file="default/header.tpl"}

<h1 id="head">Панель управления "{$strSiteName}"</h1>
		
	{include file="default/menu.tpl"}

	<div id="content" class="container_16 clearfix">
	
		<div class="grid_16">
			<h2>{$strPageName}</h2>
			
			{if $nAction==1}<p class="success">Ваши изменения сохранены</p>{/if}
			{if $nAction==2}<p class="error">При сохранении произошла ошибка</p>{/if}
		</div>

		{if isset($aRequests)}
		
			<div class="grid_16">
				<table>
					<thead>
						<tr>
							<th>Имя</th>
							<th>Телефон</th>
							<th>Дата добавления</th>
							<th>Статус</th>							
							<th colspan="3" width="10%">Действия</th>
						</tr>
					</thead>
					{*
					<tfoot>
						<tr>
							<td colspan="5" class="pagination">
								<span class="active curved">1</span><a href="#" class="curved">2</a><a href="#" class="curved">3</a><a href="#" class="curved">4</a> ... <a href="#" class="curved">10 million</a>
							</td>
						</tr>
					</tfoot>
					*}
					<tbody>
					
			{foreach from=$aRequests item=item}
			
						<tr>
							<td>{$item.name}</td>
							<td>{$item.phone}</td>
							<td style="white-space: nowrap;">{$item.date_add|date_format:"%D %H:%M"}</td>
							<td style="white-space: nowrap;">{if $item.status==1}обработан{else}не обработан{/if}</td>
							<td><a href="{$strPath}/{$strPageCode}/{$item.id}" class="edit">Редактировать</a></td>
							<td><a href="{$strPath}/{$strPageCode}/{$item.id}/del" class="delete">Удалить</a></td>
						</tr>
				
			{/foreach}
			
					</tbody>
				</table>
			</div>
			
		{elseif isset($aRequest)}
		
			<form method="POST" action="">
			
				<div class="grid_16">
					<p>
						<label>Статус <small>обработан или не обработан этот заказ</small></label>
						<input style="width: 20px;" type="checkbox" name="requests[status]" {if $aRequest.status}checked{/if} />
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Имя <small>имя автора заявки</small></label>
						<input type="hidden" name="requests[name]" value="{$aRequest.name}" />
						<b>{$aRequest.name}</b>
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Телефон <small>номер телефона автора заявки</small></label>
						<b>{$aRequest.phone}</b>
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Дата добавления заявки <small></small></label>
						<b>{$aRequest.date_add|date_format:"%D %H:%M"}</b>
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Адрес <small>адрес автора зафвки</small></label>
						<b>{$aRequest.address}</b>
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Дополнительная информация <small></small></label>
						<b>{$aRequest.text}</b>
					</p>
				</div>
			
				<div class="grid_16">
					<p class="submit">
						<input type="submit" name="reset" value="Назад">
						<input type="submit" value="Сохранить" />
					</p>
				</div>
				
			</form>			
		
		{/if}
	
	
	</div>

{include file="default/bottom.tpl"}

{include file="default/footer.tpl"}