{strip}
<form id="attGoodsForm" action="" method="POST" style="display: none; position: fixed; z-index: 1000; background-color: #FFFFFF; border: 1px solid #CCCCCC; width: 700px; height: 90%; top: 30px; margin-left: 120px; overflow-y: scroll; overflow-x: hidden; box-shadow: 0 0 5px #005500;">
    <h2 class="ml10">Сопутствующие товары</h2>
   <div class="grid_16">
        <label>Наименование <small>введите название товара для поиска (не менее 3-х символов)</small></label>
        <input id="attGSearch" class="w69p" type="text" value="" />
    </div>
    
    <div class="grid_16">
        <p class="submit">
            <input id="attGButtSearch" type="submit" value="искать" />
            <input id="attGButtAdd" class="ml10" type="submit" value="добавить" gidup="{$aGood.id}" />
            <input id="attGButtClose" type="submit" value="отмена" />
        </p>
    </div>
    
    <div class="grid_16 w71p">
        <table class="w70_5p">
            <thead>
                <tr>
                    <th width="20"><input type="checkbox" /></th>
                    <th>Наименование</th>
                </tr>
            </thead>
            <tbody id="popupGTbody"></tbody>
        </table>
    </div>
</form>
{/strip}