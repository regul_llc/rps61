{strip}
	<form id="propCatForm" action="" method="POST">
		<h2 class="header ml10">Редактирование списка характеристик</h2>
		<div class="grid_16">
			<label>Значение <small>введите значение характеристики</small></label>
			<input class="field-new-prop w69p" type="text" value="" />
		</div>
		
		<div class="grid_16">
			<p class="submit">
				<input class="add-prop-butt" type="submit" value="добавить" />
			</p>
		</div>
		
		<div class="grid_16 w71p">
			<table class="w70_5p">
				<thead>
					<tr>
						<th>Значение</th>
						<th width="100">Действие</th>
					</tr>
				</thead>
				<tbody class="prop-tbody"></tbody>
			</table>
		</div>
			
		<div class="grid_16 subbuttons">
			<p class="submin">
				<input class="saver" type="submit" value="сохранить" />
				<input class="closer" type="submit" value="закрыть" />
			</p>
		</div>
	</form>
{/strip}