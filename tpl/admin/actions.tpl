{include file="default/header.tpl"}

<h1 id="head">Панель управления "{$strSiteName}"</h1>
		
	{include file="default/menu.tpl"}

	<div id="content" class="container_16 clearfix">
	
		<div class="grid_16">
			<h2>{$strPageName}</h2>
			<div class="utils">
				<a href="{$strPath}/{$strPageCode}/new" style="margin: 13px 20px 0 0;">Добавить акцию</a>
			</div>
			{if $nAction==1}<p class="success">Ваши изменения сохранены</p>{/if}
			{if $nAction==2}<p class="error">При сохранении произошла ошибка</p>{/if}
		</div>
		
		{if isset($aActions)}
			<div class="grid_16">
				<table>
					<thead>
						<tr>
							<th>Название</th>
							<th colspan="2" width="10%">Действия</th>
						</tr>
					</thead>

					<tbody>
					
			{foreach from=$aActions item=item}
			
						<tr>
							<td><a href="{$strPath}/{$strPageCode}/{$item.id}">{$item.title}</a></td>
							<td><a href="{$strPath}/{$strPageCode}/{$item.id}" class="edit">Редактировать</a></td>
							<td><a href="{$strPath}/{$strPageCode}/{$item.id}/del" class="delete">Удалить</a></td>
						</tr>
				
			{/foreach}
			
					</tbody>
				</table>
			</div>
			
			<form method="POST" action="">
				{foreach $metaOpts as $option}
					{if $option.type == 1}
						<div class="grid_16">
							<p>
								<label>{$option.name} <small>{$option.title}</small></label>
								<input type="text" name="options[{$option.id}]" value="{$option.value}" />
							</p>
						</div>
					{elseif $option.type == 2}
						<div class="grid_16">
							<p>
								<label>{$option.name} <small>{$option.title}</small></label>
								<textarea style="height: 100px; max-width: 935px;" name="options[{$option.id}]">{$option.value|stripslashes}</textarea>
							</p>
						</div>
					{/if}
				{/foreach}
				
				<div class="grid_16">
					<p class="submit">
						<input type="submit" value="Сохранить" />
					</p>
				</div>
			</form>
			
		{elseif isset($aAction)}
		
			<form enctype="multipart/form-data" method="POST" action="">
				<div class="grid_16">
					<p>
						<label>Публикация <small>не опубликованные акции не выводятся</small></label>
						<input style="width: 20px;" type="checkbox" name="action[public]" {if $aAction.public}checked="checked"{/if} />
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Заголовок <small>не длиннее 255 символов</small></label>
						<input type="text" name="action[title]" value="{$aAction.title}" />
					</p>
				</div>
				
                                <div class="grid_16">
                                    <p>
                                        <label> Дата акции</label>
                                        <input type="text" id="datepick" name="action[date]" value="{$aAction.date|date_format:"%d-%m-%y"}" />
                                    </p>
                                </div>

				<div class="grid_16">
					<p>
						<label>Картинка <small></small></label>
						{if $aAction.image}
							<img src="/public/img/actions/preview/{$aAction.image}" style="margin: 10px;" /><br>
						{/if}
						<input type="file" name="image" />
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Анонс <small>краткое вступление</small></label>
						<textarea class="editor" name="action[description]">{$aAction.description}</textarea>
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Текст <small>полный текст акции</small></label>
						<textarea class="editor" name="action[text]">{$aAction.text}</textarea>
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Мета-заголовок <small>заголовок окна браузера не длиннее 255 символов</small></label>
						<input type="text" name="action[meta][title]" value="{$aAction.meta.title|default:''}" />
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Мета-описание <small></small></label>
						<textarea style="height: 100px; max-width: 935px;" name="action[meta][description]">{$aAction.meta.description|default:''}</textarea>
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Мета-слова <small>ключевые слова для поисковых систем</small></label>
						<textarea style="height: 100px; max-width: 935px;" name="action[meta][keywords]">{$aAction.meta.keywords|default:''}</textarea>
					</p>
				</div>
			
				<div class="grid_16">
					<p class="submit">
						<input type="submit" name="reset" value="Назад">
						<input type="submit" value="Сохранить" />
					</p>
				</div>
				
			</form>
			
		{else}
		
			<form enctype="multipart/form-data" method="POST" action="">
				<div class="grid_16">
					<p>
						<label>Публикация <small>не опубликованные акции не выводятся</small></label>
						<input style="width: 20px;" type="checkbox" name="action[public]" />
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Заголовок <small>не длиннее 255 символов</small></label>
						<input type="text" name="action[title]" />
					</p>
				</div>
				
				<div class="grid_16">
                                    <p>
                                        <label> Дата акции</label>
                                        <input type="text" id="datepicker" name="action[date]" value="{$smarty.now|date_format:$aAction.date}" />
                                    </p>
                                </div>

                                <div class="grid_16">
					<p>
						<label>Картинка <small></small></label>
						<input type="file" name="image" />
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Анонс <small>краткое вступление</small></label>
						<textarea class="editor" name="action[description]"></textarea>
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Текст <small>полный текст акции</small></label>
						<textarea class="editor" name="action[text]"></textarea>
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Мета-заголовок <small>заголовок окна браузера не длиннее 255 символов</small></label>
						<input type="text" name="action[meta][title]" />
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Мета-описание <small></small></label>
						<textarea style="height: 100px; max-width: 935px;" name="action[meta][description]"></textarea>
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Мета-слова <small>ключевые слова для поисковых систем</small></label>
						<textarea style="height: 100px; max-width: 935px;" name="action[meta][keywords]"></textarea>
					</p>
				</div>
				
				<div class="grid_16">
					<p class="submit">
						<input type="submit" name="reset" value="Назад">
						<input type="submit" value="Сохранить" />
					</p>
				</div>
				
			</form>
			
		{/if}
	
	
	</div>

{include file="default/bottom.tpl"}

{include file="default/footer.tpl"}