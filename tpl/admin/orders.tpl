{include file="default/header.tpl"}

<h1 id="head">Панель управления "{$strSiteName}"</h1>
		
	{include file="default/menu.tpl"}

	<div id="content" class="container_16 clearfix">
	
		<div class="grid_16">
			<h2>{$strPageName}</h2>
			<div class="utils">
				<!--<a href="{$strPath}/{$strPageCode}/new" style="margin: 13px 20px 0 0;">Добавить статью</a>-->
			</div>
			{if $nAction==1}<p class="success">Ваши изменения сохранены</p>{/if}
			{if $nAction==2}<p class="error">При сохранении произошла ошибка</p>{/if}
		</div>
		
		{if isset($aOrders)}
		
			<div class="grid_16">
				<table>
					<thead>
						<tr>
							<th>№</th>
							<th>Принят</th>
							<th>Заказчик</th>
							<th>Сумма</th>
							<th>Сатус</th>
							<th colspan="2" width="10%">Действия</th>
						</tr>
					</thead>
					{*
					<tfoot>
						<tr>
							<td colspan="5" class="pagination">
								<span class="active curved">1</span><a href="#" class="curved">2</a><a href="#" class="curved">3</a><a href="#" class="curved">4</a> ... <a href="#" class="curved">10 million</a>
							</td>
						</tr>
					</tfoot>
					*}
					<tbody>
					
			{foreach from=$aOrders item=item}
			
						<tr>
							<td><a href="{$strPath}/{$strPageCode}/{$item.id}"><b>{$item.id}</b></a></td>
							<td>{$item.date_add|date_format:"%D %H:%M"}</td>
							<td>{$item.user_lastname} {$item.user_name}</a></td>
							<td>{$item.summ|number_format:0:" ":" "} руб.</td>
							<td>
								{if $item.status eq 0}Новый
								{elseif $item.status eq 1}В обработке
								{elseif $item.status eq 2}Ожидает оплаты
								{elseif $item.status eq 3}Оплачен
								{elseif $item.status eq 4}Исполнен
								{/if}
							</td>
							<td><a href="{$strPath}/{$strPageCode}/{$item.id}" class="edit">Детали заказа</a></td>
							<td><a href="{$strPath}/{$strPageCode}/{$item.id}/del" class="delete">Удалить</a></td>
						</tr>
				
			{/foreach}
			
					</tbody>
				</table>
			</div>
			
		{elseif isset($aOrder)}
		
			<form method="POST" action="">				
				<div class="grid_16">
                                    <p style="display: inline-block;">
                                        <b>ФИО: </b>{$aOrder.user_name} {$aOrder.user_lastname}<br />
                                        <b>тел.: </b>{$aOrder.user_phone}<br />
                                        <b>Адрес: </b>
                                    </p>
                                    <div style="display: inline-block; margin-left: -100px;">{$aOrder.user_address}</div>
                                </div>
                                				
				<div class="grid_16">
					<p>					
						 <label>Заказанные товары: <small></small></label>
						{foreach from=$aOrder.goods item=item}
							<div style="width: 100%; clear: both; padding-top: 15px;">
								<div style="margin: 0 20px 20px 0; float: left;">
									<img src="/public/img/upload/small/{$item.cover}">
								</div>
								<div style="float: left;">
									<b>{$item.title} {$item.company} {$item.model}</b><br>
									Цена: {$item.price|number_format:0:" ":" "} руб.<br>
									Количество: {$item.amount}<br>
									На сумму: {($item.amount * $item.price)|number_format:0:" ":" "} руб.<br>
									ID: {$item.id}
								</div>
							</div>
						{/foreach}
						
						
					</p>
				</div>
				
                                <div class="grid_16">
					<p style="margin-top: 15px; {if !empty($aOrder.user_comment)}margin-bottom: 15px;{/if}">
						<label>Общая сумма заказа: <b>{$aOrder.summ|number_format:0:" ":" "} руб.</b></label>
					</p>
				</div>
                                
                                {if !empty($aOrder.user_comment)}
				<div class="grid_16">
					<p>
						<label><b>Комментарий</b> <small></small></label>
					</p>
                                        {$aOrder.user_comment}
				</div>
                                {/if}
                                
                                <div class="grid_16">
					<p style="margin-top: 15px;">
						<label>Статус <small></small></label>
						<select name="order[status]">
							<option value="0" {if $aOrder.status eq 0}selected{/if}>Новый</option>
							<option value="1" {if $aOrder.status eq 1}selected{/if}>В обработке</option>
							<option value="2" {if $aOrder.status eq 2}selected{/if}>Ожидает оплаты</option>
							<option value="3" {if $aOrder.status eq 3}selected{/if}>Оплачен</option>
							<option value="4" {if $aOrder.status eq 4}selected{/if}>Доставлен</option>
						</select>
					</p>
				</div>
			
				<div class="grid_16">
					<p class="submit">
						<input type="submit" name="reset" value="Назад">
						<input type="submit" value="Сохранить" />
					</p>
				</div>
				
			</form>
			
		{else}
		
			<form method="POST" action="">
			
				<div class="grid_16">
					<p>
						<label>Заголовок <small>не длиннее 255 символов</small></label>
						<input type="text" name="article[title]" value="" />
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Текст <small>полный текст статьи</small></label>
						<textarea class="editor" name="article[text]"></textarea>
					</p>
				</div>
				<div class="grid_16">
					<p class="submit">
						<input type="submit" name="reset" value="Назад">
						<input type="submit" value="Добавить" />
					</p>
				</div>
				
			</form>
			
		{/if}
	
	
	</div>

{include file="default/bottom.tpl"}

{include file="default/footer.tpl"}