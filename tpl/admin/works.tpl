{include file="default/header.tpl"}

<h1 id="head">Панель управления "{$strSiteName}"</h1>
		
	{include file="default/menu.tpl"}

	<div id="content" class="container_16 clearfix">
	
		<div class="grid_16">
			<h2><a href="{$strPath}/work_categories">Рубрики</a> :: <a href="{$strPath}/{$strPageCode}/{$aCategory.id}">{$aCategory.title}</a> :: {$strPageName}</h2>
			{if isset($aWorks)}
				<div class="utils">
					<a href="{$strPath}/{$strPageCode}/{$aCategory.id}/new" style="margin: 13px 20px 0 0;">Добавить работу</a>
				</div>
			{/if}
			
			{if $nAction==1}<p class="success">Ваши изменения сохранены</p>{/if}
			{if $nAction==2}<p class="error">При сохранении произошла ошибка</p>{/if}
		</div>

		{if isset($aWorks)}
		
			<div class="grid_16">
				<table>
					<thead>
						<tr>
							<th width="50%">Название</th>
							<th>Дата</th>
							<th>Публикация</th>							
							<th colspan="3" width="10%">Действия</th>
						</tr>
					</thead>
					{*
					<tfoot>
						<tr>
							<td colspan="5" class="pagination">
								<span class="active curved">1</span><a href="#" class="curved">2</a><a href="#" class="curved">3</a><a href="#" class="curved">4</a> ... <a href="#" class="curved">10 million</a>
							</td>
						</tr>
					</tfoot>
					*}
					<tbody>
					
			{foreach from=$aWorks item=item}
			
						<tr>
							<td><a href="{$strPath}/{$strPageCode}/{$aCategory.id}/{$item.id}">{$item.title}</a></td>
							<td style="white-space: nowrap;">{$item.date|date_format:"%D %H:%M"}</td>
							<td style="white-space: nowrap;">{if $item.public==1}опубликовано{else}не опубликовано{/if}</td>
							<td><a href="{$strPath}/{$strPageCode}/{$aCategory.id}/{$item.id}" class="edit">Редактировать</a></td>
							<td><a href="{$strPath}/{$strPageCode}/{$aCategory.id}/{$item.id}/del" class="delete">Удалить</a></td>
						</tr>
				
			{/foreach}
			
					</tbody>
				</table>
			</div>
			
		{elseif isset($aWork)}
		
			<input id="item_id" cover="{$aWork.cover}" type="hidden" item_type="works" item_id="{$aWork.id}" priority="{$aWork.priority}" />
			<input id="item_type" type="hidden" item_type="works" />
			<form method="POST" action="">
			
				<div class="grid_16">
					<p>
						<label>Публикация <small>неопубликованные работы не выводятся на сайте</small></label>
						<input style="width: 20px;" type="checkbox" name="work[public]" {if $aWork.public}checked{/if} />
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Заголовок <small>не длиннее 255 символов</small></label>
						<input type="text" name="work[title]" value="{$aWork.title}" />
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Обложка <small>выбирается внизу из загруженных изображений</small></label>
						<div id="work_cover_box">
							{if $aWork.cover}								
								<img src="/public/img/upload/thumbnail/{$aWork.cover}">
							{else}							
								не выбрана
							{/if}
						</div>
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Текст <small>подробное описание работы</small></label>
						<textarea style="height: 300px;" class="editor" name="work[text]">{$aWork.text}</textarea>
					</p>
				</div>
			
				<div class="grid_16">
					<p class="submit">
						<input type="submit" name="reset" value="Назад">
						<input type="submit" value="Сохранить" />
					</p>
				</div>
				
			</form>
			<div style="clear: both; width: 100%; height: 30px;"></div>
			<div class="grid_16">
				<p>
					<label>Изображения <small>все изменения при манипуляциях с изображениями происходят сразу, не нужно нажимать кнопку "сохранить"</small></label>
					{include file="upload_plugin/loader.tpl"}
				</p>
			</div>
			
			{include file="upload_plugin/include_js_works.tpl"}
			
		{else}
		
			<form method="POST" action="">
			
				<div class="grid_16">
					<p>
						<label>Публикация <small>неопубликованные работы не выводятся на сайте</small></label>
						<input style="width: 20px;" type="checkbox" name="work[public]" />
					</p>
				</div>
			
				<div class="grid_16">
					<p>
						<label>Заголовок <small>не длиннее 255 символов</small></label>
						<input type="text" name="work[title]" value="" />
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Текст <small>подробное описание рубрики</small></label>
						<textarea class="editor" name="work[text]"></textarea>
					</p>
				</div>
				<div class="grid_16">
					<p class="submit">
						<input type="submit" name="reset" value="Назад">
						<input type="submit" value="Добавить" />
					</p>
				</div>
				
			</form>
			
		{/if}
	
	
	</div>

{include file="default/bottom.tpl"}

{include file="default/footer.tpl"}