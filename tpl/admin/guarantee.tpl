{include file="default/header.tpl"}

<h1 id="head">Панель управления "{$strSiteName}"</h1>

    {include file="default/menu.tpl"}
    
    <div id="content" class="container_16 clearfix">
	
		<div class="grid_16">
			<h2>{$strPageName}</h2>
			{if $nAction==1}<p class="success">Ваши изменения сохранены</p>{/if}
			{if $nAction==2}<p class="error">При сохранении произошла ошибка</p>{/if}
		</div>
	
		<form method="POST" action="">
                        <div class="grid_16">
                            <p>
                                <label>Текст страницы <small></small></label>
                                <textarea type="text" name="options[18]">{$pageText|stripslashes}</textarea>
                            </p>
                        </div>
                                        
			<div class="grid_16">
				<p class="submit">
					<input type="submit" value="Сохранить" />
				</p>
			</div>
			
		</form>
		
                <input id="item_id" type="hidden" item_id="1" />
                <input id="item_type" type="hidden" item_type="guarantee" />

                <div style="clear: both; width: 100%; height: 30px;"></div>
                <div class="grid_16">
                        <p>
                                <label>Изображения <small>все изменения происходят сразу, не нужно нажимать кнопку "сохранить"</small></label>
                                {include file="upload_plugin/loader.tpl"}
                        </p>
                </div>
                {include file="upload_plugin/include_js.tpl"}
	
	</div>

{include file="default/bottom.tpl"}

{include file="default/footer.tpl"}