{include file="default/header.tpl"}

<h1 id="head">Панель управления "{$strSiteName}"</h1>

        {include file="default/menu.tpl"}
    
        <div id="content" class="container_16 clearfix">
            <div class="grid_16">
                <h2>{$strPageName}</h2>
                {if $nAction==1}<p class="success">Ваши изменения сохранены</p>{/if}
                {if $nAction==2}<p class="error">При сохранении произошла ошибка</p>{/if}
            </div>
            {if $aValutes}
            <form method="POST" action="">
                <div class="grid_16">
                    <p style="font-size: 15px; font-weight: bold;">Курс валют :</p>
                </div>
                {foreach $aValutes as $valute}
                    <div class="grid_16">
                        <p>
                            <label>{$valute.title} <small>укажите число большее 0</small></label>
                            <input style="width: 90px;" name="valutes[{$valute.id}]" type="text" value="{$valute.exrate}" />
                        </p>
                    </div>
                {/foreach}
				<div class="grid_16">
					<p>
						<input id="updateByCB" name="options[updateByCB]" type="checkbox" value="1" {if $aOptions[28]}checked="checked"{/if} />
						<label for="updateByCB" style="display: inline;">Обновлять цены автоматически по курсу центробанка <small></small></label>						
					</p>
				</div>
				<div class="grid_16">
					<p>
						<p style="font-size: 15px;"><b>Статус обновления цен :</b> <br/>
							{if $aOptions[29] eq 0}
								Обновление запущено. Подготовка данных к обновлению.
							{elseif $aOptions[29] eq 1}
								Обновление запущено. Идет перессчет.
							{elseif $aOptions[29] eq 2}
								Обновление завершено успешно <i>{$aOptions[30]|date_format:"%d.%m.%Y %H:%M"}</i>.
							{else}
								Обновление не производится
							{/if}							
						</p>
						{if $aOptions[29] eq 1}
						<p>
							Обновлено товаров: <span id="PricesUpdatingCount"></span><br/>
							Осталось обновить товров: <span id="PricesForUpdatingCount"></span>
						</p>
						<div class="progress progress-striped active">
							<div id="statusbar" class="bar" style="width: 0%;"></div>
						</div>
						{literal}
							<script>
								$(function() {
									var StartCount = 1;
									var nPercent = 0;
									$.post("/ajax/getPriceUpdStats", function(data){
										StartCount = data.result.PricesUpdatingStartCount.value;
										var interval = setInterval(function(){
											$.post("/ajax/getPriceUpdStats", function(data){
												//console.log(data);
												var aParams = data.result;
												$('#PricesUpdatingCount').text(aParams.PricesUpdatingCount.value);
												$('#PricesForUpdatingCount').text(aParams.PricesForUpdatingCount.value);
												nPercent = aParams.PricesUpdatingCount.value / StartCount * 100;
												$('#statusbar').css('width',nPercent+"%");
												if(aParams.prices_is_updating.value == 2){
													location.url('http://rps61.ru/admin/settings');
												}
												if(aParams.PricesForUpdatingCount.value<1){
													clearInterval(interval);
												}
											});
										},1000);		
									});												
								});				
							</script>
						{/literal}
						{/if}
					</p>
				</div>
                <div class="grid_16">
                    <p class="submit">
                        <input name="setValutes" type="submit" value="Сохранить" />
                        <input name="recalcValPrices" type="submit" value="Пересчитать" />
                    </p>
                </div>
            </form>
            {/if}
        </div>
		
			
{include file="default/bottom.tpl"}

{include file="default/footer.tpl"}