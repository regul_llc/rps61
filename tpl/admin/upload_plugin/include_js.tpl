{$strPluginPath='/public/upload-plugin/'}

{literal}

	<!-- The template to display files available for upload -->
	<script id="template-upload" type="text/x-tmpl">
	{% for (var i=0, file; file=o.files[i]; i++) { %}
	    <tr class="template-upload fade">
	        <td style="width: 150px;">
	            <span class="preview"></span>
	        </td>
	        <td style="text-align: left; vertical-align: middle;">
	            <b>{%=file.name%}</b>
	            {% if (file.error) { %}
	                <div><span class="label label-important">Error</span> {%=file.error%}</div>
	            {% } %}
	        </td>
	        <td style="text-align: left; vertical-align: middle;">
	            <p class="size">{%=o.formatFileSize(file.size)%}</p>
	            {% if (!o.files.error) { %}
	                <div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="bar" style="width:0%;"></div></div>
	            {% } %}
	        </td>
	        {% if ($('#item_type').attr('item_type')=='goods') { %}
	        	<td>
	        		
	        	</td>
	        {% } %}
	        <td style="text-align: right; vertical-align: middle;">
	            {% if (!o.files.error && !i && !o.options.autoUpload) { %}
	                <button class="btn btn-primary start">
	                    <span>Загрузить</span>
	                </button>
	            {% } %}
	            {% if (!i) { %}
	                <button style="margin-top: 10px;" class="btn btn-warning cancel">
	                    <span>Отменить</span>
	                </button>
	            {% } %}
	        </td>
	        <td></td>
	    </tr>
	{% } %}
	</script>
	<!-- The template to display files available for download -->
	<script id="template-download" type="text/x-tmpl">
	{% for (var i=0, file; file=o.files[i]; i++) { %}
	    <tr class="template-download fade">
	        <td style="width: 150px;">
	            <span class="preview">
	                {% if (file.thumbnailUrl) { %}
	                    <a class="fancybox" href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
	                {% } %}
	            </span>
	        </td>
	        <td style="text-align: left; vertical-align: middle;">
	            <p class="name">
	                <b>{%=file.name%}</b> (<span class="size">{%=file.size%}b</span>)
	            </p>
	            {% if (file.error) { %}
	                <div><span class="label label-important">Ошибка</span> {%=file.error%}</div>
	            {% } %}
	        </td>
	        <td style="text-align: center; vertical-align: middle;">
	            <textarea img_id="{%=file.img_id%}" class="img_title_editor" style="width: 100%; height: 90px;">{%=file.title%}</textarea>
	        </td>
	        {% if ($('#item_type').attr('item_type')=='goods') { %}
	        	<td style="text-align: left; vertical-align: middle; width: 100px;">
	        		Обложка: <input disabled {% if ($('#item_id').attr('cover')==file.name) { %}checked{% } %} img="{%=file.thumbnailUrl%}" type="radio" class="cover" name="cover" value="{%=file.img_id%}" />
	        	</td>
	        {% } %}
	        <td style="text-align: right; vertical-align: middle;">
	            <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
	                <span>Удалить</span>
	            </button>
	        </td>
	        <td style="text-align: right; vertical-align: middle;">
	        	<input type="checkbox" name="delete" value="1" class="toggle">
	        </td>
	    </tr>
	{% } %}
	</script>
	
{/literal}


{*<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>*}


<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="{$strPluginPath}js/vendor/jquery.ui.widget.js"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="{$strPluginPath}js/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="{$strPluginPath}js/load-image.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="{$strPluginPath}js/canvas-to-blob.min.js"></script>
<!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
<script src="{$strPluginPath}js/bootstrap.min.js"></script>
<!-- blueimp Gallery script -->
<script src="{$strPluginPath}js/blueimp-gallery.min.js"></script>
<script src="{$strPluginPath}js/jquery.blueimp-gallery.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="{$strPluginPath}js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="{$strPluginPath}js/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="{$strPluginPath}js/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="{$strPluginPath}js/jquery.fileupload-image.js"></script>
<!-- The File Upload audio preview plugin -->
<script src="{$strPluginPath}js/jquery.fileupload-audio.js"></script>
<!-- The File Upload video preview plugin -->
<script src="{$strPluginPath}js/jquery.fileupload-video.js"></script>
<!-- The File Upload validation plugin -->
<script src="{$strPluginPath}js/jquery.fileupload-validate.js"></script>
<!-- The File Upload user interface plugin -->
<script src="{$strPluginPath}js/jquery.fileupload-ui.js"></script>
<!-- The main application script -->
<script src="{$strPluginPath}js/main.js"></script>
<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE8+ -->
<!--[if gte IE 8]>
<script src="{$strPluginPath}js/cors/jquery.xdr-transport.js"></script>
<![endif]-->

<script src="{$strPluginPath}js/jquery.fancybox.pack.js"></script>
