{include file="default/header.tpl"}

<h1 id="head">Панель управления "{$strSiteName}"</h1>
		
	{include file="default/menu.tpl"}

	<div id="content" class="container_16 clearfix">
	
		<div class="grid_16">
			<h2><a href="{$strPath}/categories">Категории</a> :: <a href="{$strPath}/categories/{$aCategory.id}">{$strBreadCrumbs}</a> :: {$strPageName}</h2>
			{if $nAction==1}<p class="success">Ваши изменения сохранены</p>{/if}
			{if $nAction==2}<p class="error">При сохранении произошла ошибка</p>{/if}
			{if $nAction==3}<p class="success">Элемент успешно добавлен</p>{/if}
		</div>
	
		<form method="POST" action="">
			<div class="grid_16">
				<p>
					<label>Добавить свойство <small>Имя свойства, Флаг фильтрации, единицы измерения, приоритет, тип</small></label>
					<table width="100%">
					<td style="width: 340px;">
						<input style="width: 240px;" type="text" name="newvaluestpl[name]" value="" />
					</td>
					<td>
						<input style="width: 10px;" type="checkbox" name="newvaluestpl[filtered]" />
					</td>
					<td>
						<input style="width: 100px;" type="text" name="newvaluestpl[units]" value="" />
					</td>
                                        <td>
                                                <input style="width: 100px;" type="text" name="newvaluestpl[priority]" value="" />
                                        </td>
					<td>
						<select style="width: 200px;" name="newvaluestpl[type]">
							{foreach from=$aValueTypes key=key item=item}
								<option value="{$key}">{$item}</option>
							{/foreach}
						</select>
					</td>
					<td>
						<input style="margin-top: -12px;" type="submit" value="Добавить" />
					</td>
					</table>
				</p>
			</div>
		</form>
		<br>&nbsp;
		<br>
		<form method="POST" action="">
		
			<div class="grid_16">
				<table style="margin: 0 0 0 20px;" width="100%">
				<tr>
				<td style="width: 210px;">
					<b>Свойство</b>
				</td>
				<td style="width: 40px;">
					<b>Фильтр</b>
				</td>
				<td style="width: 100px;">
					<b>Ед. измерения</b>
				</td>
                                <td style="width: 50px;">
                                        <b>Приоритет</b>
                                </td>
				<td style="text-align: center;">	
					<b>Тип</b>
				</td>
				<td>
				</td>
				</tr>
				</table>
			</div>
			{foreach from=$aValuesTpl item=item}
			
				<div class="grid_16">
					<table style="margin: 0;" width="100%">
					<td style="width: 240px;">
						<input style="width: 240px;" type="text" name="valuestpl[{$item.id}][name]" value="{$item.name}" />
					</td>
					<td style="width: 20px;">
						<input style="" type="checkbox" name="valuestpl[{$item.id}][filtered]" {if $item.filtered eq 1}checked{/if} />
					</td>
					<td style="width: 100px;">
						<input style="width: 100px;" type="text" name="valuestpl[{$item.id}][units]" value="{$item.units}" />
					</td>
                                        <td style="width: 50px;">
                                                <input style="width: 50px;" type="text" name="valuestpl[{$item.id}][priority]" value="{$item.priority|intval}" />
                                        </td>
					<td width="100%" style="text-align: center;">	
						<b>[{$aValueTypes[$item.type]}]</b>
						{if $item.type eq 8}
							<br>Введите значения через точку с запятой:<br>
							<input style="width: 200px;" type="text" name="valuestpl[{$item.id}][values]" value="{$item.values}" />
						{/if}
					</td>
					<td align="right">
						<a class="del_value" href="{$strPath}/delvaluetpl/{$item.id}">удалить</a>
					</td>
					</table>
				</div>				
				
			{/foreach}
			
			<div class="grid_16">
				<p class="submit">
					<input type="submit" name="reset" value="Назад" />
					<input type="submit" value="Сохранить" />
				</p>
			</div>
			
		</form>
	
	</div>

{include file="default/bottom.tpl"}

{include file="default/footer.tpl"}