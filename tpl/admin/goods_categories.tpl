{include file="default/header.tpl"}

<h1 id="head">Панель управления "{$strSiteName}"</h1>
		
	{include file="default/menu.tpl"}

	<div id="content" class="container_16 clearfix">
	
		<div class="grid_16">
			<h2>{$strPageName}</h2>
			{if isset($aCategories)}
				<div class="utils">
					<a href="{$strPath}/{$strPageCode}/new" style="margin: 13px 20px 0 0;">Добавить категорию</a>
				</div>
			{/if}
			
			{if $nAction==1}<p class="success">Ваши изменения сохранены</p>{/if}
			{if $nAction==2}<p class="error">При сохранении произошла ошибка</p>{/if}
		</div>
		
		{if isset($aCategories)}
		
			<div class="grid_16">
				<table>
					<thead>
						<tr>
							<th width="50%">Название</th>
							<th>Публикация</th>	
							<th></th>	
							<th></th>	
							<th colspan="3" width="10%">Действия</th>
						</tr>
					</thead>
					{*
					<tfoot>
						<tr>
							<td colspan="5" class="pagination">
								<span class="active curved">1</span><a href="#" class="curved">2</a><a href="#" class="curved">3</a><a href="#" class="curved">4</a> ... <a href="#" class="curved">10 million</a>
							</td>
						</tr>
					</tfoot>
					*}
					<tbody>
					
			{foreach from=$aCategories item=item}
			
						<tr>
							<td style="{if $item.level==2}padding-left: 30px;{/if}">
								<b>{$item.title}</b>
							{*
							{if $item.level==2}<a href="{$strPath}/goods/{$item.id}">{else}<b>{/if}
								{$item.title}
								{if $item.level==2}</a>{else}</b>{/if}
							*}
								
							</td>
							<td style="white-space: nowrap; text-align: center;">{if $item.visible==1}да{else}нет{/if}</td>
							<td>
								{if $item.level==1}
									<a href="{$strPath}/categories/new/{$item.id}">+&nbsp;подкатегорию</a>
								{else}
									<a href="{$strPath}/goods/{$item.id}" class="edit">Товары&nbsp;({$item.count_goods})</a>
								{/if}
							</td>
							<td>
								{if $item.level==2}
									<a href="{$strPath}/values_tpl/{$item.id}" class="edit">Свойства</a>
								{/if}
							</td>
							<td>
								{if $item.level==2}
                                    <a title="Отметить все товары этой категории что их нет в наличии" href="{$strPath}/{$strPageCode}/{$item.id}/empty" class="empty">Нет&nbsp;в&nbsp;наличии</a>&nbsp;/&nbsp;
                                    <a title="Отметить все товары этой категории что они в наличии" href="{$strPath}/{$strPageCode}/{$item.id}/fill" class="fill">Под&nbsp;заказ</a>
								{/if}
							</td>
							<td><a href="{$strPath}/{$strPageCode}/{$item.id}" class="edit">Редактировать</a></td>
							<td><a href="{$strPath}/{$strPageCode}/{$item.id}/del" class="delete">Удалить</a></td>
						</tr>
				
			{/foreach}
			
					</tbody>
				</table>
			</div>
			
		{elseif isset($aCategory)}
			{*Редактирование категории*}
			<form method="POST" action="">
				<div class="grid_16">
					<p>
						<label>Публикация <small>неопубликованные товары не выводятся на сайте</small></label>
						<input style="width: 20px;" type="checkbox" name="category[visible]" {if $aCategory.visible}checked{/if} />
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Заголовок <small>не длиннее 255 символов</small></label>
						<input type="text" name="category[title]" value="{$aCategory.title}" />
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Текст <small>подробное описание рубрики</small></label>
						<textarea class="editor" name="category[about]">{$aCategory.about}</textarea>
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Мета-заголовок <small>заголовок окна браузера не длиннее 255 символов</small></label>
						<input type="text" name="category[meta][title]" value="{$aCategory.meta.title|default:''}" />
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Мета-описание <small></small></label>
						<textarea style="height: 100px; max-width: 935px;" name="category[meta][description]">{$aCategory.meta.description|default:''}</textarea>
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Мета-слова <small>ключевые слова для поисковых систем</small></label>
						<textarea style="height: 100px; max-width: 935px;" name="category[meta][keywords]">{$aCategory.meta.keywords|default:''}</textarea>
					</p>
				</div>
			
				<div class="grid_16">
					<p class="submit">
						<input type="submit" name="reset" value="Назад">
						<input type="submit" value="Сохранить" />
					</p>
				</div>
				
			</form>
			
		{else}
			{*Создание категории*}
			<form method="POST" action="">
			
				<div class="grid_16">
					<p>
						<label>Публикация <small>не опубликованные категории не выводятся на сайте</small></label>
						<input style="width: 20px;" type="checkbox" name="category[visible]" />
					</p>
				</div>
			
				<div class="grid_16">
					<p>
						<label>Заголовок <small>не длиннее 255 символов</small></label>
						<input type="text" name="category[title]" value="" />
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Текст <small>подробное описание рубрики</small></label>
						<textarea class="editor" name="category[about]"></textarea>
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Мета-заголовок <small>заголовок окна браузера не длиннее 255 символов</small></label>
						<input type="text" name="category[meta][title]" />
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Мета-описание <small></small></label>
						<textarea style="height: 100px; max-width: 935px;" name="category[meta][description]"></textarea>
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Мета-слова <small>ключевые слова для поисковых систем</small></label>
						<textarea style="height: 100px; max-width: 935px;" name="category[meta][keywords]"></textarea>
					</p>
				</div>
				
				<div class="grid_16">
					<p class="submit">
						<input type="submit" name="reset" value="Назад">
						<input type="submit" value="Добавить" />
					</p>
				</div>
				
			</form>
			
		{/if}
	
	
	</div>


{include file="default/bottom.tpl"}

{include file="default/footer.tpl"}