<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<title>Панель управления "{$strSiteName}" - {$strPageName}</title>
		<link rel="stylesheet" href="/public/css/admin/960.css" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" href="/public/css/admin/template.css" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" href="/public/css/admin/colour.css" type="text/css" media="screen" charset="utf-8" />
                <link rel="stylesheet" href="/public/js/libs/jquery-ui/jquery-ui.css" type="text/css" media="screen" charset="utf-8" />
		

                <!--[if IE]><![if gte IE 6]><![endif]-->
		{*<script src="/public/js/libs/jquery-1.9.1.min.js" type="text/javascript"></script>*}
                <script src="/public/js/libs/jquery-1.11.3.min.js" type="text/javascript"></script>
		<script src="/public/js/libs/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
                <script src="/public/js/libs/tinymce/tinymce.min.js" type="text/javascript"></script>
		<script src="/public/js/libs/jquery.maskedinput-1.3.js"></script>
		<script src="/public/js/admin.js" type="text/javascript"></script>
                
                <!--Editor Plugin-->
                {*<script src="/public/js/libs/Editor-1.5.4/js/dataTables.editor.min.js"></script>
                <script src="/public/js/libs/Editor-1.5.4/js/editor.bootstrap.min.js"></script>
                <script src="/public/js/libs/Editor-1.5.4/js/editor.foundation.min.js"></script>
                <script src="/public/js/libs/Editor-1.5.4/main.js"></script>*}
                
                {*<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/s/dt/jqc-1.11.3,moment-2.10.6,dt-1.10.10,b-1.1.0,se-1.1.0/datatables.min.css">
		<link rel="stylesheet" type="text/css" href="css/generator-base.css">
		<link rel="stylesheet" type="text/css" href="/public/js/libs/Editor-1.5.4/css/editor.dataTables.min.css">

		<script type="text/javascript" charset="utf-8" src="https://cdn.datatables.net/s/dt/jqc-1.11.3,moment-2.10.6,dt-1.10.10,b-1.1.0,se-1.1.0/datatables.min.js"></script>
		<script src="/public/js/libs/Editor-1.5.4/js/dataTables.editor.min.js"></script>
		<script src="/public/js/libs/Editor-1.5.4/main.js"></script>*}
                <!--END Editor Plugin-->
                
                <!-- Для ajax редактирования таблицы -->
                <script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
                <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" />
                <script src="/public/js/libs/Editor-1.5.4/main.js"></script>
		
		{include file="upload_plugin/include_css.tpl"}

		<script type="text/javascript">
			$(function() {
				$("#content .grid_5, #content .grid_6").sortable({
					placeholder: 'ui-state-highlight',
					forcePlaceholderSize: true,
					connectWith: '#content .grid_6, #content .grid_5',
					handle: 'h2',
					revert: true
				});
				$("#content .grid_5, #content .grid_6").disableSelection();
			});
		</script>
		<!--[if IE]><![endif]><![endif]-->
		
		<style type="text/css">
			textarea {
				height: 300px;
			}
		</style>
	</head>
	<body>