<ul id="navigation">
	{foreach from=$aMenu item=item key=key}
		{if 
			$key == $strPageCode || 
			($key=='categories' && $strPageCode=='goods') || 
			($key=='categories' && $strPageCode=='values_tpl') ||
			($key=='work_categories' && $strPageCode=='works')			
		}
			<li><span class="active">{$item}</span></li>
		{else}
			<li><a href="{$strPath}/{$key}">{$item}</a></li>
		{/if}
	{/foreach}
</ul>