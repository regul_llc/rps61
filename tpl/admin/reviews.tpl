{include file="default/header.tpl"}

<h1 id="head">Панель управления "{$strSiteName}"</h1>
		
	{include file="default/menu.tpl"}

	<div id="content" class="container_16 clearfix">
	
		<div class="grid_16">
			<h2>{$strPageName}</h2>
			
			{if $nAction==1}<p class="success">Ваши изменения сохранены</p>{/if}
			{if $nAction==2}<p class="error">При сохранении произошла ошибка</p>{/if}
		</div>

		{if isset($aReviews)}
		
			<div class="grid_16">
				<table>
					<thead>
						<tr>
							<th>Автор</th>
							<th>Сообщение</th>
							<th>Дата</th>
							<th>Публикация</th>							
							<th colspan="3" width="10%">Действия</th>
						</tr>
					</thead>
					{*
					<tfoot>
						<tr>
							<td colspan="5" class="pagination">
								<span class="active curved">1</span><a href="#" class="curved">2</a><a href="#" class="curved">3</a><a href="#" class="curved">4</a> ... <a href="#" class="curved">10 million</a>
							</td>
						</tr>
					</tfoot>
					*}
					<tbody>
					
			{foreach from=$aReviews item=item}
			
						<tr>
							<td>{$item.author}</td>
							<td>{$item.text|strip_tags:false|truncate:50:'..'}</td>
							<td style="white-space: nowrap;">{$item.date|date_format:"%D %H:%M"}</td>
							<td style="white-space: nowrap;">{if $item.public==1}опубликовано{else}не опубликовано{/if}</td>
							<td><a href="{$strPath}/{$strPageCode}/{$item.id}" class="edit">Редактировать</a></td>
							<td><a href="{$strPath}/{$strPageCode}/{$item.id}/del" class="delete">Удалить</a></td>
						</tr>
				
			{/foreach}
			
					</tbody>
				</table>
			</div>
			
		{elseif isset($aReview)}
		
			<form method="POST" action="">
			
				<div class="grid_16">
					<p>
						<label>Модерация <small>разрешить ли выводить это сообщение на сайте?</small></label>
						<input style="width: 20px;" type="checkbox" name="reviews[public]" {if $aReview.public}checked{/if} />
					</p>
				</div>
				
				{if $aReview.image}
				
					<div class="grid_16">
						<p>
							<label>Изображение <small>которое загрузил пользователь</small></label>
							<img src="/public/img/reviews/{$aReview.image}">
						</p>
					</div>
					
				{/if}
				
				<div class="grid_16">
					<p>
						<label>Автор <small>автор сообщения</small></label>
						<b>{$aReview.author}</b>
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>EMail <small>email автора сообщения</small></label>
						<b>{$aReview.email}</b>
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Дата добавления <small></small></label>
						<b>{$aReview.date|date_format:"%D %H:%M"}</b>
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Текст <small>подробный текст сообщения</small></label>
						<textarea class="editor" name="reviews[text]">{$aReview.text}</textarea>
					</p>
				</div>
			
				<div class="grid_16">
					<p class="submit">
						<input type="submit" name="reset" value="Назад">
						<input type="submit" value="Сохранить" />
					</p>
				</div>
				
			</form>
					
		{/if}
	
	
	</div>

{include file="default/bottom.tpl"}

{include file="default/footer.tpl"}