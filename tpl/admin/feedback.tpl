{include file="default/header.tpl"}

<h1 id="head">Панель управления "{$strSiteName}"</h1>
		
	{include file="default/menu.tpl"}

	<div id="content" class="container_16 clearfix">
	
		<div class="grid_16">
			<h2>{$strPageName}</h2>
		</div>

		{if isset($aFeedbacks)}
		`       {*Список контактов*}
			<div class="grid_16">
				<table>
					<thead>
						<tr>
							<th>ФИО</th>
                                                        <th>Телефон</th>
							<th>Сообщение</th>
							<th>Дата</th>							
							<th colspan="3" width="10%">Действия</th>
						</tr>
					</thead>
					
					<tbody>
					
			{foreach $aFeedbacks as $item}
			
						<tr>
							<td>{$item.name}</td>
                                                        <td>{$item.phone}</td>
							<td>{$item.text|strip_tags:false|truncate:50:'..'}</td>
							<td style="white-space: nowrap;">{$item.date_add|date_format:"%D %H:%M"}</td>
							<td><a href="{$strPath}/{$strPageCode}/{$item.id}" class="edit">Читать полностью</a></td>
							<td><a href="{$strPath}/{$strPageCode}/{$item.id}/del" class="delete">Удалить</a></td>
						</tr>
				
			{/foreach}
			
					</tbody>
				</table>
			</div>
			
		{elseif isset($aFeedback)}
		<form method="POST" action="">
                    <p><b>ФИО:</b> {$aFeedback.name}</p>
                    <p><b>Телефон:</b> {$aFeedback.phone}</p>
                    <p><b>Сообщение:</b></p>
                    <p>{$aFeedback.text}</p>
                    <p class="submit"><input type="submit" name="reset" value="Назад"></p>
                </form>		
		{/if}
	
	
	</div>

{include file="default/bottom.tpl"}

{include file="default/footer.tpl"}