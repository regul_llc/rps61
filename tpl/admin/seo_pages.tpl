{include file="default/header.tpl"}

<script src="/public/js/seo.js" type="text/javascript"></script>

<h1 id="head">Панель управления "{$strSiteName}"</h1>
		
	{include file="default/menu.tpl"}

	<div id="content" class="container_16 clearfix">
	
		<div class="grid_16">
			<h2>{$strPageName}</h2>
			<div class="utils">
				<a href="{$strPath}/{$strPageCode}/new" style="margin: 13px 20px 0 0;">Добавить страницу</a>
			</div>
			{if $nAction==1}<p class="success">Ваши изменения сохранены</p>{/if}
			{if $nAction==2}<p class="error">При сохранении произошла ошибка</p>{/if}
		</div>
		
		{if isset($aItems)}

			<div class="grid_16">
				<table>
					<thead>
						<tr>
							<th>Название</th>
							<th colspan="2" width="10%">Действия</th>
						</tr>
					</thead>
					{*
					<tfoot>
						<tr>
							<td colspan="5" class="pagination">
								<span class="active curved">1</span><a href="#" class="curved">2</a><a href="#" class="curved">3</a><a href="#" class="curved">4</a> ... <a href="#" class="curved">10 million</a>
							</td>
						</tr>
					</tfoot>
					*}
					<tbody>
					
			{foreach from=$aItems item=item}
			
						<tr>
							<td><a href="{$strPath}/{$strPageCode}/{$item.id}">{$item.title}</a></td>
							<td><a href="{$strPath}/{$strPageCode}/{$item.id}" class="edit">Редактировать</a></td>
							<td><a href="{$strPath}/{$strPageCode}/{$item.id}/del" class="delete">Удалить</a></td>
						</tr>
				
			{/foreach}
			
					</tbody>
				</table>
			</div>
			
		{elseif isset($aItem)}
			{*редактирование статей*}
			<form method="POST" action="" enctype="multipart/form-data">
				<div class="grid_16">
					<p>
						<label>Заголовок <small>не длиннее 255 символов</small></label>
						<input type="text" name="seo_page[title]" value="{$aItem.title}" />
					</p>
				</div>

				<div class="grid_16">
					<p>
						<label>Код страницы <small>если оставить пустым, будет сгенерирован из заголовка</small></label>
						<input type="text" name="seo_page[url]" value="{$aItem.url}" />
					</p>
				</div>

                <div class="grid_16">
                    <p>
                        <label>Мета-заголовок <small>заголовок окна браузера не длиннее 255 символов</small></label>
                        <input type="text" name="seo_page[meta_title]" value="{$aItem.meta_title}" />
                    </p>
                </div>

                <div class="grid_16">
                    <p>
                        <label>Мета-описание <small></small></label>
                        <input type="text" name="seo_page[meta_description]" value="{$aItem.meta_description}">
                    </p>
                </div>

                <div class="grid_16">
                    <p>
                        <label>Мета-слова <small>ключевые слова для поисковых систем</small></label>
                        <input type="text" name="seo_page[meta_keywords]" value="{$aItem.meta_keywords}">
                    </p>
                </div>
				
				<div class="grid_16">
					<p>
						<label>Текст <small>полный текст</small></label>
						<textarea class="editor" name="seo_page[text]">{$aItem.text}</textarea>
					</p>
				</div>

                <div class="grid_16" style="margin-top: 40px;">
                    <p>
                        <label>Настройки вывода товаров <small>Внимание! Что бы изменения вступили в силу надо нажать на кнопку "Сохранить" </small></label>

                    <table>
                        <tr>
                            <th style="width: 300px; text-align: center;">
                                Категория
                            </th>
                            <th style="text-align: center;" class="sub_cat_boxs">
                                Поля
                            </th>
                        </tr>

                        <tr>
                            <td>
                                <select id="filter_cat" style="width: 300px; height: 200px;" name="seo_page[filter][cat][]" multiple>
                                    {foreach from=$aCats item=aCat}
                                        <optgroup label="{$aCat.title}">
                                            {foreach from=$aCat.childs item=aSubCat}
                                                <option {if in_array($aSubCat.id, $aItem.filter.cat)}selected{/if} style="padding-left: 20px;" value="{$aSubCat.id}">{$aSubCat.title}</option>
                                            {/foreach}
                                        </optgroup>
                                    {/foreach}
                                </select>
                            </td>
                            <td style="vertical-align: top;" class="sub_cat_boxs" id="filter_fields">
                                {foreach from=$aFields item=item}
                                    <div style="float: right; padding: 5px; margin: 5px; background-color: #dadada;">
                                        <input {if isset($aItem.filter.fields) && in_array($item.name, $aItem.filter.fields)}checked{/if} onchange="checkField('{$item.name}')" style="float: left;" type="checkbox" value="{$item.name}" name="seo_page[filter][fields][]" id="ff{str_replace(' ', '_', $item.name)}"/>
                                        <label style="float: left; margin: 0 0 0 5px; padding: 0;" for="ff{str_replace(' ', '_', $item.name)}">{$item.name}</label>
                                    </div>
                                {/foreach}

                            </td>
                        </tr>
                    </table>

                    <table>
                        <tr>
                            <th style="text-align: center;">
                                Фильтры
                            </th>
                        </tr>
                        <tr>
                            <td>
                                <input name="seo_page[filter][condition]" id="filter_condition" style="width: 300px; border-color: #6ebcff" type="text" value="{$aItem.filter.condition}" /> <small>AND - И, OR - ИЛИ</small>
                            </td>
                        </tr>
                        <tr>
                            <td id="filters_list">
                                {foreach from=$aItem.filters item=aFilter}
                                    <!--{$aCurFilters = $aItem.filter.filter}-->
                                    {include file="filter.tpl"}

                                {/foreach}
                            </td>
                        </tr>
                    </table>

                    <table>
                        <tr>
                            <th style="text-align: center;">
                                Сортировка
                            </th>
                            <th style="text-align: center;">
                                Порядок
                            </th>
                            <th style="text-align: center;">
                                Показывать навигатор
                            </th>
                            <th style="text-align: center;">
                                Товаров на странице
                            </th>
                            {*<th style="text-align: center;">
                                Товаров всего (0 - выводить все)
                            </th>*}
                        </tr>
                        <tr>

                            <td style="text-align: center;">
                                <select name="seo_page[filter][orderby]" style="width: 200px;" id="filter_order_by">
                                    {foreach from=$aFields item=item}
                                        <option {if $aItem.filter.orderby == $item.name}selected{/if}>{$item.name}</option>
                                    {/foreach}
                                </select>
                            </td>
                            <td style="text-align: center;">
                                <select name="seo_page[filter][order]" style="width: 150px;">
                                    <option {if $aItem.filter.order == 'ASC'}selected{/if} value="ASC">По возрастанию</option>
                                    <option {if $aItem.filter.order == 'DESC'}selected{/if} value="DESC">По убыванию</option>
                                    <option {if $aItem.filter.order == 'RAND()'}selected{/if} value="RAND()">Случайно</option>
                                </select>
                            </td>
                            <td style="text-align: center;">
                                <input {if isset($aItem.filter.nav) && $aItem.filter.nav}checked{/if} type="checkbox" name="seo_page[filter][nav]" />
                            </td>
                            <td style="text-align: center;">
                                <input value="{$aItem.filter.ppage}" style="width: 30px;" type="text" name="seo_page[filter][ppage]" />
                            </td>
                            {*<td style="text-align: center;">
                                <input value="{$aItem.filter.limit}" style="width: 30px;" type="text" name="seo_page[filter][limit]" />
                            </td>*}
                        </tr>

                    </table>
                    </p>
                </div>

				<div class="grid_16">
					<p class="submit">
						<input type="submit" name="reset" value="Назад">
						<input type="submit" value="Сохранить" />
					</p>
				</div>
				
			</form>

			
		{else}

			<form method="POST" action="" enctype="multipart/form-data">

                <div class="grid_16">
                    <p>
                        <label>Заголовок <small>не длиннее 255 символов</small></label>
                        <input type="text" name="seo_page[title]" value="" />
                    </p>
                </div>

                <div class="grid_16">
                    <p>
                        <label>Код страницы <small>если оставить пустым, будет сгенерирован из заголовка</small></label>
                        <input type="text" name="seo_page[url]" value="" />
                    </p>
                </div>

                <div class="grid_16">
                    <p>
                        <label>Мета-заголовок <small>заголовок окна браузера не длиннее 255 символов</small></label>
                        <input type="text" name="seo_page[meta_title]" value="" />
                    </p>
                </div>

                <div class="grid_16">
                    <p>
                        <label>Мета-описание <small></small></label>
                        <input type="text" name="seo_page[meta_description]" value="">
                    </p>
                </div>

                <div class="grid_16">
                    <p>
                        <label>Мета-слова <small>ключевые слова для поисковых систем</small></label>
                        <input type="text" name="seo_page[meta_keywords]" value="">
                    </p>
                </div>

                <div class="grid_16">
                    <p>
                        <label>Текст <small>полный текст</small></label>
                        <textarea class="editor" name="seo_page[text]"></textarea>
                    </p>
                </div>

                <div class="grid_16" style="margin-top: 40px;">
                    <p>
                        <label>Настройки вывода товаров <small> </small></label>

                        <table>
                            <tr>
                                <th style="width: 300px; text-align: center;">
                                    Категория
                                </th>
                                <th style="text-align: center;" class="sub_cat_boxs">
                                    Поля
                                </th>
                            </tr>

                            <tr>
                                <td>
                                    <select id="filter_cat" style="width: 300px; height: 200px;" name="seo_page[filter][cat][]" multiple>
                                        {foreach from=$aCats item=aCat}
                                            <optgroup label="{$aCat.title}">
                                                {foreach from=$aCat.childs item=aSubCat}
                                                    <option style="padding-left: 20px;" value="{$aSubCat.id}">{$aSubCat.title}</option>
                                                {/foreach}
                                            </optgroup>
                                        {/foreach}
                                    </select>
                                </td>
                                <td style="vertical-align: top;" class="sub_cat_boxs" id="filter_fields">

                                </td>
                            </tr>
                        </table>

                        <table>
                            <tr>
                                <th style="text-align: center;">
                                    Фильтры
                                </th>
                            </tr>
                            <tr>
                                <td>
                                    <input name="seo_page[filter][condition]" id="filter_condition" style="width: 300px; border-color: #6ebcff" type="text" value="" /> <small>AND - И, OR - ИЛИ</small>
                                </td>
                            </tr>
                            <tr>
                                <td id="filters_list">
                                </td>
                            </tr>
                        </table>

                        <table>
                                <tr>
                                    <th style="text-align: center;">
                                        Сортировка
                                    </th>
                                    <th style="text-align: center;">
                                        Порядок
                                    </th>
                                    <th style="text-align: center;">
                                        Показывать навигатор
                                    </th>
                                    <th style="text-align: center;">
                                        Товаров на странице
                                    </th>
                                    {*<th style="text-align: center;">
                                        Товаров всего (0 - выводить все)
                                    </th>*}
                                </tr>
                                <tr>

                                <td style="text-align: center;">
                                    <select name="seo_page[filter][orderby]" style="width: 200px;" id="filter_order_by">

                                    </select>
                                </td>
                                <td style="text-align: center;">
                                    <select name="seo_page[filter][order]" style="width: 150px;">
                                       <option value="ASC">По возрастанию</option>
                                       <option value="DESC">По убыванию</option>
                                       <option value="RAND()">Случайно</option>
                                    </select>
                                </td>
                                <td style="text-align: center;">
                                    <input type="checkbox" name="seo_page[filter][nav]" />
                                </td>
                                <td style="text-align: center;">
                                    <input style="width: 30px;" type="text" name="seo_page[filter][ppage]" />
                                </td>
                                {*<td style="text-align: center;">
                                    <input style="width: 30px;" type="text" name="seo_page[filter][limit]" />
                                </td>*}
                            </tr>

                        </table>




                    </p>
                </div>
                            
				<div class="grid_16" style="margin-top: 40px;">
					<p class="submit">
						<input type="submit" name="reset" value="Назад">
						<input type="submit" value="Добавить" />
					</p>
				</div>


				
			</form>
			
		{/if}
	
	
	</div>

{include file="default/bottom.tpl"}

{include file="default/footer.tpl"}