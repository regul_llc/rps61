<div style="width: 100%; clear: both;" iid="{$aFilter.id}" ttl="{$aFilter.name}" id="filter{str_replace(' ', '_', $aFilter.name)}" class="filter_box">
    <div style="margin: 5px; padding: 5px; background-color: #6ebcff; width: 13px; float: left;">
        <b><span class="num">{if isset($strNum)}{$strNum}{/if}</span></b>
    </div>
    <div style="margin: 5px; padding: 5px; width: 300px; float: left;">
        <b>{$aFilter.name}</b>
    </div>
    <div style="margin: 5px; padding: 5px; width: 400px; float: left;">

        {if $aFilter.type == 1}

            <input style="width: 300px;" value="{if isset($aCurFilters[{$aFilter.name}])}$aCurFilters[{$aFilter.name}]{/if}" type="text" name="seo_page[filter][filter][{$aFilter.name}]" />

        {elseif $aFilter.type == 3}

            <input style="width: 75px;" {if isset($aCurFilters[{$aFilter.name}])}checked{/if} type="checkbox" name="seo_page[filter][filter][{$aFilter.name}]" />

        {elseif $aFilter.type == 4 || $aFilter.type == 6 || $aFilter.type == 9 || $aFilter.type == 10}

            от <input style="width: 75px;" name="seo_page[filter][filter][{$aFilter.name}][from]" value="{if isset($aCurFilters[{$aFilter.name}].from)}{$aCurFilters[{$aFilter.name}].from}{/if}" id="tfA_1" type="text">
            до <input style="width: 75px;" name="seo_page[filter][filter][{$aFilter.name}][to]" value="{if isset($aCurFilters[{$aFilter.name}].to)}{$aCurFilters[{$aFilter.name}].to}{/if}" id="tfA_2" type="text">

        {elseif $aFilter.type == 7 || $aFilter.type == 8}

            <select style="height: 100px; width: 390px;" name="seo_page[filter][filter][{$aFilter.name}][]" multiple>
                {foreach from=$aFilter.values item=i}
                    <option {if isset($aCurFilters[{$aFilter.name}]) && in_array($i, $aCurFilters[{$aFilter.name}])}selected{/if}>{$i}</option>
                {/foreach}
            </select>

        {/if}

    </div>
</div>