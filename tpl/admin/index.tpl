{include file="default/header.tpl"}

<h1 id="head">Панель управления "{$strSiteName}"</h1>
		
	{include file="default/menu.tpl"}

	<div id="content" class="container_16 clearfix">
	
		<div class="grid_16">
			<h2>{$strPageName}</h2>
			{if $nAction==1}<p class="success">Ваши изменения сохранены</p>{/if}
			{if $nAction==2}<p class="error">При сохранении произошла ошибка</p>{/if}
		</div>
	
		<form method="POST" action="">
		{$aDisOpts = array(18, 3, 14, 16, 10, 11, 19)}
			{foreach from=$aOptions item=item}
                            
				{if $item.type == 1}
				
					<div class="grid_16">
						<p>
							<label>{$item.name} <small>{$item.title}</small></label>
							<input type="text" name="options[{$item.id}]" value="{$item.value}" />
						</p>
					</div>
					
				{elseif $item.type == 2 && !$item.id|in_array:$aDisOpts}
				
					<div class="grid_16">
						<p>
							<label>{$item.name} <small>{$item.title}</small></label>
							<textarea {if $item.id == 21 || $item.id == 22}style="height: 100px; max-width: 935px;"{else}class="editor"{/if} type="text" name="options[{$item.id}]">{$item.value|stripslashes}</textarea>
						</p>
					</div>
					
				{elseif $item.type == 3}
				
					<div class="grid_16">
						<p>
							<label>{$item.name} <small>{$item.title}</small></label>
							<select style="width: 60px;" name="options[{$item.id}]">
								<option {if $item.value==1}selected{/if} value="1">Да</option>
								<option {if $item.value==0}selected{/if} value="0">Нет</option>
							</select>
						</p>
					</div>
					
				{elseif $item.type == 4}
				
					<div class="grid_16">
						<p>
							<label>{$item.name} <small>{$item.title}</small></label>
							<input style="width: 90px;" type="text" name="options[{$item.id}]" value="{$item.value}" />
						</p>
					</div>
					
				{elseif $item.type == 5}
				
					<div class="grid_16">
						<p>
							<label>{$item.name} <small>{$item.title}</small></label>
							<input class="phone" style="width: 130px;" type="text" name="options[{$item.id}]" value="{$item.value}" />
						</p>
					</div>
					
				{/if}
				
			{/foreach}
			
			<div class="grid_16">
				<p class="submit">
					<input type="submit" value="Сохранить" />
				</p>
			</div>
			
		</form>
		
		{if isset($strImageType) && $strImageType == 'main'}
			<input id="item_id" type="hidden" item_id="1" />
			<input id="item_type" type="hidden" item_type="{$strImageType}" />
			
			<div style="clear: both; width: 100%; height: 30px;"></div>
			<div class="grid_16">
				<p>
					<label>Изображения <small>выводятся в главном слайдере, все изменения происходят сразу, не нужно нажимать кнопку "сохранить"</small></label>
					{include file="upload_plugin/loader.tpl"}
				</p>
			</div>
			
			{include file="upload_plugin/include_js.tpl"}
		{/if}
	
	</div>

{include file="default/bottom.tpl"}

{include file="default/footer.tpl"}