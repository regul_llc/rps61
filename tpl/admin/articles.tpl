{include file="default/header.tpl"}

<h1 id="head">Панель управления "{$strSiteName}"</h1>
		
	{include file="default/menu.tpl"}

	<div id="content" class="container_16 clearfix">
	
		<div class="grid_16">
			<h2>{$strPageName}</h2>
			<div class="utils">
				<a href="{$strPath}/{$strPageCode}/new" style="margin: 13px 20px 0 0;">Добавить статью</a>
			</div>
			{if $nAction==1}<p class="success">Ваши изменения сохранены</p>{/if}
			{if $nAction==2}<p class="error">При сохранении произошла ошибка</p>{/if}
		</div>
		
		{if isset($aArticles)}
			{*cписок статей*}
			<div class="grid_16">
				<table>
					<thead>
						<tr>
							<th>Название</th>
							<th colspan="2" width="10%">Действия</th>
						</tr>
					</thead>
					{*
					<tfoot>
						<tr>
							<td colspan="5" class="pagination">
								<span class="active curved">1</span><a href="#" class="curved">2</a><a href="#" class="curved">3</a><a href="#" class="curved">4</a> ... <a href="#" class="curved">10 million</a>
							</td>
						</tr>
					</tfoot>
					*}
					<tbody>
					
			{foreach from=$aArticles item=item}
			
						<tr>
							<td><a href="{$strPath}/{$strPageCode}/{$item.id}">{$item.title}</a></td>
							<td><a href="{$strPath}/{$strPageCode}/{$item.id}" class="edit">Редактировать</a></td>
							<td><a href="{$strPath}/{$strPageCode}/{$item.id}/del" class="delete">Удалить</a></td>
						</tr>
				
			{/foreach}
			
					</tbody>
				</table>
			</div>
			
		{elseif isset($aArticle)}
			{*редактирование статей*}
			<form method="POST" action="" enctype="multipart/form-data">
				<div class="grid_16">
					<p>
						<label>Заголовок <small>не длиннее 255 символов</small></label>
						<input type="text" name="article[title]" value="{$aArticle.title}" />
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Текст <small>полный текст статьи</small></label>
						<textarea class="editor" name="article[text]">{$aArticle.text}</textarea>
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Мета-заголовок <small>заголовок окна браузера не длиннее 255 символов</small></label>
						<input type="text" name="article[meta][title]" value="{$aArticle.meta.title|default:''}" />
					</p>
				</div>

				<div class="grid_16">
					<p>
						<label>Мета-описание <small></small></label>
						<textarea style="height: 100px; max-width: 935px;" name="article[meta][description]">{$aArticle.meta.description|default:''}</textarea>
					</p>
				</div>

				<div class="grid_16">
					<p>
						<label>Мета-слова <small>ключевые слова для поисковых систем</small></label>
						<textarea style="height: 100px; max-width: 935px;" name="article[meta][keywords]">{$aArticle.meta.keywords|default:''}</textarea>
					</p>
				</div>
                                
				<div class="grid_16">
					<p>
						<label>Доп. файлы <small>выберите файлы, чтобы прикрепить их к товару</small></label>
						<input name="additFiles[]" type="file" multiple="multiple" value="Прикрепить" />
					</p>
				</div>
                                        
				{if $aArticle.additFiles}
					<div class="grid_16">
						<table>
							<thead>
								<tr>
									<th>Файл</th>
									<th>Название</th>
									<th>Действие</th>
								</tr>
							</thead>
							<tbody>
								{foreach $aArticle.additFiles as $file}
									<tr>
										<td>
											<a href="/admin/articles/{$aArticle.id}/{$file.filename}"><img width="32" height="32" style="padding-right: 5px" src="/public/img/icon_{$file.icon}.png">{$file.filename}</a>
										</td>
										<td>
											<input style="width: 500px;" name="additFileName[{$file.id}][name]" type="text" value="{if !empty($file.name)}{$file.name}{/if}" />
										</td>
										<td>
											<a class="delete deleterFile" href="#" fid="{$file.id}" itid="{$aArticle.id}" fname="{$file.filename}" ittype="article">удалить</a>
										</td>
									</tr>
								{/foreach}
							</tbody>
						</table>
					</div>
				{/if}
			
				<div class="grid_16">
					<p class="submit">
						<input type="submit" name="reset" value="Назад">
						<input type="submit" value="Сохранить" />
					</p>
				</div>
				
			</form>
                                        
			<input id="item_id" type="hidden" item_id="{$aArticle.id}" />
			<input id="item_type" type="hidden" item_type="article" />

			<div style="clear: both; width: 100%; height: 30px;"></div>
			<div class="grid_16">
					<p>
							<label>Изображения <small>все изменения происходят сразу, не нужно нажимать кнопку "сохранить"</small></label>
							{include file="upload_plugin/loader.tpl"}
					</p>
			</div>
			{include file="upload_plugin/include_js.tpl"}
			
		{else}
			{*добавление статей*}
			<form method="POST" action="" enctype="multipart/form-data">
			
				<div class="grid_16">
					<p>
						<label>Заголовок <small>не длиннее 255 символов</small></label>
						<input type="text" name="article[title]" value="" />
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Текст <small>полный текст статьи</small></label>
						<textarea class="editor" name="article[text]"></textarea>
					</p>
				</div>
                
				<div class="grid_16">
					<p>
						<label>Мета-заголовок <small>заголовок окна браузера не длиннее 255 символов</small></label>
						<input type="text" name="article[meta][title]" />
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Мета-описание <small></small></label>
						<textarea style="height: 100px; max-width: 935px;" name="article[meta][description]"></textarea>
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Мета-слова <small>ключевые слова для поисковых систем</small></label>
						<textarea style="height: 100px; max-width: 935px;" name="article[meta][keywords]"></textarea>
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Доп. файлы <small>выберите файлы, чтобы прикрепить их к товару</small></label>
						<input name="additFiles[]" type="file" multiple="multiple" value="Прикрепить" />
					</p>
				</div>
                            
				<div class="grid_16">
					<p class="submit">
						<input type="submit" name="reset" value="Назад">
						<input type="submit" value="Добавить" />
					</p>
				</div>
				
			</form>
			
		{/if}
	
	
	</div>

{include file="default/bottom.tpl"}

{include file="default/footer.tpl"}