{include file="default/header.tpl"}
<h1 id="head">Панель управления "{$strSiteName}"</h1>
		
	{include file="default/menu.tpl"}

        <div id="content" class="container_16 clearfix" style="padding-left: 40px;">

		<div class="row">
			<h2><a href="{$strPath}/categories">Категории</a> :: <a href="{$strPath}/{$strPageCode}/{$aCategory.id}">{$strBreadCrumbs}</a> :: {$strPageName}</h2>
			{if isset($aGoods)}
				<div class="utils">
					<a href="{$strPath}/{$strPageCode}/{$aCategory.id}/new" style="margin: 13px 20px 0 0;">Добавить товар</a>
				</div>
			{/if}
			
			{if $nAction==1}<p class="success">Ваши изменения сохранены</p>{/if}
			{if $nAction==2}<p class="error">При сохранении произошла ошибка</p>{/if}
		</div>

		{if isset($aGoods)}
			{* список товаров *}
			<form method="POST" action="">
				<div class="row">
					<p class="search-good-block">
						<label for="searcherGood">Модель <small>укажите модель для поиска товара</small></label>
						<input id="searcherGood" type="text" value="" cid="{$aCategory.id}" />
						<input id="submGoodSearch" type="submit" value="Искать" />
					</p>
				</div>
			</form>
                                                
                        <p>
                            <input type="button" id="save_ajax" value="Сохранить значения" />
                            <input type="hidden" name="cat_id" id="cat_id" value="{$aGoods[0]['cat_id']}" />
                        </p>
                        
			<div class="row">
				<table id="tbl_catalog">
					<thead>
						<tr>
							<th width="50%">Название</th>
							<th>Дата</th>
							<th>Публикация</th>							
							{*<th colspan="3" width="10%">Действия</th>*}
                                                        <th>Наличие</th>
                                                        <th>Цена</th>
                                                        <th>Приоритет</th>
                                                        <th>Действия</th>
                                                        <th>Действия</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th width="50%">Название</th>
							<th>Дата</th>
							<th>Публикация</th>							
							<th>Наличие</th>
                                                        <th>Цена</th>
                                                        <th>Приоритет</th>
                                                        <th>Действия</th>
                                                        <th>Действия</th>
						</tr>
					</tfoot>
					<tbody id="tabGoodLister">
					
			{foreach from=$aGoods item=item}
			
						<tr>
							<td><a href="{$strPath}/{$strPageCode}/{$aCategory.id}/{$item.id}">{$item.title} {$item.brand} {$item.model}</a></td>
							<td style="white-space: nowrap;">{$item.date|date_format:"%D %H:%M"}</td>
							<td style="white-space: nowrap;">
                                                            {*{if $item.public==1}опубликовано{else}не опубликовано{/if}*}
                                                            <select style="width: 70px; margin-left: 12px;" size="1" id="status_{$item.id}" name="status_{$item.id}">
                                                                <option value="1" {if $item.public==1} selected="selected" {/if}>
                                                                    Да
                                                                </option>
                                                                <option value="0" {if $item.public==0} selected="selected" {/if}>
                                                                    Нет
                                                                </option>
                                                            </select>
                                                        </td>
                                                        <td style="white-space: nowrap;">
                                                            {*{if $item.public==1}опубликовано{else}не опубликовано{/if}*}
                                                            <select style="width: 70px;" size="1" id="existence_{$item.id}" name="existence_{$item.id}">
                                                                <option value="1" {if $item.existence==1} selected="selected" {/if}>
                                                                    Да
                                                                </option>
                                                                <option value="0" {if $item.existence==0} selected="selected" {/if}>
                                                                    Нет
                                                                </option>
                                                            </select>
                                                        </td>
                                                        <td><input style="width: 70px;" type="text" id="price_{$item.id}" name="price_{$item.id}" value="{$item.price}"></td>
                                                        <td><input style="width: 40px; margin-left: 12px;" type="text" id="priority_{$item.id}" name="priority_{$item.id}" value="{$item.priority}"></td>
							<td><a href="{$strPath}/{$strPageCode}/{$aCategory.id}/{$item.id}" class="edit">Редактировать</a></td>
							<td><a href="{$strPath}/{$strPageCode}/{$aCategory.id}/{$item.id}/del" class="delete">Удалить</a></td>
						</tr>
				
			{/foreach}
			
					</tbody>
				</table>
			</div>
                        
                        <br>
                        <p>
                            <input type="button" id="save_ajax" value="Сохранить значения" />
                        </p>
			
		{elseif isset($aGood)}
			{* редактирование товара *}
                        {include file="popup/attendant_goods.tpl"}
			<input id="item_id" cover="{$aGood.cover}" type="hidden" item_type="goods" item_id="{$aGood.id}" />
			<input id="item_type" type="hidden" item_type="goods" />
			<form method="POST" action="" enctype="multipart/form-data">
			
				<div class="row">
					<p>
						<label>Публикация <small>неопубликованные товары не выводятся на сайте</small></label>
						<input style="width: 20px;" type="checkbox" name="good[public]" {if $aGood.public}checked{/if} />
					</p>
				</div>
				
				<div class="row">
					<p>
						<label>Наличие <small>имеется ли товар в наличии</small></label>
						<input style="width: 20px;" type="checkbox" name="good[existence]" {if $aGood.existence}checked="checked"{/if} />
					</p>
				</div>
				
				<div class="row">
					<p>
						<label>Приоритет <small>товары с большим приоритетом выводятся раньше</small></label>
						<input type="text" name="good[priority]" value="{$aGood.priority}" />
					</p>
				</div>
				
				<div class="row">
					<p>
						<label>Наименование <small>например: сплит-система, телевизор т.д., не длиннее 255 символов</small></label>
						<input type="text" name="good[title]" value="{$aGood.title}" />
					</p>
				</div>
				
				<div class="row">
					<p>
						<label>Модель <small>например: HR1870/00, не длиннее 255 символов</small></label>
						<input type="text" name="good[model]" value="{$aGood.model}" />
					</p>
				</div>
				
				<div class="row">
					<p>
						<label>Обложка <small>выбирается внизу из загруженных изображений</small></label>
						<div id="good_cover_box">
							{if $aGood.cover}								
								<img src="/public/img/upload/thumbnail/{$aGood.cover}">
							{else}							
								не выбрана
							{/if}
						</div>
					</p>
				</div>
				
				<div class="row">
					<p>
						<label>Сопутствующие товары</label>
						<a id="viewerAttGPopup" href="#">добавить</a>
						<table>
							<thead>
								<tr>
									<th>Наименование</th>
									<th>Действие</th>
								</tr>
							</thead>
							<tbody id="mainGTbody">
								{if !empty($aGood.attendant_goods)}
									{foreach $aGood.attendant_goods as $kattgood => $attgood}
										<tr>
											<td>{$attgood}</td>
											<td><a class="delete delAttendantGood" href="#" gid="{$kattgood}" gidup="{$aGood.id}">удалить</a></td>
										</tr>
									{/foreach}
								{/if}
							</tbody>
						</table>
					</p>
				</div>

                                <div class="row">   
					<p>
						<label>Текст <small>подробное описание</small></label>
						<textarea style="height: 300px;" class="editor" name="good[text]">{$aGood.text}</textarea>
					</p>
				</div>
				
				<div class="row">
					<p>
						<label>Мета-заголовок <small>заголовок окна браузера не длиннее 255 символов</small></label>
						<input type="text" name="good[meta][title]" value="{$aGood.meta.title|default:''}" />
					</p>
				</div>

				<div class="row">
					<p>
						<label>Мета-описание <small></small></label>
						<textarea style="height: 100px; max-width: 935px;" name="good[meta][description]">{$aGood.meta.description|default:''}</textarea>
					</p>
				</div>

				<div class="row">
					<p>
						<label>Мета-слова <small>ключевые слова для поисковых систем</small></label>
						<textarea style="height: 100px; max-width: 935px;" name="good[meta][keywords]">{$aGood.meta.keywords|default:''}</textarea>
					</p>
				</div>
				
				<div class="row">
					<p>
						<label>Акция <small>занчок (лейбл) акции, по которой идет этот товар</small></label>
						<select style="width: 300px;" name="good[action_label]">
							<option value="" {if empty($aGood.action_label)}selected="selected"{/if}>нет</option>
							<option value="gift" {if $aGood.action_label == 'gift'}selected="selected"{/if}>2 года обслуживания в подарок</option>
							<option value="freeinst" {if $aGood.action_label == 'freeinst'}selected="selected"{/if}>Бесплатный монтаж</option>
						</select>
					</p>
				</div>
				
				{foreach from=$aValues item=item}
					{if $item.type == 1 || $item.type == 7}
					
						<div class="row">
							<p>
								<label>{$item.name} <small>{if $item.filtered}Фильтруемое свойство{/if}</small></label>
								<input type="text" name="good[values][{$item.tpl_id}]" value="{$item.value}" />
							</p>
						</div>
						
					{elseif $item.type == 2}
					
						<div class="row">
							<p>
								<label>{$item.name} <small>{if $item.filtered}Фильтруемое свойство{/if}</small></label>
								<textarea type="text" name="good[values][{$item.tpl_id}]">{$item.value}</textarea>
							</p>
						</div>
						
					{elseif $item.type == 3}
					
						<div class="row">
							<p>
								<label>{$item.name} <small>{if $item.filtered}Фильтруемое свойство{/if}</small></label>
								<select style="width: 60px;" name="good[values][{$item.tpl_id}]">
									<option {if $item.value==1}selected{/if} value="1">Да</option>
									<option {if $item.value==0}selected{/if} value="0">Нет</option>
								</select>
							</p>
						</div>
						
					{elseif $item.type == 4 || $item.type == 6}
					
						<div class="row">
							<p>
								<label>{$item.name} <small>{if $item.filtered}Фильтруемое свойство{/if}</small></label>
                                                                <input style="width: 90px;" class="price_rub" type="text" name="good[values][{$item.tpl_id}]" value="{$item.value}" />&nbsp;&nbsp;<b>{$item.units}</b>
							</p>
						</div>
					{elseif $item.type == 9 || $item.type == 10}

						<div class="row">
								<p>
										<label>{$item.name} <small></small></label>
                                                                                <input style="width: 90px;" class="price_ue" type="text" name="good[values][{$item.tpl_id}]" value="{$item.value}" />{if !empty($item.units)}&nbsp;&nbsp;<b>{$item.units}</b>{/if}
								</p>
						</div>
						
					{elseif $item.type == 5}
					
						<div class="row">
							<p>
								<label>{$item.name} <small>{if $item.filtered}Фильтруемое свойство{/if}</small></label>
								<input class="phone" style="width: 130px;" type="text" name=good[values][{$item.tpl_id}]" value="{$item.value}" />
							</p>
						</div>
						
					{elseif $item.type == 8}
					
						<div class="row">
							<p>
								<label>{$item.name} <small>{if $item.filtered}Фильтруемое свойство{/if}</small></label>
								<select style="width: 300px;" name="good[values][{$item.tpl_id}]">
									{foreach from=$item.values item=item1}
										<option {if $item.value == $item1}selected{/if}>{$item1}</option>
									{/foreach}
								</select>
							</p>
						</div>
						
					{/if}	
				
				{/foreach}
                                
                                <div class="row">
                                    <p>
                                        <label>Доп. файлы <small>выберите файлы, чтобы прикрепить их к товару</small></label>
                                        <input name="additFiles[]" type="file" multiple="multiple" value="Прикрепить" />
                                    </p>
                                </div>
                                
                                {if $aGood.additFiles}
                                    <div class="row">
                                        <table>
                                            <thead>
                                                <tr>
                                                    <th>Файл</th>
                                                    <th>Название</th>
                                                    <th>Действие</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {foreach $aGood.additFiles as $file}
                                                    <tr>
                                                        <td>
                                                            <a href="/admin/goods/{$aGood.cat_id}/{$aGood.id}/{$file.filename}"><img width="32" height="32" style="padding-right: 5px" src="/public/img/icon_{$file.icon}.png">{$file.filename}</a>
                                                        </td>
                                                        <td>
                                                            <input style="width: 500px;" name="additFileName[{$file.id}][name]" type="text" value="{if !empty($file.name)}{$file.name}{/if}" />
                                                        </td>
                                                        <td>
                                                            <a class="delete delFile" href="#" fid="{$file.id}" gid="{$aGood.id}" fname="{$file.filename}">удалить</a>
                                                        </td>
                                                    </tr>
                                                {/foreach}
                                            </tbody>
                                        </table>
                                    </div>
                                {/if}

                                
                                
                                {if $aCategory.display_options == 1}
                                    <div class="row">
                                        <label style="margin-top: 10px;">Дополнииельные иконки</label>
                                        {foreach $aGood.icons as $icon}
                                            <p>
                                                <label style="width: auto;">
                                                    <img src="/public/img/funcicons/{$icon.path}" />
                                                    <input name="good[options_list][]" type="checkbox" value="{$icon.id}" {if $icon.checked == 'checked'}checked="checked"{/if} />
                                                    {$icon.title}
                                                </label>
                                            </p>
                                        {/foreach}
                                    </div>
                                {/if}
                                
				<div class="row">
					<p class="submit">
						<input type="submit" name="reset" value="Назад">
						<input type="submit" value="Сохранить" />
					</p>
				</div>
				
			</form>
			<div style="clear: both; width: 100%; height: 30px;"></div>
			<div class="row">
				<p>
					<label>Изображения <small>все изменения при манипуляциях с изображениями, <b>кроме выбора обложки</b>, происходят сразу, не нужно нажимать кнопку "сохранить"</small></label>
					{include file="upload_plugin/loader.tpl"}
				</p>
			</div>
			
			{include file="upload_plugin/include_js.tpl"}
			
		{else}
			{* добавление товара *}
			<form method="POST" action="" enctype="multipart/form-data">
			
				<div class="row">
					<p>
						<label>Публикация <small>неопубликованные товары не выводятся на сайте</small></label>
						<input style="width: 20px;" type="checkbox" name="good[public]" />
					</p>
				</div>
				
				<div class="row">
					<p>
						<label>Наличие <small>имеется ли товар в наличии</small></label>
						<input style="width: 20px;" type="checkbox" name="good[existence]" checked="checked" />
					</p>
				</div>
				
				<div class="row">
					<p>
						<label>Приоритет <small>товары с большим приоритетом выводятся раньше</small></label>
						<input type="text" name="good[priority]" />
					</p>
				</div>
			
				<div class="row">
					<p>
						<label>Наименование <small>например: сплит-система, телевизор т.д., не длиннее 255 символов</small></label>
						<input type="text" name="good[title]" value="" />
					</p>
				</div>
				
				<div class="row">
					<p>
						<label>Модель <small>например: HR1870/00, не длиннее 255 символов</small></label>
						<input type="text" name="good[model]" value="" />
					</p>
				</div>
				
				<div class="row">
					<p>
						<label>Текст <small>подробное описание рубрики</small></label>
						<textarea class="editor" name="good[text]"></textarea>
					</p>
				</div>
				
				<div class="row">
					<p>
						<label>Мета-заголовок <small>заголовок окна браузера не длиннее 255 символов</small></label>
						<input type="text" name="good[meta][title]" />
					</p>
				</div>

				<div class="row">
					<p>
						<label>Мета-описание <small></small></label>
						<textarea style="height: 100px; max-width: 935px;" name="good[meta][description]"></textarea>
					</p>
				</div>

				<div class="row">
					<p>
						<label>Мета-слова <small>ключевые слова для поисковых систем</small></label>
						<textarea style="height: 100px; max-width: 935px;" name="good[meta][keywords]"></textarea>
					</p>
				</div>
				
				<div class="row">
					<p>
						<label>Акция <small>занчок (лейбл) акции, по которой идет этот товар</small></label>
						<select style="width: 300px;" name="good[action_label]">
							<option value="">нет</option>
							<option value="gift">2 года обслуживания в подарок</option>
						</select>
					</p>
				</div>
				
				{foreach from=$aValues item=item}
				
					{if $item.type == 1 || $item.type == 7}
					
						<div class="row">
							<p>
								<label>{$item.name} <small></small></label>
								<input type="text" name="good[values][{$item.id}]" value="" />
							</p>
						</div>
						
					{elseif $item.type == 2}
					
						<div class="row">
							<p>
								<label>{$item.name} <small></small></label>
								<textarea type="text" name="good[values][{$item.id}]"></textarea>
							</p>
						</div>
						
					{elseif $item.type == 3}
					
						<div class="row">
							<p>
								<label>{$item.name} <small></small></label>
								<select style="width: 60px;" name="good[values][{$item.id}]">
									<option value="1">Да</option>
									<option value="0">Нет</option>
								</select>
							</p>
						</div>
						
					{elseif $item.type == 4 || $item.type == 6}
					
						<div class="row">
							<p>
								<label>{$item.name} <small></small></label>
								<input style="width: 90px;" type="text" name="good[values][{$item.id}]" value="" />&nbsp;&nbsp;<b>{$item.units}</b>
							</p>
						</div>
                                                        
                                        {elseif $item.type == 9 || $item.type == 10}
                                                
                                                <div class="row">
                                                        <p>
                                                            <label>{$item.name} <small></small></label>
                                                            <input style="width: 90px;" type="text" name="good[values][{$item.id}]" value="" />{if !empty($item.units)}&nbsp;&nbsp;<b>{$item.units}</b>{/if}
                                                        </p>
                                                </div>
						
					{elseif $item.type == 5}
					
						<div class="row">
							<p>
								<label>{$item.name} <small></small></label>
								<input class="phone" style="width: 130px;" type="text" name="good[values][{$item.id}]" value="" />
							</p>
						</div>
						
					{elseif $item.type == 8}
					
						<div class="row">
							<p>
								<label>{$item.name} <small></small></label>
								<select style="width: 300px;" name="good[values][{$item.id}]">
									{foreach from=$item.values item=item1}
										<option>{$item1}</option>
									{/foreach}
								</select>
							</p>
						</div>
						
					{/if}	
				
				{/foreach}
                                
                                <div class="row">
                                    <p>
                                        <label>Доп. файлы <small>выберите файлы, чтобы прикрепить их к товару</small></label>
                                        <input name="additFiles[]" type="file" multiple="multiple" value="Прикрепить" />
                                    </p>
                                </div>
                                
                                {if $aCategory.display_options == 1}
                                    <div class="row">
                                        <label style="margin-top: 10px;">Дополнииельные иконки</label>
                                        {foreach $aIcons as $icon}
                                            <p>
                                                <label style="width: auto;">
                                                    <img width="25" height="25" src="/public/img/funcicons/{$icon.path}" />
                                                    <input name="good[options_list][]" type="checkbox" value="{$icon.id}" />
                                                    {$icon.title}
                                                </label>
                                            </p>
                                        {/foreach}
                                    </div>
                                {/if}
                                
				<div class="row">
					<p class="submit">
						<input type="submit" name="reset" value="Назад">
						<input type="submit" value="Добавить" />
					</p>
				</div>
				
			</form>
			
		{/if}
	
	
	</div>

{include file="default/bottom.tpl"}

{include file="default/footer.tpl"}