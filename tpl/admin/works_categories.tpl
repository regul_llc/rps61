{include file="default/header.tpl"}

<h1 id="head">Панель управления "{$strSiteName}"</h1>
		
	{include file="default/menu.tpl"}

	<div id="content" class="container_16 clearfix">
	
		<div class="grid_16">
			<h2>{$strPageName}</h2>
			{if isset($aCategories)}
				<div class="utils">
					<a href="{$strPath}/{$strPageCode}/new" style="margin: 13px 20px 0 0;">Добавить рубрику</a>
				</div>
			{/if}
			
			{if $nAction==1}<p class="success">Ваши изменения сохранены</p>{/if}
			{if $nAction==2}<p class="error">При сохранении произошла ошибка</p>{/if}
		</div>
		
		{if isset($aCategories)}
		
			<div class="grid_16">
				<table>
					<thead>
						<tr>
							<th width="50%">Название</th>
							<th>Публикация</th>	
							<th colspan="3" width="10%">Действия</th>
						</tr>
					</thead>
					{*
					<tfoot>
						<tr>
							<td colspan="5" class="pagination">
								<span class="active curved">1</span><a href="#" class="curved">2</a><a href="#" class="curved">3</a><a href="#" class="curved">4</a> ... <a href="#" class="curved">10 million</a>
							</td>
						</tr>
					</tfoot>
					*}
					<tbody>
					
			{foreach from=$aCategories item=item}
			
						<tr>
							<td><a href="{$strPath}/works/{$item.id}">{$item.title}</a></td>
							<td style="white-space: nowrap;">{if $item.public==1}опубликовано{else}не опубликовано{/if}</td>
							<td><a href="{$strPath}/works/{$item.id}" class="edit">Работы</a></td>
							<td><a href="{$strPath}/{$strPageCode}/{$item.id}" class="edit">Редактировать</a></td>
							<td><a href="{$strPath}/{$strPageCode}/{$item.id}/del" class="delete">Удалить</a></td>
						</tr>
				
			{/foreach}
			
					</tbody>
				</table>
			</div>
			
		{elseif isset($aCategory)}
		
			<form method="POST" action="">
				<div class="grid_16">
					<p>
						<label>Публикация <small>неопубликованные рубрики не выводятся на сайте</small></label>
						<input style="width: 20px;" type="checkbox" name="category[public]" {if $aCategory.public}checked{/if} />
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Заголовок <small>не длиннее 255 символов</small></label>
						<input type="text" name="category[title]" value="{$aCategory.title}" />
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Текст <small>подробное описание рубрики</small></label>
						<textarea class="editor" name="category[text]">{$aCategory.text}</textarea>
					</p>
				</div>
			
				<div class="grid_16">
					<p class="submit">
						<input type="submit" name="reset" value="Назад">
						<input type="submit" value="Сохранить" />
					</p>
				</div>
				
			</form>
			
		{else}
		
			<form method="POST" action="">
			
				<div class="grid_16">
					<p>
						<label>Публикация <small>не опубликованные категории не выводятся на сайте</small></label>
						<input style="width: 20px;" type="checkbox" name="category[public]" />
					</p>
				</div>
			
				<div class="grid_16">
					<p>
						<label>Заголовок <small>не длиннее 255 символов</small></label>
						<input type="text" name="category[title]" value="" />
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Текст <small>подробное описание рубрики</small></label>
						<textarea class="editor" name="category[text]"></textarea>
					</p>
				</div>
				<div class="grid_16">
					<p class="submit">
						<input type="submit" name="reset" value="Назад">
						<input type="submit" value="Добавить" />
					</p>
				</div>
				
			</form>
			
		{/if}
	
	
	</div>


{include file="default/bottom.tpl"}

{include file="default/footer.tpl"}