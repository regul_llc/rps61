{include file="default/header.tpl"}

<h1 id="head">Панель управления "{$strSiteName}"</h1>
		
	{include file="default/menu.tpl"}

	<div id="content" class="container_16 clearfix">
	
		<div class="grid_16">
			<h2>{$strPageName}</h2>
			<div class="utils">
				<a href="{$strPath}/{$strPageCode}/new" style="margin: 13px 20px 0 0;">Добавить новость</a>
			</div>
			{if $nAction==1}<p class="success">Ваши изменения сохранены</p>{/if}
			{if $nAction==2}<p class="error">При сохранении произошла ошибка</p>{/if}
		</div>
		
		{if isset($aNews)}
		
			<div class="grid_16">
				<table>
					<thead>
						<tr>
							<th>Название</th>
							<th colspan="2" width="10%">Действия</th>
						</tr>
					</thead>
					{*
					<tfoot>
						<tr>
							<td colspan="5" class="pagination">
								<span class="active curved">1</span><a href="#" class="curved">2</a><a href="#" class="curved">3</a><a href="#" class="curved">4</a> ... <a href="#" class="curved">10 million</a>
							</td>
						</tr>
					</tfoot>
					*}
					<tbody>
					
			{foreach from=$aNews item=item}
			
						<tr>
							<td><a href="{$strPath}/{$strPageCode}/{$item.id}">{$item.title}</a></td>
							<td><a href="{$strPath}/{$strPageCode}/{$item.id}" class="edit">Редактировать</a></td>
							<td><a href="{$strPath}/{$strPageCode}/{$item.id}/del" class="delete">Удалить</a></td>
						</tr>
				
			{/foreach}
			
					</tbody>
				</table>
			</div>
			
		{elseif isset($aNew)}
		
			<form enctype="multipart/form-data" method="POST" action="">
				<div class="grid_16">
					<p>
						<label>Заголовок <small>не длиннее 255 символов</small></label>
						<input type="text" name="news[title]" value="{$aNew.title}" />
					</p>
				</div>

                                <div class="grid_16">
                                    <p>
                                        <label> Дата акции</label>
                                        <input type="text" id="datepick" name="news[date]" value="{$aNew.date|date_format:"%d-%m-%y"}" />
                                    </p>
                                </div>
				
				<div class="grid_16">
					<p>
						<label>Картинка <small></small></label>
						{if $aNew.image}
							<img src="/public/img/news/{$aNew.image}" style="margin: 10px;" /><br>
						{/if}
						<input type="file" name="image" />
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Анонс <small>краткое вступление</small></label>
						<textarea class="editor" name="news[description]">{$aNew.description}</textarea>
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Текст <small>полный текст новости</small></label>
						<textarea class="editor" name="news[text]">{$aNew.text}</textarea>
					</p>
				</div>
			
				<div class="grid_16">
					<p class="submit">
						<input type="submit" name="reset" value="Назад">
						<input type="submit" value="Сохранить" />
					</p>
				</div>
				
			</form>
			
		{else}
		
			<form enctype="multipart/form-data" method="POST" action="">
			
				<div class="grid_16">
					<p>
						<label>Заголовок <small>не длиннее 255 символов</small></label>
						<input type="text" name="news[title]" value="" />
					</p>
				</div>

                                <div class="grid_16">
                                    <p>
                                        <label> Дата акции</label>
                                        <input type="text" id="datepick" name="news[date]" value="" />
                                    </p>
                                </div>

				<div class="grid_16">
					<p>
						<label>Картинка <small></small></label>

						<input type="file" name="image" />
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Анонс <small>краткое вступление</small></label>
						<textarea class="editor" name="news[description]"></textarea>
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>Текст <small>полный текст новости</small></label>
						<textarea class="editor" name="news[text]"></textarea>
					</p>
				</div>
				<div class="grid_16">
					<p class="submit">
						<input type="submit" name="reset" value="Назад">
						<input type="submit" value="Добавить" />
					</p>
				</div>
				
			</form>
			
		{/if}
	
	
	</div>

{include file="default/bottom.tpl"}

{include file="default/footer.tpl"}