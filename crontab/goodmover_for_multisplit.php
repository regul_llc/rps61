<?php

/**
 * Переносим наружные и внутренние сплиты в новую подкатегорию сплитсистем "Мультисплит" (ID: 380)
 */

//увеличиваем время жизни скрипта
set_time_limit(84600);

//подключаем необходимые библиотеки и коннектимся к БД
define('DR', $_SERVER['DOCUMENT_ROOT'].'/');

include DR.'lib/registry.php';
include DR.'lib/helper.php';
include DR.'lib/db.php';

H::connectToDB();
H::incModels();

//создаем объкт БД
$db = Registry::get('db');

$strSql = "
	SELECT
		`id`
	FROM
		`goods`
	WHERE
		`cat_id` IN (351, 352) AND (`title` LIKE '%наружный блок%' OR `title` LIKE '%внутренний блок%' OR `title` LIKE '%Мультисплит-система%')
";

$aGIds = $db->_getAll($strSql);
if (empty($aGIds) || empty($aGIds[0]))
	exit('Товары для переноса не найдены');
$gIds = '';
$aGIdsSize = count($aGIds);
foreach ($aGIds as $key => $gid) {
	$gIds.= $gid['id'];
	if ($key < ($aGIdsSize - 1))
		$gIds.= ',';
}

$strSql = "
	SELECT
		*
	FROM
		`values`
	WHERE
		`good_id` IN (".$gIds.")
";
$aValues = $db->_getAll($strSql);

foreach ($aValues as $val) {
	switch ($val['value_tpl_id']) {
		case 29:
		case 36:
			$db->delete(goods::TABLE_VALUES, db::p('id = ?i', $val['id']));
			break;
		
		case 30:
		case 37:
			$db->update(goods::TABLE_VALUES, array('value_tpl_id' => 165), 'id = '.$val['id']);
			break;
		
		case 31:
		case 38:
			$db->update(goods::TABLE_VALUES, array('value_tpl_id' => 166), 'id = '.$val['id']);
			break;
		
		case 32:
		case 39:
			$db->update(goods::TABLE_VALUES, array('value_tpl_id' => 167), 'id = '.$val['id']);
			break;
		
		case 33:
		case 40:
			$db->update(goods::TABLE_VALUES, array('value_tpl_id' => 168), 'id = '.$val['id']);
			break;
		
		case 34:
		case 41:
			$db->update(goods::TABLE_VALUES, array('value_tpl_id' => 169), 'id = '.$val['id']);
			break;
		
		case 35:
		case 42:
			$db->update(goods::TABLE_VALUES, array('value_tpl_id' => 170), 'id = '.$val['id']);
			break;
		
		case 109:
		case 107:
			$db->update(goods::TABLE_VALUES, array('value_tpl_id' => 171), 'id = '.$val['id']);
			break;
		
		case 110:
		case 108:
			$db->update(goods::TABLE_VALUES, array('value_tpl_id' => 172), 'id = '.$val['id']);
			break;
	}
}

$db->update(goods::TABLE, array('cat_id' => 380), 'id IN ('.$gIds.')');

exit('The end... :-))');