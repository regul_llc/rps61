<?php

//увеличиваем время жизни скрипта
set_time_limit(86400);

//подключаем необходимые библиотеки и коннектимся к БД
define('DR', $_SERVER['DOCUMENT_ROOT'].'/');

include DR.'lib/registry.php';
include DR.'lib/helper.php';
include DR.'lib/db.php';

H::connectToDB();
H::incModels();

//создаем объкт БД
$db = Registry::get('db');

//конвертер рублевых цен в доллары и евро
function convertValutesRubl($aPrices) {
    $exrate = settings::getExchangeValutes();
    
    $aPrices[0] = ($aPrices[0] > 0)? $aPrices[0] : 0;
    if ($aPrices[1] <= 0 ) {
        $aPrices[1] = $aPrices[0] / $exrate[0];
        $aPrices[2] = $aPrices[0] / $exrate[1];
    }
    elseif ($aPrices[2] <= 0)
        $aPrices[2] = $aPrices[0] / $exrate[1];
    
    return $aPrices;
}

//установщик валют
function defineValutes($aValues, $nGood) {
    global $db;
    $aTypes = goods::getValueTypesForFilter($aValues);

    $priceForValutes = array();
    foreach ($aValues as $nValueTpl => $strValue) {
        switch ($aTypes[$nValueTpl]) {
            case VTYPE_PRICE:
                $strValue = floatval($strValue);
                $priceForValutes[0] = $strValue;
                break;
            case VTYPE_PRICE_USD:
                $strValue = floatval($strValue);
                $priceForValutes[1] = $strValue;
                break;
            case VTYPE_PRICE_EURO:
                $strValue = floatval($strValue);
                $priceForValutes[2] = $strValue;
        }
    }
    ksort($priceForValutes);
    $priceForValutes = convertValutesRubl($priceForValutes);

    foreach ($aValues as $nValueTpl => $strValue) {
        if ($aTypes[$nValueTpl] == VTYPE_PRICE)
            $strValue = $priceForValutes[0];
        elseif ($aTypes[$nValueTpl] == VTYPE_PRICE_USD)
            $strValue = $priceForValutes[1];
        elseif ($aTypes[$nValueTpl] == VTYPE_PRICE_EURO)
            $strValue = $priceForValutes[2];

        if ($aOldValue = goods::getValue($nValueTpl, $nGood)) {
            if (in_array($aTypes[$nValueTpl], array(VTYPE_NUMBER, VTYPE_PRICE))) $strValue = floatval($strValue);

            $db->update(goods::TABLE_VALUES, array(
                    'value' => $strValue
            ), db::p('id = ?i', $aOldValue['id']));
        } else {
            if (in_array($aTypes[$nValueTpl], array(VTYPE_NUMBER, VTYPE_PRICE))) $strValue = floatval($strValue);

            $db->add(goods::TABLE_VALUES, array(
                'value_tpl_id' => $nValueTpl,
                'good_id' => $nGood,
                'value' => $strValue
            ));
        }
    }
}


//выбираем товары с ценами в 3-х валютах
$strSql = 'SELECT DISTINCT g.`id`
    FROM `goods` AS g, `values_tpl` AS vt
    WHERE g.`cat_id` = vt.`cat_id` AND vt.`type` IN (9,10)';

$aGoods = $db->_getAll($strSql);

if (empty($aGoods) || !$aGoods)
    exit('Товары, содержащие 3 вида цены не найдены');

//меняем цены с учетом текущего курса валют
foreach ($aGoods as $good) {
    //получаем массив цен
    $aValues = goods::getValues($good['id'], TRUE, TRUE);
    //обновляeм цены
    defineValutes($aValues, $good['id']);
}

echo 'Конвертация валют из рублей завершена';

?>