<?php

//увеличиваем время жизни скрипта
set_time_limit(86400);

// change current dir
chdir(dirname(__FILE__));
//
//подключаем необходимые библиотеки и коннектимся к БД
define('DR', '../');

require_once DR.'lib/registry.php';
require_once DR.'lib/helper.php';
require_once DR.'lib/db.php';

H::connectToDB('production');
H::incModels();

//создаем объкт БД
$db = Registry::get('db');

//$aGoods = $db->getAll('goods', 'public = 1');
//выбераем все ID категорий, товары которых емеют три вида цен
/*$strSql = 'SELECT DISTINCT `cat_id` FROM `values_tpl` WHERE `type` = 9 OR `type` = 10';
$catIds = $db->_getAll($strSql);
if (empty($catIds) || !$catIds)
    exit('Нет категорий, товары которых имеют 3 вида цен');

foreach ($catIds as $kcid => $cid)
    $catIds[$kcid] = $cid['cat_id'];

$catIds = implode (', ', $catIds);*/

//выбираем товары с ценами в 3-х валютах
$strSql = 'SELECT DISTINCT g.`id`
    FROM `goods` AS g 
    WHERE g.`cat_id` = 351 AND g.public=1';

$aGoods = $db->_getAll($strSql);

print 'Найдено '.count($aGoods).' товаров.'.PHP_EOL;

if (empty($aGoods) || !$aGoods)
    exit('Товары, содержащие 3 вида цены не найдены');

//H::debug_info($aGoods);
//меняем цены с учетом текущего курса валют
foreach ($aGoods as $good) {
    //получаем массив цен
    $aValues = goods::getValues($good['id'], TRUE, TRUE);
	foreach ($aValues as $key=>$value){
		if (isset($value)) $aValues[$key] += $value*5/100;
	}
    //обновлякм цены
    goods::setValues($aValues, $good['id']);
    //выводим ID товара для наглядности
	$aValues = goods::getValues($good['id'], TRUE, TRUE);
    print "ID обновляемого товара: ".$good['id']."\n";
}

echo 'Конвертация завершена';