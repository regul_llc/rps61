<?php

//увеличиваем время жизни скрипта
set_time_limit(86400);

// change current dir
chdir(dirname(__FILE__));
//
//подключаем необходимые библиотеки и коннектимся к БД
define('DR', '../');

require_once DR.'lib/registry.php';
require_once DR.'lib/helper.php';
require_once DR.'lib/db.php';

H::connectToDB('production');
H::incModels();

//создаем объкт БД
$db = Registry::get('db');

//$aGoods = $db->getAll('goods', 'public = 1');
//выбераем все ID категорий, товары которых емеют три вида цен
/*$strSql = 'SELECT DISTINCT `cat_id` FROM `values_tpl` WHERE `type` = 9 OR `type` = 10';
$catIds = $db->_getAll($strSql);
if (empty($catIds) || !$catIds)
    exit('Нет категорий, товары которых имеют 3 вида цен');

foreach ($catIds as $kcid => $cid)
    $catIds[$kcid] = $cid['cat_id'];

$catIds = implode (', ', $catIds);*/
 
$strSql = "SELECT value FROM quick_storage WHERE paramname='prices_is_updating'";
$strParam = 'prices_is_updating';
$nUpdatingStatus = $db->getOneBy('quick_storage', 'value', array("paramname"=>'prices_is_updating'));
if ((int)$nUpdatingStatus === 0){
	$db->_executeSql(sprintf("REPLACE INTO quick_storage SET paramname='prices_is_updating', value=1, stamp=%d",time()));
}elseif ((int)$nUpdatingStatus === 1){
	exit('script is worked');
}else{
	exit('false start');
}

//выбираем товары с ценами в 3-х валютах
$strSql = 'SELECT DISTINCT g.`id`
    FROM `goods` AS g, `values_tpl` AS vt
    WHERE g.`cat_id` = vt.`cat_id` AND vt.`type` IN (9,10) AND g.public=1';

$aGoods = $db->_getAll($strSql);
$nGoodsCount = count($aGoods);
$db->_executeSql(sprintf("REPLACE INTO quick_storage SET paramname='PricesUpdatingStartCount', value=%d, stamp=%d", $nGoodsCount , time()));
$db->_executeSql(sprintf("REPLACE INTO quick_storage SET paramname='PricesForUpdatingCount', value=%d, stamp=%d", $nGoodsCount , time()));
$db->_executeSql(sprintf("REPLACE INTO quick_storage SET paramname='PricesUpdatingCount', value=%d, stamp=%d", 0 , time()));

if (empty($aGoods) || !$aGoods)
    exit('Товары, содержащие 3 вида цены не найдены');

$nUpdatedCount = 0;
//меняем цены с учетом текущего курса валют
foreach ($aGoods as $good) {
    //получаем массив цен
    $aValues = goods::getValues($good['id'], TRUE, TRUE);
    var_dump($aValues);
    //обновлякм цены
    goods::setValues($aValues, $good['id']);
	
	$nUpdatedCount++;
	$db->_executeSql(sprintf("UPDATE quick_storage SET paramname='PricesForUpdatingCount', value=%d, stamp=%d WHERE paramname='PricesForUpdatingCount'", $nGoodsCount-$nUpdatedCount , time()));
	$db->_executeSql(sprintf("UPDATE quick_storage SET paramname='PricesUpdatingCount', value=%d, stamp=%d WHERE paramname='PricesUpdatingCount'", $nUpdatedCount , time()));
    //выводим ID товара для наглядности
    echo "ID обновляемого товара: ".$good['id']."\n";
	usleep(200000);
}
$db->_executeSql(sprintf("UPDATE quick_storage SET paramname='prices_is_updating', value=2, stamp=%d WHERE paramname='prices_is_updating'",time()));
$db->_executeSql(sprintf("UPDATE options SET value=2 WHERE id=29"));
$db->_executeSql(sprintf("UPDATE options SET value=%d WHERE id=30",time()));
echo 'Конвертация завершена';