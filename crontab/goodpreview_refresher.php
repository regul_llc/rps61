<?php
//увеличиваем время жизни скрипта
set_time_limit(86400);

/**
 * Памятка того, как работает UploadHandler
 * $upload_handler = new UploadHandler($_POST);
 * - где $_POST, к примеру, равен:
 *  Array
 *   (
 *       [item_id] => 18
 *       [item_type] => goods
 *   );
 * 
 * Содержимое аргументов метода $upload_handler->create_scaled_image($file_name, $version, $options):
 * $file_name => Panasonic___CS_C_513c613cc2f2b_18.jpg (подтасовываем имя фотки из базы)
 * $version => main (подтасовываем ключ массива $aImageVersions)
 * $options => Array
 *       (
 *           [upload_dir] => Z:/home/rprofservice.ru/www/public/img/upload/main/
 *           [max_width] => 748
 *           [max_height] => 329
 *           [jpeg_quality] => 80
 *           [crop] => 1
 *       ) (подтасовываем $aImageVersions['list'] или $aImageVersions['medium'])
 */

chdir(dirname(__FILE__)); // change current dir

define('DR', '../');



require_once '../lib/registry.php';
require_once '../lib/helper.php';
require_once '../lib/db.php';

require_once '../public/upload-plugin/server/php/UploadHandler.php';



H::ConnectToDB('production');
H::incModels();

$aImageVersions = array(
    // превью в списке товаров
    'list' => array(
    	'upload_dir' => DR.'public/img/upload/list/',
        'max_width' => 216,
        'max_height' => 155,
        'jpeg_quality' => 80,
        'canvas' => array('r' => 255, 'g' => 255, 'b' => 255)
    ),
    
    // увеличенное изображение в галерее товара
    'medium' => array(
    	'upload_dir' => DR.'public/img/upload/medium/',
        'max_width' => 343,
        'max_height' => 259,
        'jpeg_quality' => 80,
        'canvas' => array('r' => 255, 'g' => 255, 'b' => 255)
        //'crop' => true
    )
);

final class viewRefresher extends UploadHandler {
    public function __construct($options) {
        parent::__construct($options, FALSE);
        $this->options['upload_dir'] = DR.'public/img/upload/';
    }
    
    protected function get_full_url() {
        //$https = !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off';
        //$remPath = '/public/upload-plugin/server/php';
        return DR.'public/upload-plugin/server/php';
            /*($https ? 'https://' : 'http://').
            (!empty($_SERVER['REMOTE_USER']) ? $_SERVER['REMOTE_USER'].'@' : '').
            (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : ($_SERVER['SERVER_NAME'].
            ($https && $_SERVER['SERVER_PORT'] === 443 ||
            $_SERVER['SERVER_PORT'] === 80 ? '' : ':'.$_SERVER['SERVER_PORT']))).$remPath;*/
    }
    
    public function refresh($file_name, $version, $options){
        //удаляем фотку, если она уже существует
        $picName = $this->options['upload_dir'].$version.'/'.$file_name;
        if (file_exists($picName))
            unlink($picName);
        clearstatcache();
        
        return $this->create_scaled_image($file_name, $version, $options);
    }
}

//вытаскиваем из БД все фотки товаров
$db = Registry::get('db');
$strSql = "SELECT `item_id`, `item_type`, `file_name` FROM `".images::TABLE."` WHERE `item_type` = 'goods'";
$aPhotos = @$db->_getAll($strSql);

if(empty($aPhotos))
    exit('Не найдено ни одного товара с фото');

//$aPhotos = array($aPhotos[50]);
$eGoodIds = array();
foreach ($aPhotos as $photo) {
    foreach ($aImageVersions as $kversion => $imgVersion) {
        $uploader = new viewRefresher(array(
            'item_id' => $photo['item_id'],
            'item_type' => $photo['item_type']
        ));
        
        if(!$uploader->refresh($photo['file_name'], $kversion, $imgVersion))
            array_push($eGoodIds, $photo['item_id']);
    }
}

$eGoodIds = array_unique($eGoodIds);

if (!empty($eGoodIds)) {
    echo "Товары со следующими ID не обновили свои превью:\n";
    foreach ($eGoodIds as $eId)
        echo $eId."\n";
}
else
    echo 'Превью успешно обновлены.';
