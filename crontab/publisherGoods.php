<?php
/**
 * Устанавливаем публикацию товаров по бренду Tosot и Airwel
 * Tosot - 16 шт
 * Midea - 17 шт
 * Toshiba - 26 шт

 */
//увеличиваем время жизни скрипта
set_time_limit(86400);

//подключаем необходимые библиотеки и коннектимся к БД
define('DR', dirname(__FILE__).'/../');

require_once DR.'lib/registry.php';
require_once DR.'lib/helper.php';
require_once DR.'lib/db.php';

H::connectToDB();
H::incModels();

//создаем объкт БД
$db = Registry::get('db');

//вытаскиваем все товары с брендами Tosot и Airwel (щас это бытовые кондеры)
$strSql = "SELECT g.`id` FROM `".goods::TABLE."` AS g, `".goods::TABLE_VALUES."` AS v
    WHERE g.`id` = v.`good_id` AND v.`value_tpl_id` = 30 AND v.`value` = 'Toshiba' AND g.`public` = 1";
$aIds = $db->_getAll($strSql);

$aEIds = array();
if (empty($aIds))
    exit('Товаров с такими брендами не найдено');

foreach ($aIds as $good) {
    echo '<p>Товар с ID: '.$good['id'].'</p>';
    
    $success = $db->update(goods::TABLE, array('public' => 0), db::p('id = ?i', $good['id']));
    if (!$success)
        array_push($aEIds, $good['id']);
}

if (!empty($aEIds)) {
    echo '<p><b>Товары со следующими ID не изменили свой статус публикации:</b></p>';
    foreach ($aEIds as $id)
        echo '<p>'.$id.'</p>';
}
else
    echo 'Смена статуса завершена.';