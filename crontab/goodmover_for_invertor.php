<?php

/**
 * Переносим инверторы в новую подкатегорию альтернативной энергетики "Инверторы" (ID: 380)
 */

//увеличиваем время жизни скрипта
set_time_limit(84600);

//подключаем необходимые библиотеки и коннектимся к БД
define('DR', $_SERVER['DOCUMENT_ROOT'].'/');

include DR.'lib/registry.php';
include DR.'lib/helper.php';
include DR.'lib/db.php';

H::connectToDB();
H::incModels();

//создаем объкт БД
$db = Registry::get('db');

$strSql = "
	SELECT
		`id`
	FROM
		`goods`
	WHERE
		`cat_id` = 366 AND `title` LIKE '%инвертор%'
";
$aGIds = $db->_getAll($strSql);
if (empty($aGIds) || empty($aGIds[0]))
	exit('Товары для переноса не найдены');
$gIds = '';
$aGIdsSize = count($aGIds);
foreach ($aGIds as $key => $gid) {
	$gIds.= $gid['id'];
	if ($key < ($aGIdsSize - 1))
		$gIds.= ',';
}

$strSql = "
	SELECT
		*
	FROM
		`values`
	WHERE
		`good_id` IN (".$gIds.")
";
$aValues = $db->_getAll($strSql);
foreach ($aValues as $val) {
	switch ($val['value_tpl_id']) {
		case 129:
			$db->update(goods::TABLE_VALUES, array('value_tpl_id' => 182), db::p('id = ?i', $val['id']));
			break;
		
		case 130:
			$db->update(goods::TABLE_VALUES, array('value_tpl_id' => 183), db::p('id = ?i', $val['id']));
			break;
		
		case 131:
			$db->update(goods::TABLE_VALUES, array('value_tpl_id' => 184), db::p('id = ?i', $val['id']));
			break;
		
		case 132:
			$db->update(goods::TABLE_VALUES, array('value_tpl_id' => 185), db::p('id = ?i', $val['id']));
			break;
		
		case 133:
			$db->update(goods::TABLE_VALUES, array('value_tpl_id' => 186), db::p('id = ?i', $val['id']));
			break;
		
		case 134:
			$db->update(goods::TABLE_VALUES, array('value_tpl_id' => 187), db::p('id = ?i', $val['id']));
			break;
		
		case 135:
			$db->update(goods::TABLE_VALUES, array('value_tpl_id' => 188), db::p('id = ?i', $val['id']));
			break;
		
		case 56:
			$db->update(goods::TABLE_VALUES, array('value_tpl_id' => 189), db::p('id = ?i', $val['id']));
			break;
		
		case 99:
			$db->update(goods::TABLE_VALUES, array('value_tpl_id' => 190), db::p('id = ?i', $val['id']));
			break;
		
		case 100:
			$db->update(goods::TABLE_VALUES, array('value_tpl_id' => 191), db::p('id = ?i', $val['id']));
			break;
	}
}

$db->update(goods::TABLE, array('cat_id' => 381), 'id IN ('.$gIds.')');

exit('The end... :-))');