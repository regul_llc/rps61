<?php

class Ajax_Controller extends Default_Controller 
{
	
	public function getPriceUpdStats() {
        header('Content-Type: application/json');
		$db = Registry::get('db');
		$aParams = array('prices_is_updating','PricesForUpdatingCount','PricesUpdatingCount','PricesUpdatingStartCount');
		$strSql = sprintf('SELECT * FROM quick_storage WHERE paramname IN ("%s")',  implode('","', $aParams));
        $aRows = $db->_getAll($strSql);
		$aResult = array();
		foreach ($aRows as $aVal){
			$aResult[$aVal['paramname']] = array('value'=>$aVal['value'], 'stamp'=>$aVal['stamp']);
		}
        $this->JSON['result'] = $aResult;
        $this->end();
    }

    public function getFields() {
        header('Content-Type: application/json');

        if (isset($_POST['cats']) && is_array($_POST['cats']) && count($_POST['cats'])>0) {

            $this->JSON['fields'] = seo_pages::getSameFields($_POST['cats']);

        }
        else $this->JSON['fields'] = array();

        $this->end();
    }

    public function getFilter() {
        header('Content-Type: application/json');

        $aNums = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З', 'И', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Э', 'Ю', 'Я');

        if (isset($_POST['name']) && ($strName = trim($_POST['name']))!='' && isset($_POST['count'])) {
            $nCount = intval($_POST['count']);

            $this->oSmarty->assign('aFilter', goods::getFilterByName($strName));
            $this->oSmarty->assign('strNum', $aNums[$nCount]);
            $this->JSON['filter'] = $this->oSmarty->fetch('admin/filter.tpl');
        }
        else $this->JSON['filter'] = '';

        $this->end();
    }
    
    public function setDataTabale() {
        
        //var_dump($_POST);
        $nCatId = intval($_POST['cat_id']);
        $aData = explode('&', $_POST['data']);
        
        $k = 0;
        foreach($aData as $name => $value){
            //print $value.' ; ';
            if ($k == 4) $k = 0;
            
            $k++;
            
            $aFileds = explode('_', $value);
            $aValues = explode('=', $aFileds[1]);
            $nItemId = $aValues[0];
            
            
                
            if ($k == 1)
            {
                $nStatus = $aValues[1];
                goods::setAjaxDataTable(array('public' => $nStatus), $nCatId, $nItemId);
            }
            if ($k == 2)
            {
                $nExistence = $aValues[1];
                goods::setAjaxDataTable(array('existence' => $nExistence), $nCatId, $nItemId);
            }
            if ($k == 3)
            {
                $nPrice = $aValues[1];
                $aValues = goods::getValues($nItemId, TRUE, TRUE);
                $aKeys = array_keys($aValues);
                $aValues[$aKeys[0]] = $nPrice;
                $aValues[$aKeys[1]] = 0;
                $aValues[$aKeys[2]] = 0;

                goods::setAjaxDataTablePrice($aValues, $nItemId);
                
            }
            if ($k == 4)
            {
                $nPriority = $aValues[1];
                goods::setAjaxDataTable(array('priority' => $nPriority), $nCatId, $nItemId);
            }
            
        }

    }

	public function setimgtitle() {	
		if (isset($_POST['item_id']) && isset($_POST['title'])) {
			images::setTitle($_POST['title'], $_POST['item_id']);
		}
	}
	
	public function setImgPriority() {
		if (isset($_POST['item_id']) && isset($_POST['priority'])) {
			images::setPriority($_POST['priority'], $_POST['item_id']);
		}
	}
	
	
	public function setcover() {	
		if (isset($_POST['item_id']) && isset($_POST['img_id']) && isset($_POST['item_type'])) {
			switch ($_POST['item_type']) {
				case 'goods': goods::setCover($_POST['item_id'], $_POST['img_id']); break;
				case 'works': works::setCover($_POST['item_id'], $_POST['img_id']); break;
			}
		}
	}
        
	public function addfeedback() {
		header('Content-Type: application/json');

		if (isset($_POST['name']) && isset($_POST['phone']) && isset($_POST['text'])) {
			$this->JSON['success'] = feedback::set($_POST);
			$this->end();
		}
	}
        
	public function deleteGoodsFile() {
		header('Content-Type: application/json');

		if (isset($_POST['fid']) && isset($_POST['gid']) && isset($_POST['fname']))
			$this->JSON['success'] = goods::deleteAdditFile($_POST['fid'], $_POST['fname'], $_POST['gid']);
		else
			$this->JSON['success'] = FALSE;
		$this->end();
	}
        
	public function deleteFile() {
		header('Content-Type: application/json');

		if (isset($_POST['fid']) && isset($_POST['itid']) && isset($_POST['fname']) && isset($_POST['ittype']))
			$this->JSON['success'] = files::delete($_POST['fid'], $_POST['fname'], $_POST['itid'], $_POST['ittype']);
		else
			$this->JSON['success'] = FALSE;
		$this->end();
	}
        
	public function searchGoods() {
		header('Content-Type: application/json');

		if (!isset($_POST['catId'])) {
			$this->JSON['success'] = FALSE;
			$this->end();
		}

		$this->JSON['goods'] = goods::_getForSearch($_POST['catId'], 'model', $_POST['fieldval']);
		$this->JSON['success'] = (empty($this->JSON['goods']))? FALSE : TRUE;   //H::debug_info($this->JSON['goods']);
		$this->end();
	}
	
	public function addrequest() {	
		if (isset($_POST['name']) && isset($_POST['phone'])) {
			requests::set($_POST);
			$aUser = user::getCurrentUser();
			
			$strMailTextTpl = "
				Имя: <b>{name}</b><br>
				Телефон: <b>{phone}</b><br>
				Адрес: <b>{address}</b><br>
				Дополнительная информация: <b>{text}</b><br>
			";
			
			requests::send_mail(options::get(2), $_POST['name'], "Новая заявка", H::tpl($strMailTextTpl, $_POST));
		} else $this->e();
	}
	
        //поиск товара по названию для последующей его привязки к сопутствующим
        public function searchAttendantGoods() {
            header('Content-Type: application/json');
            
            if (!isset($_POST['title'])) {
                $this->JSON['success'] = FALSE;
                $this->end();
            }
            
            $this->JSON['goods'] = goods::_getForAttendant($_POST['title']);
            $this->JSON['success'] = (empty($this->JSON['goods']))? FALSE : TRUE;
            $this->end();
        }
        
        
        function deleteAttGood() {
            header('Content-Type: application/json');
            
            if (isset($_POST['gid']) && intval($_POST['gid']) > 0 && isset($_POST['gidup']) && intval($_POST['gidup']) > 0)
                $this->JSON['success'] = goods::deleteAttGood($_POST['gid'], $_POST['gidup']);
            else
                $this->JSON['success'] = FALSE;
            $this->end();
        }
	
	public function add_to_cart() {
		header('Content-Type: application/json');
		
		if (isset($_POST['good_id']) && ($nGood = $_POST['good_id']) && isset($_POST['amount']) && ($nAmount = $_POST['amount'])) {
			$cart = new cart();
			$cart->add($nGood, $nAmount);

			$aCartInfo = $cart->getInfo();
			$this->JSON['cart_info_amount'] = $aCartInfo['amount'];
			$this->JSON['cart_info_summ'] = number_format(round($aCartInfo['summ']), 0, ' ', ' ');
			$this->end();
		}
	}
	
	
	/** проверка капчи **/
	public function checkcaptcha() {	
		session_start();
		$this->JSON['result'] = (isset($_POST['captcha']) && $_POST['captcha']==$_SESSION["captcha"]) ? 1 : 0;
		$this->end();
	}
	
	
	
	/****** Служебные методы  *******/
	
	protected $oSmarty = null;
	
	/** Массив возвращаемых данных в формате Json */
	protected $JSON = array();
	
	public function __call($strMethod, $mParams) {		
		if (!method_exists($this, $strMethod)) {
			$this->e('Экшн "'.$strMethod.'" не найден!');		
		} else {
			call_user_func_array(array(&$this, $strMethod));	
		}
	}
	
	
	public function __construct() {
		$this->oSmarty = new Smarty;
		$this->oSmarty->template_dir = DR.'/tpl/';
		$this->oSmarty->compile_dir = DR.'/tpl/smarty_sys/ctpl/';
		$this->oSmarty->cache_dir = DR.'/tpl/smarty_sys/cache/';
	}
	
	
	public function __destruct() {
//		$this->end();
	}

	
	/** Возвращет ошибку с текстом
	 *
	 * @param str $strMessage - Текст сообщения */
	protected function e($strMessage) {
		echo json_encode(array(
			'message' => $strMessage,
			'error' => true
		));
		die;
	}
	
	
	public function end() {
		$this->JSON['error'] = false;
		
		echo json_encode($this->JSON);
		die;
	}
}
