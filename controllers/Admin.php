<?php

class Admin_controller extends Default_Controller
{
	protected $oSmarty = null;
	
	/** добавляет новые копии загруженных картинок
	 * @todo Сделать универсальный, чтобы проверял из настроек загрузчика чего не хватает и ресайзил
	 * @return unknown
	 */
	public function resize() {
		return false;
		$strPath = DR.'/public/img/upload/';
		include_once DR.'lib/wideimage/WideImage.php';
		$aFiles = scandir($strPath);
		
		foreach ($aFiles as $strFile) {
			if (is_file($strPath.$strFile)) {
				echo $strFile.'<br>';
				$image = WideImage::loadFromFile($strPath.$strFile);
				$image->resize(1200, 1200)->saveToFile($strPath.'big/'.$strFile);
			}
		}
	}
	
	public function index() {
		$nAction = 0;

		if (isset($_POST['options'])) {
			$nAction = (options::set($_POST['options'])) ? 1 : 2;
		}

		$this->oSmarty->assign('nAction', $nAction);
		$this->oSmarty->assign('strPageName', 'Основные параметры');
		$this->oSmarty->assign('aOptions', options::getAll());
		$this->oSmarty->assign('strImageType', 'main');
		$this->oSmarty->display('index.tpl');
	}
	
	public function others() {
		$nAction = 0;

		if (isset($_POST['options'])) {
			$nAction = (options::set($_POST['options'])) ? 1 : 2;
		}

		$this->oSmarty->assign('nAction', $nAction);
		$this->oSmarty->assign('strPageName', 'Остальные настройки');
		$this->oSmarty->assign('aOptions', options::getAll('pages'));
		$this->oSmarty->display('index.tpl');
	}
        
        /* Гарантии */
        /*public function guarantee() {
            $nAction = 0;
            
            if (isset($_POST['options']))
                $nAction = (options::set($_POST['options']))? 1 : 2;
            
            $this->oSmarty->assign(array(
                'nAction' => $nAction,
                'strPageName' => 'Страница "Гарантии"',
                'pageText' => options::get(18)
            ));
            
            $this->oSmarty->display('guarantee.tpl');
        }*/
		
		
        /* Вакансии */
		public function vacancy() {
			$nAction = 0;
			
			if(isset($_POST['options']))
				$nAction = (options::set($_POST['options']))? 1 : 2;
			
			$this->oSmarty->assign(array(
				'nAction' => $nAction,
				'strPageName' => 'Страница "Вакансии"',
				'pageText' => options::get(19)
			));
			
			$this->oSmarty->display('vacancy.tpl');
		}
		
        
        /* Настройки */
        public function settings() {
            $nAction = 0;
            $db = Registry::get('db');
            if (isset($_POST['setValutes'])){
				$nAction = (settings::setValutes($_POST['valutes']))? 1 : 2;
				$aOptions[28] = (!empty($_POST['options']['updateByCB'])) ? 1 : 0;	
				options::set($aOptions);
			}
			
			if (isset($_POST['recalcValPrices'])){
				$aOptions[29] = 1;
				options::set($aOptions);
				$strSql = sprintf("REPLACE INTO quick_storage SET paramname='prices_is_updating', value=0, stamp=%d",time());
				$db->_executeSql($strSql);
				exec('php '.dirname(__FILE__).'/../crontab/price_refresher.php > /dev/null &');
				//$this->redirect('settings');
			}
			
			//var_dump(options::getAllOptions('valutes'));
			$this->oSmarty->assign(array(
                'nAction' => $nAction,
                'strPageName' => 'Настройки',
                'aValutes' => settings::getAllValutes(),
				'aOptions' => options::getAllOptions('valutes')
            ));
            $this->oSmarty->display('settings.tpl');
            
            /*if (isset($_POST['options'])){
                if (!preg_match('/^[\d]+((\.|,)[\d]+)?$/', $_POST['options'][18]) || !preg_match('/^[\d]+((\.|,)[\d]+)?$/', $_POST['options'][19]))
                    $this->oSmarty->assign('errCurrency', 'Курсы валют должны быть заполнены числом');
                else {
                    $_POST['options'][18] = str_replace(',', '.', $_POST['options'][18]);
                    $_POST['options'][19] = str_replace(',', '.', $_POST['options'][19]);
                    
                    $nAction = (options::set($_POST['options']))? 1 : 2;
                }
            }
            
            $this->oSmarty->assign(array(
                'nAction' => $nAction,
                'strPageName' => 'Настройки',
                'aOptions' => options::getAll('excurrency')
            ));
            $this->oSmarty->display('index.tpl');*/
        }

	
	public function articles() {
            $nAction = 0;
            $aAddress = Registry::get('address');

            if (isset($_POST['reset'])) {
                    $this->redirect($this->strMethodName);
            }

            // Страница редактировани
            if (isset($aAddress[3]) && ($nId = intval($aAddress[3]))) {
                if (isset($aAddress[4]) && $aAddress[4]=='del') {
                    articles::del($nId);
                    $this->redirect($this->strMethodName);
                }
                elseif (isset($aAddress[4]) && preg_match('/\.(xls|xlsx|csv|pdf|txt|doc|docx)$/i', $aAddress[4])) {
                    $redirectPath = '/admin/articles/'.$nId;
                    H::downloadFile($nId.'/'.$aAddress[4], $redirectPath, 'article');
                }
                $strPageName = 'Редактирование статьи';

                // Сохранение
                if (isset($_POST['additFileName']))
                    files::set($_POST['additFileName']);
                
                if (isset($_POST['article'])) {
                    $nAction = (articles::set($_POST['article'], $nId)) ? 1 : 2;
                }

                $this->oSmarty->assign('aArticle', articles::get($nId));

            // Страница добавления
            } elseif (isset($aAddress[3]) && $aAddress[3]=='new') {
                $strPageName = 'Новая статья';

                if (isset($_POST['article'])) {
                    $nId = articles::set($_POST['article']);
                    $this->redirect(sprintf('%s/%d', $this->strMethodName, $nId));
                }

            // Список
            } else {
                $strPageName = 'Статьи';
                $this->oSmarty->assign('aArticles', articles::getAll());
            }

            $this->oSmarty->assign('nAction', $nAction);
            $this->oSmarty->assign('strPageName', $strPageName);
            $this->oSmarty->display('articles.tpl');
	}

	
	public function news() {
		$nAction = 0;
		$aAddress = Registry::get('address');

		if (isset($_POST['reset'])) {
			$this->redirect($this->strMethodName);
		}

		// Страница редактировани
		if (isset($aAddress[3]) && ($nId = intval($aAddress[3]))) {
			if (isset($aAddress[4]) && $aAddress[4]=='del') {
				news::del($nId);
				$this->redirect($this->strMethodName);
			}
			$strPageName = 'Редактирование новости';

			// Сохранение
			if (isset($_POST['news'])) {
				$nAction = (news::set($_POST['news'], $nId)) ? 1 : 2;
			}

			$this->oSmarty->assign('aNew', news::get($nId));
			
		// Страница добавления
		} elseif (isset($aAddress[3]) && $aAddress[3]=='new') {
			$strPageName = 'Добавление новости';
			
			if (isset($_POST['news'])) {
				$nId = news::set($_POST['news']);
				$this->redirect(sprintf('%s/%d', $this->strMethodName, $nId));
			}
			
		// Список
		} else {
			$strPageName = 'Новость';
			$this->oSmarty->assign('aNews', news::getAll());
		}

		$this->oSmarty->assign('nAction', $nAction);
		$this->oSmarty->assign('strPageName', $strPageName);
		$this->oSmarty->display('news.tpl');
	}
	
	
	public function actions() {
		$nAction = 0;
		$aAddress = Registry::get('address');
		
		if (isset($_POST['reset'])) {
			$this->redirect($this->strMethodName);
		}
		
		// Страница редактирования
		if (isset($aAddress[3]) && ($nId = intval($aAddress[3]))) {
			if (isset($aAddress[4]) && $aAddress[4]=='del') {
				actions::del($nId);
				$this->redirect($this->strMethodName);
			}
			$strPageName = 'Редактирование акции';
			
			// Сохранение
			if (isset($_POST['action'])) {
				$nAction = (actions::set($_POST['action'], $nId)) ? 1 : 2;
			}
			$action = actions::get($nId);
        //                $action['date'] = date("d-m-Y", $action['date']);
                        
			$this->oSmarty->assign('aAction', $action);
		}
		// Страница добавления
		elseif (isset($aAddress[3]) && $aAddress[3] == 'new') {
			$strPageName = 'Добавление акции';
			
			if (isset($_POST['action'])) {
				$nId = actions::set($_POST['action']);
				$this->redirect(sprintf('%s/%d', $this->strMethodName, $nId));
			}
		}
		// Список
		else {
			if (isset($_POST['options'])) {
				$nAction = (options::set($_POST['options'])) ? 1 : 2;
			}
			
			$strPageName = 'Акции';
			$this->oSmarty->assign('aActions', actions::getAll());
			$this->oSmarty->assign('metaOpts', options::getAll('actions'));
		}
		
		$this->oSmarty->assign('nAction', $nAction);
		$this->oSmarty->assign('strPageName', $strPageName);
		$this->oSmarty->display('actions.tpl');
	}

	
	public function banners() {
		$nAction = 0;
		$aAddress = Registry::get('address');

		if (isset($_POST['reset'])) {
			$this->redirect($this->strMethodName);
		}

		// Страница редактировани
		if (isset($aAddress[3]) && ($nId = intval($aAddress[3]))) {
			if (isset($aAddress[4]) && $aAddress[4]=='del') {
				banners::del($nId);
				$this->redirect($this->strMethodName);
			}
			$strPageName = 'Редактирование';

			// Сохранение
			if (isset($_POST['banners'])) {
				$nAction = (banners::set($_POST['banners'], $nId)) ? 1 : 2;
			}

			$this->oSmarty->assign('aBanner', banners::get($nId));
			
		// Страница добавления
		} elseif (isset($aAddress[3]) && $aAddress[3]=='new') {
			$strPageName = 'Добавление';
			
			if (isset($_POST['banners'])) {
				$nId = banners::set($_POST['banners']);
				$this->redirect(sprintf('%s/%d', $this->strMethodName, $nId));
			}
			
		// Список
		} else {
			$strPageName = 'Баннеры';
			$this->oSmarty->assign('aBanners', banners::getAll());
		}

		$this->oSmarty->assign('nAction', $nAction);
		$this->oSmarty->assign('strPageName', $strPageName);
		$this->oSmarty->display('banners.tpl');
	}
	
	
	public function orders() {
		$nAction = 0;
		$aAddress = Registry::get('address');
		$cart = new cart();
		if (isset($_POST['reset'])) {
			$this->redirect($this->strMethodName);
		}
		
		if (isset($aAddress[3]) && ($nId = intval($aAddress[3]))) {
			$strPageName = 'Заказ №'.$nId;
			
			// Удаление заказа
			if (isset($aAddress[4]) && $aAddress[4]=='del') {
				$cart->deleteOrder($nId);
				$this->redirect($this->strMethodName);
			}
			
			// Сохранение
			if (isset($_POST['order'])) {
				$nAction = ($cart->setOrder($_POST['order'], $nId)) ? 1 : 2;
			}

			$this->oSmarty->assign('aOrder', $cart->getOrder($nId));
		} else {
			$this->oSmarty->assign('aOrders', $cart->getOrders());
			$strPageName = 'Заказы';
		}
		
		$this->oSmarty->assign('nAction', $nAction);
		$this->oSmarty->assign('strPageName', $strPageName);
		$this->oSmarty->display('orders.tpl');
	}
	
	
	public function delvaluetpl() {
		$aAddress = Registry::get('address');
		if (isset($aAddress[3]) && ($nId = intval($aAddress[3]))) {
			$aValueTpl = goods::getValueTpl($nId);
			goods::delValueTpl($nId);
			
			$this->redirect('values_tpl/'.$aValueTpl['cat_id']);
		}
	}
	
	
	public function values_tpl() {
		$aAddress = Registry::get('address');
		
		if (isset($aAddress[3]) && ($nId = intval($aAddress[3]))) {			
			if (isset($_POST['reset'])) {
				$this->redirect('categories');
			}
			
			$aCat = goods::getCat($nId);
			
			$strPageName = "Редактирование свойств товаров";
			$nAction = 0;

			if (isset($_POST['newvaluestpl'])) {
				goods::addValueTpl($_POST['newvaluestpl'], $nId);
				$nAction = 3;
			}
			
			if (isset($_POST['valuestpl'])) {
				goods::setValuesTpl($_POST['valuestpl'], $nId);
				$nAction = 1;
			}

			$this->oSmarty->assign('aCategory', $aCat);
			$this->oSmarty->assign('aValueTypes', goods::$field_types);
			$this->oSmarty->assign('strBreadCrumbs', goods::getBreadCrumbs($aCat['id']));
			$this->oSmarty->assign('nAction', $nAction);
			$this->oSmarty->assign('strPageName', $strPageName);
			$this->oSmarty->assign('aValuesTpl', goods::getValuesTplSimple($nId));
			$this->oSmarty->display('values_tpl.tpl');
		}
		else $this->error404();
	}

        
        public function feedback() {
            $aAddress = Registry::get('address');
            
            if (isset($_POST['reset']))
                $this->redirect($this->strMethodName);
            
            //детальный просмотр конкрентого сообщения
            if (isset($aAddress[3]) && ($nId = intval($aAddress[3]))) {
                //удаление сообщения
                if (isset($aAddress[4]) && $aAddress[4] == 'del') {
                    feedback::del($nId);
                    $this->redirect($this->strMethodName);
                }
                $strPageName = 'Просмотр сообщения';
                $this->oSmarty->assign('aFeedback', feedback::get($nId));
            }
            //список сообщений
            else {
                $strPageName = 'Обратная связь';
                $this->oSmarty->assign('aFeedbacks', feedback::_getAll());
            }
            
            $this->oSmarty->assign('strPageName', $strPageName);
            $this->oSmarty->display('feedback.tpl');
        }
        
	
	public function reviews() {
		$nAction = 0;
		$aAddress = Registry::get('address');

		if (isset($_POST['reset'])) {
			$this->redirect($this->strMethodName);
		}

		// Страница редактировани
		if (isset($aAddress[3]) && ($nId = intval($aAddress[3]))) {
			if (isset($aAddress[4]) && $aAddress[4]=='del') {
				reviews::del($nId);
				$this->redirect($this->strMethodName);
			}
			$strPageName = 'Модерация сообщения';

			// Сохранение
			if (isset($_POST['reviews'])) {
				$nAction = (reviews::set($_POST['reviews'], $nId)) ? 1 : 2;
			}

			$this->oSmarty->assign('aReview', reviews::get($nId));
			
		// Страница добавления
		} elseif (isset($aAddress[3]) && $aAddress[3]=='new') {
			$strPageName = 'Новое сообщение';
			
			if (isset($_POST['reviews'])) {
				$nId = reviews::set($_POST['reviews']);
				$this->redirect(sprintf('%s/%d', $this->strMethodName, $nId));
			}
			
		// Список
		} else {
			$strPageName = 'Отзывы';
			$this->oSmarty->assign('aReviews', reviews::getAll());
		}

		$this->oSmarty->assign('nAction', $nAction);
		$this->oSmarty->assign('strPageName', $strPageName);
		$this->oSmarty->display('reviews.tpl');
	}

	
	public function requests() {
		$nAction = 0;
		$aAddress = Registry::get('address');

		if (isset($_POST['reset'])) {
			$this->redirect($this->strMethodName);
		}

		// Страница редактировани
		if (isset($aAddress[3]) && ($nId = intval($aAddress[3]))) {
			if (isset($aAddress[4]) && $aAddress[4]=='del') {
				requests::del($nId);
				$this->redirect($this->strMethodName);
			}
			$strPageName = 'Модерация сообщения';

			// Сохранение
			if (isset($_POST['requests'])) {
				$nAction = (requests::set($_POST['requests'], $nId)) ? 1 : 2;
			}

			$this->oSmarty->assign('aRequest', requests::get($nId));
			
		// Страница добавления
		} elseif (isset($aAddress[3]) && $aAddress[3]=='new') {
			$strPageName = 'Новое сообщение';
			
			if (isset($_POST['requests'])) {
				$nId = requests::set($_POST['requests']);
				$this->redirect(sprintf('%s/%d', $this->strMethodName, $nId));
			}
			
		// Список
		} else {
			$strPageName = 'Заявки';
			$this->oSmarty->assign('aRequests', requests::getAll());
		}

		$this->oSmarty->assign('nAction', $nAction);
		$this->oSmarty->assign('strPageName', $strPageName);
		$this->oSmarty->display('requests.tpl');
	}
	
	public function goods() {
		$nAction = 0;
		$aAddress = Registry::get('address');

		if (isset($aAddress[3]) && ($nCatId = intval($aAddress[3]))) {
			if (isset($_POST['reset'])) {
				$this->redirect(sprintf('%s/%d', $this->strMethodName, $nCatId));
			}

			// Страница редактировани 
			if (isset($aAddress[4]) && ($nId = intval($aAddress[4]))) {
				
				if (isset($aAddress[5]) && $aAddress[5]=='del') {
					goods::del($nId);
					$this->redirect('goods/'.$nCatId);
				}
				elseif (isset($aAddress[5]) && preg_match('/\.(xls|xlsx|csv|pdf|txt|doc|docx)$/i', $aAddress[5])) {
					$redirectPath = '/admin/goods/'.$nCatId.'/'.$nId;
					H::downloadFile($nId.'/'.$aAddress[5], $redirectPath);
					exit;
				}
				
				$strPageName = 'Редактирование товара';
	
				// Сохранение
				if (isset($_POST['additFileName']))
					goods::setAdditFiles($_POST['additFileName']);
                                
                                if(isset($_POST['attGood']))
					goods::addAttGood($_POST['attGood'], $nId);
                                
				if (isset($_POST['good'])) {
					if (isset($_POST['good']['values'])) {
						goods::setValues($_POST['good']['values'], $nId);
					}
					
					$nAction = (goods::set($_POST['good'], $nCatId, $nId)) ? 1 : 2;
//                                        print '<pre>';
//                                        var_dump($_POST['good']);
//                                        print '</pre>';
				}

				$this->oSmarty->assign('aGood', goods::get($nId, TRUE));
				$this->oSmarty->assign('aValues', goods::getValues($nId, TRUE, FALSE, TRUE));
                               
				
			// Страница добавления 
			} elseif (isset($aAddress[4]) && $aAddress[4]=='new') {
				$strPageName = 'Новый товар';
				
				if (isset($_POST['good'])) {
					$nId = goods::set($_POST['good'], $nCatId);
					
					if (isset($_POST['good']['values'])) {
						goods::setValues($_POST['good']['values'], $nId);
					}
					
					$this->redirect(sprintf('%s/%d/%d', $this->strMethodName, $nCatId, $nId));
				}

				$this->oSmarty->assign('aValues', goods::getValuesTpl($nCatId));
                                $this->oSmarty->assign('aIcons', goods::getIcons());
				
			// Список 
			} else {
				$strPageName = 'Товары';
				$this->oSmarty->assign('aGoods', goods::_getAll($nCatId));
			}
		} 
		else $this->redirect('categories');

		$this->oSmarty->assign('aCategory', goods::getCat($nCatId));
		$this->oSmarty->assign('strBreadCrumbs', goods::getBreadCrumbs($nCatId));
		$this->oSmarty->assign('nAction', $nAction);
		$this->oSmarty->assign('strPageName', $strPageName);
		$this->oSmarty->display('goods.tpl');
	}
	
	public function categories() {
		$nAction = 0;
		$aAddress = Registry::get('address');

		if (isset($_POST['reset'])) {
			$this->redirect($this->strMethodName);
		}

		// Страница редактировани категории
		if (isset($aAddress[3]) && ($nId = intval($aAddress[3]))) {
			if (isset($aAddress[4]) && $aAddress[4]=='del') {
				goods::delCat($nId);
				$this->redirect($this->strMethodName);
			}
            // обнуление наличии всех товаров категории
			if (isset($aAddress[4]) && $aAddress[4]=='empty') {
				goods::setExistenceForCat($nId, 0);
				$this->redirect($this->strMethodName);
			}
            // устанавливаем наличие всех товаров категории
			if (isset($aAddress[4]) && $aAddress[4]=='fill') {
				goods::setExistenceForCat($nId, 1);
				$this->redirect($this->strMethodName);
			}
			$strPageName = 'Редактирование категории';

			// Сохранение категории
			if (isset($_POST['category'])) {
				$nAction = (goods::setCat($_POST['category'], $nId)) ? 1 : 2;
			}

			$this->oSmarty->assign('aCategory', goods::getCat($nId));
			
		// Страница добавления категории
		} elseif (isset($aAddress[3]) && $aAddress[3]=='new') {
			$strPageName = 'Новая категория';
			
			if (isset($_POST['category'])) {
				if (isset($aAddress[4]) && ($nParentId = intval($aAddress[4]))) {
					$_POST['category']['parent_id'] = $nParentId;
				}
				$nId = goods::setCat($_POST['category']);
				$this->redirect(sprintf('%s/%d', $this->strMethodName, $nId));
			}
			
		// Список категорий
		} else {
			$strPageName = 'Категории товаров';
			$this->oSmarty->assign('aCategories', goods::getAllCats());
		}

		$this->oSmarty->assign('nAction', $nAction);
		$this->oSmarty->assign('strPageName', $strPageName);
		$this->oSmarty->display('goods_categories.tpl');
	}
	
	
	public function works() {
		$nAction = 0;
		$aAddress = Registry::get('address');

		if (isset($aAddress[3]) && ($nCatId = intval($aAddress[3]))) {
			if (isset($_POST['reset'])) {
				$this->redirect(sprintf('%s/%d', $this->strMethodName, $nCatId));
			}

			// Страница редактировани работы
			if (isset($aAddress[4]) && ($nId = intval($aAddress[4]))) {
				
				if (isset($aAddress[5]) && $aAddress[5]=='del') {
					works::del($nId);
					$this->redirect($this->strMethodName);
				}
				
				$strPageName = 'Редактирование работы';
	
				// Сохранение работы
				if (isset($_POST['work'])) {
					$nAction = (works::set($_POST['work'], $nCatId, $nId)) ? 1 : 2;
				}
	
				$this->oSmarty->assign('aWork', works::get($nId));
				
			// Страница добавления работы
			} elseif (isset($aAddress[4]) && $aAddress[4]=='new') {
				$strPageName = 'Новая работа';
				
				if (isset($_POST['work'])) {
					$nId = works::set($_POST['work'], $nCatId);
					$this->redirect(sprintf('%s/%d/%d', $this->strMethodName, $nCatId, $nId));
				}
				
			// Список работ
			} else {
				$strPageName = 'Работы';
				$this->oSmarty->assign('aWorks', works::getAll($nCatId));
			}
		} 
		else $this->redirect('work_categories');

		$this->oSmarty->assign('aCategory', works::getCat($nCatId));
		$this->oSmarty->assign('nAction', $nAction);
		$this->oSmarty->assign('strPageName', $strPageName);
		$this->oSmarty->display('works.tpl');
	}
	
	
	public function work_categories() {
		$nAction = 0;
		$aAddress = Registry::get('address');

		if (isset($_POST['reset'])) {
			$this->redirect($this->strMethodName);
		}

		// Страница редактировани категории
		if (isset($aAddress[3]) && ($nId = intval($aAddress[3]))) {
			if (isset($aAddress[4]) && $aAddress[4]=='del') {
				works::delCat($nId);
				$this->redirect($this->strMethodName);
			}
			$strPageName = 'Редактирование рубрики';

			// Сохранение категории
			if (isset($_POST['category'])) {
				$nAction = (works::setCat($_POST['category'], $nId)) ? 1 : 2;
			}

			$this->oSmarty->assign('aCategory', works::getCat($nId));
			
		// Страница добавления категории
		} elseif (isset($aAddress[3]) && $aAddress[3]=='new') {
			$strPageName = 'Новая рубрика';
			
			if (isset($_POST['category'])) {
				$nId = works::setCat($_POST['category']);
				$this->redirect(sprintf('%s/%d', $this->strMethodName, $nId));
			}
			
		// Список категорий
		} else {
			$strPageName = 'Рубрики работ';
			$this->oSmarty->assign('aCategories', works::getAllCats());
		}

		$this->oSmarty->assign('nAction', $nAction);
		$this->oSmarty->assign('strPageName', $strPageName);
		$this->oSmarty->display('works_categories.tpl');
	}
	
	public function pages() {
		$nAction = 0;
		$aAddress = Registry::get('address');

		if (isset($_POST['reset'])) {
				$this->redirect($this->strMethodName);
		}
		
		// Страница редактирования
		if (isset($aAddress[3]) && ($nId = intval($aAddress[3]))) {
			if (isset($aAddress[4]) && $aAddress[4]=='del') {
				pages::del($nId);
				$this->redirect($this->strMethodName);
			}
			$strPageName = 'Редактирование страницы';

			// Сохранение
			if (isset($_POST['page'])) {
				$nAction = (pages::set($_POST['page'], $nId)) ? 1 : 2;
			}

			$this->oSmarty->assign('aPage', pages::get($nId));
		}// Страница добавления
		elseif (isset($aAddress[3]) && $aAddress[3]=='new') {
			$strPageName = 'Новая страница';

			if (isset($_POST['page'])) {
				$nId = pages::set($_POST['page']);
				$this->redirect(sprintf('%s/%d', $this->strMethodName, $nId));
			}

			$this->oSmarty->assign('crumbs', TRUE);

		}// Список
		else {
			$strPageName = 'Страницы';
			$this->oSmarty->assign('aPages', pages::getAll());
		}
		
		$this->oSmarty->assign('nAction', $nAction);
		$this->oSmarty->assign('strPageName', $strPageName);
		$this->oSmarty->display('pages.tpl');
	}


    public function seo_pages() {
        $nAction = 0;
        $aAddress = Registry::get('address');

        if (isset($_POST['reset'])) {
            $this->redirect($this->strMethodName);
        }

        // Страница редактирования
        if (isset($aAddress[3]) && ($nId = intval($aAddress[3]))) {
            if (isset($aAddress[4]) && $aAddress[4]=='del') {
                seo_pages::del($nId);
                $this->redirect($this->strMethodName);
            }
            $strPageName = 'Редактирование страницы';

            // Сохранение
            if (isset($_POST['seo_page'])) {
                $nAction = (seo_pages::set($_POST['seo_page'], $nId)) ? 1 : 2;
            }

            $aItem = seo_pages::get($nId);
            $tree = new tree();

            $this->oSmarty->assign('aCats', $tree->get2(0, 2));
            $this->oSmarty->assign('aFields', seo_pages::getSameFields($aItem['filter']['cat']));
            $this->oSmarty->assign('aItem', $aItem);
        }// Страница добавления
        elseif (isset($aAddress[3]) && $aAddress[3]=='new') {
            $strPageName = 'Новая страница';

            if (isset($_POST['seo_page'])) {
                $nId = seo_pages::set($_POST['seo_page']);
                $this->redirect(sprintf('%s/%d', $this->strMethodName, $nId));
            }

            $tree = new tree();
            $this->oSmarty->assign('aCats', $tree->get2(0, 2));

        }// Список
        else {
            $strPageName = 'Страницы';
            $this->oSmarty->assign('aItems', seo_pages::getAll());
        }

        $this->oSmarty->assign('nAction', $nAction);
        $this->oSmarty->assign('strPageName', $strPageName);
        $this->oSmarty->display('seo_pages.tpl');
    }

		
	public function contacts() {
		$nAction = 0;
		
		if (isset($_POST['options'])) {
			$nAction = (options::set($_POST['options'])) ? 1 : 2;
		}

		$this->oSmarty->assign('nAction', $nAction);
		$this->oSmarty->assign('strPageName', 'Контакты');
		$this->oSmarty->assign('aOptions', options::getAll('contacts'));
		$this->oSmarty->display('index.tpl');
	}
	

	/** Форма авторизации пользователя **/
	public function login() {
		$strMessage = '';

		if (isset($_POST['submit'])) {
			if (isset($_POST['login']) && $_POST['login']!='' && isset($_POST['password']) && $_POST['password']!='') {
				$oUser = new user();

				if ($oUser->login($_POST, $this->strPath)) {
					$this->redirect();
				} else {
					$strMessage = join('<br>', $oUser->getErrors());
				}
			} else $strMessage = 'Введите Ваш логин и пароль';
		}
	
		$this->oSmarty->assign('strMessage', $strMessage);
		$this->oSmarty->assign('strPageName', 'Авторизация');
		$this->oSmarty->display('login.tpl');
	}
	
	
	/** Разлогинивание **/
	public function logout() {
		user::logout($this->strPath);
		$this->redirect();
	}
	
	
	/** Страница не найдена **/
	public function error404() {	
		$this->oSmarty->assign('strPageName', 'Страница не найдена');
		$this->oSmarty->assign('strMessage', 'Страница не найдена');
		$this->oSmarty->display('404.tpl');
		die;
	}
	
	
	/** отрабатывается перед вызванным методом **/
	public function init() {
		$this->oSmarty->assign('strSiteName', options::get(1));

		if (!user::isLogin("")) {
			$this->login();
			die;
		}
		
		$aMenu = array(
			'' => 'Основное',
			'categories' => 'Товары',
			'orders' => 'Заказы',
			'work_categories' => 'Портфолио',
			'articles' => 'Статьи',
			'news' => 'Новости',
			'actions' => 'Акции',
			'banners' => 'Партнеры',
			'reviews' => 'Отзывы',
			//'guarantee' => 'Гарантии',
			'vacancy' => 'Вакансии',
			'pages' => 'Страницы',
			'contacts' => 'Контакты',
			'feedback' => 'Обратная связь',
			'settings' => 'Настройки',
//			'requests' => 'Заявки',
			'others' => 'Остальное',
            'seo_pages' => 'SEO-страницы',
		);
		
		$this->oSmarty->assign('strPageCode', $this->strMethodName);
		$this->oSmarty->assign('strPath', $this->strPath);
		$this->oSmarty->assign('aMenu', $aMenu);
		$this->oSmarty->assign('aCurUser', user::getCurrentUser());
	}
	
	/** редирект **/
	protected function redirect($strUrlPage='', $bLocal=true) {
		header(sprintf("Location: %s", (($bLocal)?$this->strPath:'').'/'.$strUrlPage));
		exit;
	}
        
        public function saveAjax() {
            return array('status' => '', 'price' => '');
        }

        

        /** Перехват вызова метода **/
	public function __call($strMethod, $mParams) {		
		if (!method_exists($this, $strMethod)) {
			$this->error404();			
		} else {
			call_user_func_array(array(&$this, $strMethod));	
		}
	}
}

