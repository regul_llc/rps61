<?php

class Default_Controller
{
	protected $oSmarty = null;
	protected $strPath = '';
	
	public function __call($strMethod, $mParams) {		
		die('sdfasdasd');
		if (!method_exists($this, $strMethod)) {
			$this->error404();			
		} else {
			call_user_func_array(array(&$this, $strMethod));	
		}
	}
	
	
	public function __construct($aParams) {
		$this->oSmarty = new Smarty;
		
		$this->oSmarty->template_dir = $aParams['smarty_template_dir'];
		$this->oSmarty->compile_dir = $aParams['smarty_compile_dir'];
		$this->oSmarty->cache_dir = $aParams['smarty_cache_dir'];
		
		$this->strPath = $aParams['path'];
		$this->oSmarty->assign('strPath', $aParams['path']);
		
		if (isset($aParams['page'])) {
			$this->strMethodName = $aParams['page'];
			$this->oSmarty->assign('strPage', $this->strMethodName);
		}
		
		$this->init();
	}
	
	
	public function error404() {	
		die('<h1>Страница не найдена!</h1>');
	}
	
	
	protected function init() {
		
	}
	
	
	protected function redirect($strUrlPage='') {
		header(sprintf("Location: %s", '/'.$strUrlPage));
		exit;
	}
}
