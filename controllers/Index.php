<?php

class Index_controller extends Default_Controller
{
	protected $oSmarty = null;
	
	
	/** картинка капчи **/
	public function captcha() {
		H::capthca();
	}
	
	
	public function index() {
		$aPage = pages::get(1);
		$this->oSmarty->assign('strPageTitle', 'Главная');
		$this->oSmarty->assign('aMetaInfo', array(
			'title' => $aPage['meta']['title'],
			'description' => $aPage['meta']['description'],
			'keywords' => $aPage['meta']['keywords']
		));
		$this->oSmarty->assign('aGallery', images::getAll(1, 'main'));
		$this->oSmarty->assign('aRandGoods', goods::getRandForIndex());
		$this->oSmarty->assign('aBanners', banners::getAll(3));
		$this->oSmarty->assign('strMainTextTop', $aPage['text']);
		$this->oSmarty->assign('strMainTextBottom', options::get(14));
		$this->oSmarty->assign('aLastWorks', works::getAllCats(false, 2));
		$this->oSmarty->display('index.tpl');
	}
	
	
	public function news() {
		$aAddress = Registry::get('address');

		if (isset($aAddress[2]) && ($nId = intval($aAddress[2]))) {
			$aNew = news::get($nId);
			$this->oSmarty->assign('aNew', $aNew);
			$this->oSmarty->assign('strPageTitle', $aNew['title']);
		} else {
			$this->oSmarty->assign('aNews', news::getAll());
			$this->oSmarty->assign('strPageTitle', 'Новости');
		}
		
		$this->oSmarty->display('news.tpl');
	}
	
	
	public function splitsistemy() {
		$this->oSmarty->assign('strPageTitle', 'Сплит-системы');
		$this->oSmarty->display('catalog.tpl');
	}
	
	public function aboutus() {
		$this->page(2,'aboutus.tpl');
	}
	
	public function service() {
		$this->page(3);
	}
	
	public function actions() {
		$aAddress = Registry::get('address');
		
		if (isset($aAddress[2]) && ($nId = intval($aAddress[2]))) {
        $aAction = actions::get($nId);
        $this->oSmarty->assign('aAction', $aAction);
		$this->oSmarty->assign('aMetaInfo', $aAction['meta']);
        $this->oSmarty->assign('strPageTitle', $aAction['title']);
		}
		else {
			$aOpts = options::getAll('actions');
			$this->oSmarty->assign('aActions', actions::getAll(FALSE));
			$this->oSmarty->assign('aMetaInfo', array(
				'title' => (!isset($aOpts[0]['value']))? '' : $aOpts[0]['value'],
				'description' => (!isset($aOpts[1]['value']))? '' : $aOpts[1]['value'],
				'keywords' => (!isset($aOpts[2]['value']))? '' : $aOpts[2]['value']
			));
			$this->oSmarty->assign('strPageTitle', 'Акции');
		}
		
		$this->oSmarty->display('actions.tpl');
	}

	
	public function delivery() {
		$this->page(4);
	}
	
	public function vacancy() {
		$this->page(5);
	}
        
	public function guarantee() {		
		$this->oSmarty->assign('aImages', images::getAll(1, 'guarantee'));
		$this->oSmarty->assign('isGuarantee', true);
		$this->page(6);
	}
        
        public function publicoffer() {
                $this->page(7);
        }
	
	private function page($id, $template = FALSE) {
		$aPage = pages::get($id);
		$this->oSmarty->assign('aPage', $aPage);
		$this->oSmarty->assign('aMetaInfo', $aPage['meta']);
		$this->oSmarty->assign('strPageTitle', (!empty($aPage['title'])? $aPage['title'] : ''));
		$this->oSmarty->display((!$template)? 'textpage.tpl' : $template);
	}
	
	public function articles() {
		$aAddress = Registry::get('address');
                
		if (isset($aAddress[3])) {
			$redirectPath = '/articles/'.intval($aAddress[2]);
			H::downloadFile(intval($aAddress[2]).'/'.$aAddress[3], $redirectPath, 'article');
			exit;
		}
                
		if (isset($aAddress[2]) && ($nId = intval($aAddress[2]))) {
			$aArticle = articles::get($nId);
			$aMetaInfo = array(
				'title' => $aArticle['meta']['title'],
				'description' => $aArticle['meta']['description'],
				'keywords' => $aArticle['meta']['keywords']
			);
			
			$this->oSmarty->assign('strPageTitle', $aArticle['title']);
			$this->oSmarty->assign('aArticle', $aArticle);
			$this->oSmarty->assign('aImages', images::getAll($nId, 'article'));
		} else {
			$aMetaInfo = array(
				'title' => options::get(20),
				'description' => options::get(21),
				'keywords' => options::get(22)
			);
			$this->oSmarty->assign('strPageTitle', 'Статьи');
			$this->oSmarty->assign('aArticles', articles::getAll());
		}
		
		$this->oSmarty->assign('aMetaInfo', $aMetaInfo);
		$this->oSmarty->display('articles.tpl');
	}
	
	
	public function categories() {
		$aAddress = Registry::get('address');

		if (isset($aAddress[2]) && ($nId = intval($aAddress[2]))) {
			$aCategory = goods::getCat($nId);
			$this->oSmarty->assign('strPageTitle', $aCategory['title']);
			$this->oSmarty->assign('aCategory', $aCategory);
			$this->oSmarty->assign('aGoods', goods::getAll($nId, false));
		} else {
			$this->oSmarty->assign('strPageTitle', 'Рубрики');
		}
		
		$this->oSmarty->display('goods.tpl');
	}
	
	
	public function category() {
		$aAddress = Registry::get('address');

		if (isset($aAddress[2]) && ($nId = intval($aAddress[2]))) {
			$aCategory = goods::getCat($nId, FALSE);
			if (!$aCategory)
				$this->error404();
                        
			$this->oSmarty->assign('strPageTitle', $aCategory['title']);
			$this->oSmarty->assign('aMetaInfo', array(
				'title' => $aCategory['meta']['title'],
				'description' => $aCategory['meta']['description'],
				'keywords' => $aCategory['meta']['keywords']
			));
			$this->oSmarty->assign('aCategory', $aCategory);
			
			$this->oSmarty->display('category.tpl');
		} else $this->redirect();
	}
	

	public function good() {
		$aAddress = Registry::get('address');
                
		if (isset($aAddress[3])) {
			$redirectPath = '/good/'.intval($aAddress[2]);
			H::downloadFile(intval($aAddress[2]).'/'.$aAddress[3], $redirectPath);
			exit;
		}
                
		if (isset($aAddress[2]) && ($nId = intval($aAddress[2]))) {
			$aGood = goods::get($nId);
			if(!$aGood['public']){
				$this->error404();
			}			
			$this->oSmarty->assign('strPageTitle', $aGood['title']);
			$this->oSmarty->assign('aMetaInfo', array(
				'title' => $aGood['meta']['title'],
				'description' => $aGood['meta']['description'],
				'keywords' => $aGood['meta']['keywords']
			));
			$this->oSmarty->assign('aAttGoods', $aGood['attendant_goods']);

			$this->oSmarty->assign('aGood', $aGood);
			$this->oSmarty->assign('aValues', goods::getValues($nId, false));
			$this->oSmarty->assign('aGallery', images::getAll($nId, 'goods'));
		}
		
		$this->oSmarty->display('good.tpl');
	}
	
	public function search() {
		$this->oSmarty->assign('strPageTitle', 'Поиск товаров');
		
		if (isset($_POST['search_req']) && ($strSearchReq = $_POST['search_req'])) {
			$aResult = goods::search($strSearchReq);
			
			$this->oSmarty->assign('strCountResult', H::get_sign(count($aResult), 'найден', 'найдено', 'найдено').' '.count($aResult).' '.H::get_sign(count($aResult), 'товар', 'товара', 'товаров'));
			$this->oSmarty->assign('aResult', $aResult);
		}
		
		$this->oSmarty->display('search.tpl');
	}
	
	public function contacts() {
		$this->oSmarty->assign('strPageTitle', 'Контакты');
		$this->oSmarty->assign('aContacts', options::getAllOptions('contacts'));
		
		$this->oSmarty->display('contacts.tpl');
	}
	
	
	public function reviews() {
		$nAction = 0;
		if (isset($_POST['review'])) {
			if (isset($_POST['review']['author']) && $_POST['review']['author']!='' && isset($_POST['review']['text']) && $_POST['review']['text']!='' && md5($_POST['review']['captcha']) == $_SESSION['captcha']) {
				reviews::set($_POST['review']);
				$nAction = 1;
			} else $nAction = 2;
		}
		
		$aAddress = Registry::get('address');
		
		$this->oSmarty->assign('strPageTitle', 'Отзывы');
		$this->oSmarty->assign('nAction', $nAction);
		$this->oSmarty->assign('bIsModering', options::get(4));
		
		$bAllRew = options::get(4)==0;
		$this->oSmarty->assign('aReviews', reviews::getAll($bAllRew));
		
		$this->oSmarty->display('reviews.tpl');
	}
	
	
	public function catalog() {
		
		$aAddress = Registry::get('address');
		
		//$cart = new cart();
                
		if (isset($aAddress[2]) && ($nCatId = intval($aAddress[2]))) {
			if (isset($_POST['fReset'])) {
				if (isset($_SESSION['filter'][$nCatId]))
					$_SESSION['filter'][$nCatId] = array();
				unset($_POST);
			}

			$aSubCat = goods::getCat($nCatId, FALSE);
			$aCat = goods::getCat($aSubCat['parent_id'], FALSE);
			if (!$aCat)
				$this->error404();
			$this->oSmarty->assign('strPageTitle', 'Каталог товаров');
			$this->oSmarty->assign('aMetaInfo', array(
				'title' => $aSubCat['meta']['title'],
				'description' => $aSubCat['meta']['description'],
				'keywords' => $aSubCat['meta']['keywords']
			));
			
			$this->oSmarty->assign('aCurFilters', goods::getFiltersArray($nCatId));
			
//			if (!isset($_GET['page'])) $_GET['page'] = 1;
			$mGoods =  goods::getAll($nCatId, false, true, " ORDER BY `priority` DESC ");			
			$aPNavigator = H::get_pnavigator_params($mGoods['count'], '/catalog/'.$nCatId, goods::GOODS_PER_PAGE);
			//H::debug_info($mGoods['list']);
			$this->oSmarty->assign('aPNavigator', $aPNavigator);
			$this->oSmarty->assign('aFilter', goods::getFilters($nCatId));
			$this->oSmarty->assign('nCurSubCat', $nCatId);
			$this->oSmarty->assign('nCurCat', $aCat['id']);
			$this->oSmarty->assign('aGoods', $mGoods['list']);
			$this->oSmarty->assign('aSubCategory', $aSubCat);
			$this->oSmarty->assign('aCategory', $aCat);
			$this->oSmarty->display('catalog.tpl');
		} else $this->error404();
	}


    protected function seoPage($aPage) {
        $this->oSmarty->assign('aMetaInfo', array(
            'title' => $aPage['meta_title'],
            'description' => $aPage['meta_description'],
            'keywords' => $aPage['meta_keywords']
        ));

//        if (!isset($_GET['page'])) $_GET['page'] = 1;
        $mGoods =  goods::getAllForSeo($aPage['filter']);
        $aPNavigator = H::get_pnavigator_params($mGoods['count'], '/'.$this->strMethodName.'/', intval($aPage['filter']['ppage']));

        $this->oSmarty->assign('aGoods', $mGoods['list']);
        $this->oSmarty->assign('aPNavigator', $aPNavigator);
        $this->oSmarty->assign('aPage', $aPage);

        $this->oSmarty->display('seo_page.tpl');
    }
	
	
	public function cart() {
		$cart = new cart();
		
		$aAddress = Registry::get('address');

		// удаление товара из корзины
		if (isset($aAddress[2]) && $aAddress[2]=='del' && isset($aAddress[3]) && ($nItem = intval($aAddress[3]))) {
			$cart->delete($nItem);
			$this->redirect('cart');
		}
		
		// пересчет
		if (isset($_POST['recount'])) {
			$cart->update($_POST['cart']);
		}
		
		// предоформление
		if (isset($_POST['order'])) {
			$this->order();
			exit;
		}
		
		$this->oSmarty->assign('aCart', $cart->getCart());
		$this->oSmarty->assign('strPageTitle', 'Корзина');
		$this->oSmarty->display('cart.tpl');
	}
	
	
	public function order() {
		$cart = new cart();
		$nAction = 0;
		if (isset($_POST['confirm']) && isset($_POST['user'])) {
			$nOrder = $cart->order($_POST['user']);
			$this->oSmarty->assign('nOrder', $nOrder);
                        $this->oSmarty->assign('secretkey', SECRET_KEY);
			$strPageTitle = 'Ваш заказ принят';
			$nAction = 2;
		} else {
			$this->oSmarty->assign('aCart', $cart->getCart());
			$strPageTitle = 'Оформление заказа';
			$nAction = 1;
		}
		
		$this->oSmarty->assign('strPageTitle', $strPageTitle);
		$this->oSmarty->assign('nAction', $nAction);
		$this->oSmarty->display('order.tpl');
	}
        
        public function printorder() {
            $aAddress = Registry::get('address');
            if (isset($aAddress[2]) && !empty($aAddress[2])) {
                $aAddress[2] = trim($aAddress[2]);
                $aAddress[2] = stripslashes($aAddress[2]);
                $aAddress[2] = htmlspecialchars($aAddress[2]);
                
                $cart = new cart();
                $aOrder = $cart->getOrder($aAddress[2], TRUE);
                if (!$aOrder)
                    $this->error404();
                $aOrder['summ_words'] = H::num2str(round($aOrder['summ']));
                $this->oSmarty->assign(array(
                    'strPageTitle' => 'Счет № '.intval($aOrder['id']),
                    'aOrder' => $aOrder
                ));
                $this->oSmarty->display('printorder.tpl');
            }
            else
                $this->error404();
        }
	
	
	public function works() {
		$aAddress = Registry::get('address');
		
		if (isset($aAddress[2]) && ($nCat = intval($aAddress[2]))) {
			$aCat = works::getCat($nCat);
			$this->oSmarty->assign('strPageTitle', 'Портфолио :: '.$aCat['title']);
                        $this->oSmarty->assign('bCrumbs', '<a href="/works">Портфолио</a> :: '.$aCat['title']);
			$this->oSmarty->assign('aWorks', works::getAll($nCat));
		} else {
			$this->oSmarty->assign('strPageTitle', 'Портфолио');
			$this->oSmarty->assign('aCats', works::getAllCats());
		}
		
		$this->oSmarty->display('works.tpl');
	}
	
	public function work() {
		$aAddress = Registry::get('address');
		
		if (isset($aAddress[2]) && ($nId = intval($aAddress[2]))) {
			$aItem = works::get($nId);
			$aCat = works::getCat($aItem['cat_id']);
			$this->oSmarty->assign('aWork', $aItem);
			$this->oSmarty->assign('strPageTitle', 'Портфолио :: '.$aItem['title']);
                        $this->oSmarty->assign('bCrumbs', '<a href="/works">Портфолио</a> :: '.$aItem['title']);
			$this->oSmarty->assign('aImages', images::getAll($nId, 'works'));
			
			$this->oSmarty->display('work.tpl');
		} 		
	}
	
	
	/** Страница не найдена **/
	public function error404() {	
		header("HTTP/1.1 404 Page Not Found");
		$this->oSmarty->assign('strPageName', 'Страница не найдена');
		$this->oSmarty->assign('strPageTitle', 'Страница не найдена');
		$this->oSmarty->assign('strMessage', 'К сожалению, страница не существуюет или была удалена.');
		$this->oSmarty->display('under_construction.tpl');
		die;
	}
	
	
	/** отрабатывается перед вызванным методом **/
	public function init() {
		$cart = new cart();
		
		$this->oSmarty->assign('aCartInfo', $cart->getInfo());		
		$this->oSmarty->assign('strSiteName', 'РПС');
		$this->oSmarty->assign('aOptions', options::getAllOptions());
		$this->oSmarty->assign('aLastNews', news::getAll(3));
		//$this->oSmarty->assign('aLastGoods', goods::getLastGoods(10));
		$this->oSmarty->assign('aSilder', images::getImagesForSlider());
		$this->oSmarty->assign('aCategories', goods::getAllCats(false));
		$this->oSmarty->assign('aCatsMenu', goods::getCatsMenu(FALSE));
        $this->oSmarty->assign('strPageCode', $this->strMethodName);

        if ($aPage = seo_pages::search($this->strMethodName)) {
            $this->seoPage($aPage);
            exit;
        }
	}
	
	
	/** Перехват вызова метода **/
	public function __call($strMethod, $mParams) {	
		if (!method_exists($this, $strMethod)) {
			$this->error404();			
		} else {
			call_user_func_array(array(&$this, $strMethod));	
		}
	}
}

