$(function() {
	$('.digit').keyup(function (e) { 
		var strValue = $(this).val();
		
		if (strValue=='') $(this).val('0');
		else {
			var re = /[^0-9\.\-]/gi;
			if (re.test(strValue)) {
				strValue = strValue.replace(re, '');
				$(this).val(strValue)
			}			
		}
	});
	
	$('#request_form').submit(function () { 
		var bReturn = true;
		
		if ($('#name_str').val()=='') {
			$('#name_str').addClass('error');
			bReturn = false;
		} else {
			$('#name_str').removeClass('error');
		}
		
		if ($('#phone_str').val()=='') {
			$('#phone_str').addClass('error');
			bReturn = false;
		} else {
			$('#phone_str').removeClass('error');
		}
		
		var bIsNormCaptcha = checkCaptcha($('#tfCaptcha').val());
		if ($('#tfCaptcha').val()=='' || !bIsNormCaptcha) {
			$('#tfCaptcha').addClass('error');
			bReturn = false;
		} else {
			$('#tfCaptcha').removeClass('error');
		}
		
		if (bReturn) {
			$.ajax({
				type: 'POST',
				dataType: "json",
				async: false,
				url: '/ajax/addrequest',
				data: {
					'name': $('#name_str').val(),
					'phone': $('#phone_str').val(),
					'address': $('#address_str').val(),
					'text': $('#long_text').val()
				},
				success: function(oData) {
					alert('Ваша заявка принята, мы свяжемся с вами в ближайшее время.');
					$('.order-form-container .arcticmodal-close').click();
				}
			});
		}
		
		return false;
	});
	
  $("#phone_str").mask("+7 (999) 999-99-99");

  if ($('.bannercontainer').length > 0) {
    $('.bannercontainer').kenburn(
              {
                thumbWidth:50,
                thumbHeight:50,
                
                thumbAmount:5,                                          
                thumbStyle:"both",
                thumbVideoIcon:"off",
                
                thumbVertical:"bottom",
                thumbHorizontal:"center",             
                thumbXOffset:0,
                thumbYOffset:40,
                bulletXOffset:0,
                bulletYOffset:-16,
                
                hideThumbs:"on",
                                                                                      
                touchenabled:'on',
                pauseOnRollOverThumbs:'off',
                pauseOnRollOverMain:'off',
                preloadedSlides:2,              
                
                timer:7,
                
                debug:"off"              
            });
  }
  
	//Галлерея изображений
  $('.gallery').carouFredSel({
    items: 1,
    scroll: {
      fx: "crossfade"
    },
    auto: false,
    pagination: {
      container: '.gallery-container .thumbnails',
      anchorBuilder: function(nr) {
        var src = $(this).attr('src');
        src = src.replace('/medium/', '/thumbnail/');
        return '<img src="' + src + '">';
      }
    }
  });

  $('.js-no-action').on('click', function(event) {
    event.preventDefault();
  });

  $('.order').on('click', function() {
    $('.order-form-container').arcticmodal();
  });
  $('.calculator').on('click', function() {
    $('.calc-form-container').arcticmodal();
  });



  //Slider gallery in content
  var thumbnailsWidth = 0;
  $('.thumbnails img').each(function() {
    var imgWidth = /*$(this).width()*/133 + parseInt($(this).css('margin-left')) + parseInt($(this).css('margin-right'));
    thumbnailsWidth += imgWidth;
  });
  $('.thumbnails').width(thumbnailsWidth);
  $('.thumbnails img:nth-child(4)').addClass('lastVisible');
  $('.thumbnails img:nth-child(1)').addClass('firstVisible');

  $('.pagination-right').on('click', function() {
    var lastVisibleImage = $('.thumbnails .lastVisible');
    if (!isLast(lastVisibleImage[0])) {
      slideToNext();
    }
    //Turn on arrows
    var linkLeft = $('.pagination-left');
    if (linkLeft.hasClass('disabled')) {
      linkLeft.removeClass('disabled');
    }
  });
  $('.pagination-left').on('click', function() {
    var firstVisibleImage = $('.thumbnails .firstVisible');
    if (!isFirst(firstVisibleImage[0])) {
      slideToPrevious();
    }
    //Turn on arrows
    var linkRight = $('.pagination-right');
    if (linkRight.hasClass('disabled')) {
      linkRight.removeClass('disabled');
    }
  });

  var moveOffset = 32 + 133;
  var images = $('.thumbnails img');  
  function isLast(image) {
    return image === $('.thumbnails img:last-child')[0];
  }
  function isFirst(image) {
    return image === $('.thumbnails img:first-child')[0];
  }
  function slideToNext() {
    $('.thumbnails').animate({left: '-=' + moveOffset}, 400);
    
    //Set firstVisible
    var indexOfFirstVisible = images.index($('.thumbnails .firstVisible'));
    images.siblings('.firstVisible').removeClass('firstVisible');
    images.eq(indexOfFirstVisible + 1).addClass('firstVisible');

    //Set lastVisible
    var indexOfLastVisible = images.index($('.thumbnails .lastVisible'));
    images.siblings('.lastVisible').removeClass('lastVisible');
    images.eq(indexOfLastVisible + 1).addClass('lastVisible');

    var lastVisible = $('.thumbnails .lastVisible');
    if (isLast(lastVisible[0])) {
      $('.pagination-right').addClass('disabled');
    }
  }
  function slideToPrevious() {
    $('.thumbnails').animate({left: '+=' + moveOffset}, 400);
    
    //Set firstVisible
    var indexOfFirstVisible = images.index($('.thumbnails .firstVisible'));
    images.siblings('.firstVisible').removeClass('firstVisible');
    images.eq(indexOfFirstVisible - 1).addClass('firstVisible');

    //Set lastVisible
    var indexOfLastVisible = images.index($('.thumbnails .lastVisible'));
    images.siblings('.lastVisible').removeClass('lastVisible');
    images.eq(indexOfLastVisible - 1).addClass('lastVisible');

    var firstVisible = $('.thumbnails .firstVisible');
    if (isFirst(firstVisible[0])) {
      $('.pagination-left').addClass('disabled');
    }
  }



  //Slider gallery in portfolio
  var workContainerHeight = 0;
  $('.work-container .work').each(function(index) {
    if (index > 1) {
      return;
    };
    var workHeight = $(this).height() + parseInt($(this).css('margin-top')) + parseInt($(this).css('margin-bottom'))
                       + parseInt($(this).css('padding-top')) + parseInt($(this).css('padding-bottom'));
    workContainerHeight += workHeight;
  });
  $('.work-container').height(workContainerHeight);
  $('.work-container .work:nth-child(1)').addClass('firstVisible');
  $('.work-container .work:nth-child(2)').addClass('lastVisible');

  $('.pagination-down a').on('click', function() {
    var lastVisibleBlock = $('.work-container .lastVisible');
    if (!isLastBlock(lastVisibleBlock[0])) {
      slideToDown();
    }
    //Turn on arrows
    var linkUp = $('.pagination-up');
    if (linkUp.hasClass('disabled')) {
      linkUp.removeClass('disabled');
    }
  });
  $('.pagination-up a').on('click', function() {
    var firstVisibleBlock = $('.work-container .firstVisible');
    if (!isFirstBlock(firstVisibleBlock[0])) {
      slideToUp();
    }
    //Turn on arrows
    var linkDown = $('.pagination-down');
    if (linkDown.hasClass('disabled')) {
      linkDown.removeClass('disabled');
    }
  });

  var elements = $('.work-container .work');  
  function isLastBlock(block) {
    return block === $('.work-container .work:last-child')[0];
  }
  function isFirstBlock(block) {
    return block === $('.work-container .work:first-child')[0];
  }
  function slideToDown() {    
    //Set firstVisible
    var indexOfFirstVisible = elements.index($('.work-container .firstVisible'));
    elements.siblings('.firstVisible').removeClass('firstVisible');
    elements.eq(indexOfFirstVisible + 1).addClass('firstVisible');

    //Set lastVisible
    var indexOfLastVisible = elements.index($('.work-container .lastVisible'));
    elements.siblings('.lastVisible').removeClass('lastVisible');
    elements.eq(indexOfLastVisible + 1).addClass('lastVisible');

    var lastVisibleElement = $('.work-container .lastVisible');
    var moveOffset = lastVisibleElement.height() + parseInt(lastVisibleElement.css('margin-top')) + parseInt(lastVisibleElement.css('margin-bottom'))
                       + parseInt(lastVisibleElement.css('padding-top')) + parseInt(lastVisibleElement.css('padding-bottom')) + 1;
    $('.work-moving-container').animate({top: '-=' + moveOffset}, 400);

    if (isLastBlock(lastVisibleElement[0])) {
      $('.pagination-down').addClass('disabled');
    }
  }
  function slideToUp() {    
    //Set firstVisible
    var indexOfFirstVisible = elements.index($('.work-container .firstVisible'));
    elements.siblings('.firstVisible').removeClass('firstVisible');
    elements.eq(indexOfFirstVisible - 1).addClass('firstVisible');

    //Set lastVisible
    var indexOfLastVisible = elements.index($('.work-container .lastVisible'));
    elements.siblings('.lastVisible').removeClass('lastVisible');
    elements.eq(indexOfLastVisible - 1).addClass('lastVisible');

    var firstVisibleElement = $('.work-container .firstVisible');
    var moveOffset = firstVisibleElement.height() + parseInt(firstVisibleElement.css('margin-top')) + parseInt(firstVisibleElement.css('margin-bottom'))
                       + parseInt(firstVisibleElement.css('padding-top')) + parseInt(firstVisibleElement.css('padding-bottom')) + 1;
    $('.work-moving-container').animate({top: '+=' + moveOffset}, 400);

    if (isFirstBlock(firstVisibleElement[0])) {
      $('.pagination-up').addClass('disabled');
    }
  }
});

if ($('#map').length > 0) {
  ymaps.ready(init);
}
var myMap, 
    myPlacemark;

function init(){ 
    myMap = new ymaps.Map ("map", {
        center: [$('#map').attr('lat'), $('#map').attr('lon')],
        zoom: $('#map').attr('zoom'),
        behaviors: ['default', 'scrollZoom']
    });

    //Создаем и добавляем метку    
    myPlacemark = new ymaps.Placemark([$('#map').attr('lat'), $('#map').attr('lon')], {
        content: 'Наш адрес',
        balloonContent: $('#address').html()
    });    
    myMap.geoObjects.add(myPlacemark);

    // Создание экземпляра элемента управления
    myMap.controls.add(
       new ymaps.control.ZoomControl()
    );

    // Обращение к конструктору класса элемента управления по ключу - кнопки "Схема", "Спутник"
    myMap.controls.add('typeSelector');

    //Добавляем кнопки на карте
    myMap.controls.add('mapTools');
}


function checkCaptcha(strCaptcha) {
	var result = false;
	
	$.ajax({
		type: 'POST',
		dataType: "json",
		async: false,
		url: '/ajax/checkcaptcha',
		data: {
			'captcha': strCaptcha
		},
		success: function(oData) {
			result = oData.result == 1;
		}
	});
	
	return result;
}


function calc() {
    var type = document.getElementById("type");

    var lustraCount = document.getElementById("lustraCount");

    var lustraType = document.getElementById("lustraType");

    var square = document.getElementById("square");

    var result = document.getElementById("result");

    var tochSvetCount = document.getElementById("tochSvetCount");

    var trubCount = document.getElementById("trubCount");

    var price = 0;

    var svetType = 0;

    var otvod = 150;
 
    price += parseInt(type.options[type.selectedIndex].value) * parseFloat(square.value);
    

    price += parseInt(lustraType.options[lustraType.selectedIndex].value) * parseInt(lustraCount.value);


    if (parseInt(tochSvetCount.value) != 0)

    {

        theGroup = document.theForm.with;

        for (var i = 0; i < theGroup.length; i++) {

            if (theGroup[i].checked) {

                svetType = theGroup[i].value;

                break;

            }

        }

        price += parseInt(tochSvetCount.value) * svetType;

    }

    price += parseInt(trubCount.value) * otvod;

    result.innerHTML = price;

}
function price_calc() {
    var type = document.getElementById("price_type");

    var lustraCount = document.getElementById("price_lustraCount");

    var lustraType = document.getElementById("price_lustraType");

    var square = document.getElementById("price_square");

    var result = document.getElementById("price_result");

    var tochSvetCount = document.getElementById("price_tochSvetCount");

    var trubCount = document.getElementById("price_trubCount");

    var price = 0;

    var svetType = 0;

    var otvod = 150;
 
    price += parseInt(type.options[type.selectedIndex].value) * parseFloat(square.value);
    

    price += parseInt(lustraType.options[lustraType.selectedIndex].value) * parseInt(lustraCount.value);


    if (parseInt(tochSvetCount.value) != 0)

    {

        theGroup = document.theForm.with;

        for (var i = 0; i < theGroup.length; i++) {

            if (theGroup[i].checked) {

                svetType = theGroup[i].value;

                break;

            }

        }

        price += parseInt(tochSvetCount.value) * svetType;

    }

    price += parseInt(trubCount.value) * otvod;

    result.innerHTML = price;

}