/**
 * Обработчик для ФОС контактов
 * @params.form
 *     - объект формы
 * @params.fields
 *     - объекты полей формы
 */
function feedhandler(params) {
    var hasError = false,
    dataSend = {};
    
    for (var i in params.fields) {
        if ($.trim(params.fields[i].val()) == '') {
            params.fields[i].addClass('fError');
            hasError = true;
        }
        else
            params.fields[i].removeClass('fError');
        dataSend[i] = $.trim(params.fields[i].val());
    }
    
    if (!hasError) {
        $.ajax({
            url: '/ajax/addfeedback',
            type: 'POST',
            dataType: 'JSON',
            data: dataSend,
            cache: false,
            success: function(oData){
                if (oData.success) {
                    $('input[type=text], textarea', params.form).val('');
                    show_msg('Сообщение отправлено.');
                }
            }
        });
    }
}


$(document).ready(function() {
	
	$('#refCaptcha').click(function(){
		document.getElementById('piccaptcha').setAttribute('src', '/captcha.php?rid=' + Math.random());
		return false;
	});
        
	$(".phone").mask("+7 (999) 999-99-99");

	$('.more-products .product-block:nth-child(even)').addClass('product-block-margin-left');
        
        //[Форма обратной связи в контактах]
        if (document.getElementById('form_feedback')) {
            var feedForm = $('#form_feedback'),
            feedFields = {
                name: $('[name="fename"]', feedForm),
                phone: $('[name="fephone"]', feedForm),
                text: $('[name="fetext"]', feedForm)
            };
            
            feedForm.submit(function(){
                feedhandler({
                    form: feedForm,
                    fields: feedFields
                });
                
                return false;
            });
        }
        //[/Форма обратной связи в контактах]
	
	// проверка формы заказа перед отправкой
	$('#form_order').submit(function() { 
		var name = $('#order_name');
		var phone = $('#order_phone');
		var address = $('#order_address');
                var agreement = $('#agreement input');
                var border_box = $('#agreement .border-box');
		var error = false;
		
		if (name.val()=='') {
			name.addClass('error');
			error = true;
		}
		else name.removeClass('error');
		
		if (phone.val()=='') { 
			phone.addClass('error');
			error = true;
		}
		else phone.removeClass('error');
		
		if (address.val()=='') {
			address.addClass('error');
			error = true;
		}
		else address.removeClass('error');
		
                if (!agreement.prop('checked')) {
                    border_box.addClass('error');
			error = true;
                        console.log('checked');
                }
		else agreement.removeClass('error');
                
		if (error) return false;
	});
	
	
	$('.filters .select select').chosen({disable_search: true});
	$('[placeholder]').placeholder();
	$("a[rel=example_group], a[rel=guarGroup], a[rel=articleGroup]").fancybox({
		'transitionIn'		: 'none',
		'transitionOut'		: 'none',
		'titlePosition' 	: 'over',
		'titleFormat'       : function(title, currentArray, currentIndex, currentOpts) {
		    return '<span id="fancybox-title-over">Image ' +  (currentIndex + 1) + ' / ' + currentArray.length + ' ' + title + '</span>';
		}
	});

	$('.category_item').on('click', function(event) {
		var cat = $(this).attr('cat');
		
		if ($('#sub'+cat).length) {
//			event.preventDefault();
			$('#sub'+cat).slideToggle(200);
		}
	});

	$('.js-no-action').on('click', function(event) {
		event.preventDefault();
	});

	if (!document.getElementById('noImgGood')) {
		$('.product-slider .gallery').carouFredSel({
			items: 1,
			scroll: {
			  fx: "crossfade"
			},
			auto: false,
			pagination: {
			  container: '.product-slider .thumbnails',
			  anchorBuilder: function(nr) {
				var src = $(this).attr('src');
				src = src.replace('/medium/', '/small/');
				return '<img src="' + src + '">';
			  }
			}
		});
	}
  //Slider gallery in content
  var thumbnailsWidth = 0;
  $('.thumbnails img').each(function() {
    var imgWidth = /*$(this).width()*/84 + parseInt($(this).css('margin-left')) + parseInt($(this).css('margin-right'));
    thumbnailsWidth += imgWidth;
  });
  $('.thumbnails').width(thumbnailsWidth);
  $('.thumbnails img:nth-child(3)').addClass('lastVisible');
  $('.thumbnails img:nth-child(1)').addClass('firstVisible');
  $('.pagination-right').on('click', function() {
    var lastVisibleImage = $('.thumbnails .lastVisible');
    if (!isLast(lastVisibleImage[0])) {
      slideToNext();
    }
  });
  $('.pagination-left').on('click', function() {
    var firstVisibleImage = $('.thumbnails .firstVisible');
    if (!isFirst(firstVisibleImage[0])) {
      slideToPrevious();
    }
  });
  var moveOffset = 28 + 84;
  var images = $('.thumbnails img');  
  function isLast(image) {
    return image === $('.thumbnails img:last-child')[0];
  }
  function isFirst(image) {
    return image === $('.thumbnails img:first-child')[0];
  }
  function slideToNext() {
    $('.thumbnails').animate({left: '-=' + moveOffset}, 400);
    
    //Set firstVisible
    var indexOfFirstVisible = images.index($('.thumbnails .firstVisible'));
    images.siblings('.firstVisible').removeClass('firstVisible');
    images.eq(indexOfFirstVisible + 1).addClass('firstVisible');

    //Set lastVisible
    var indexOfLastVisible = images.index($('.thumbnails .lastVisible'));
    images.siblings('.lastVisible').removeClass('lastVisible');
    images.eq(indexOfLastVisible + 1).addClass('lastVisible');
  }  
  function slideToPrevious() {
    $('.thumbnails').animate({left: '+=' + moveOffset}, 400);
    
    //Set firstVisible
    var indexOfFirstVisible = images.index($('.thumbnails .firstVisible'));
    images.siblings('.firstVisible').removeClass('firstVisible');
    images.eq(indexOfFirstVisible - 1).addClass('firstVisible');

    //Set lastVisible
    var indexOfLastVisible = images.index($('.thumbnails .lastVisible'));
    images.siblings('.lastVisible').removeClass('lastVisible');
    images.eq(indexOfLastVisible - 1).addClass('lastVisible');
  }


	$(".shop.number-1 .slider-container").carouFredSel({
		auto 	: false,
		prev	: {	
			button	: ".shop.number-1 .prev",
			key		: "left"
		},
		next	: { 
			button	: ".shop.number-1 .next",
			key		: "right"
		}
	});

	$(".shop.number-2 .slider-container").carouFredSel({
		auto 	: false,
		prev	: {	
			button	: ".shop.number-2 .prev",
			key		: "left"
		},
		next	: { 
			button	: ".shop.number-2 .next",
			key		: "right"
		}
	});

	$(".shop.number-3 .slider-container").carouFredSel({
		auto 	: false,
		prev	: {	
			button	: ".shop.number-3 .prev",
			key		: "left"
		},
		next	: { 
			button	: ".shop.number-3 .next",
			key		: "right"
		}
	});

	/*$(".main-image").carouFredSel({
		scroll: {
			fx: "crossfade"
		}
	});*/
	
	$(".buy").click(function() {
		var good_id = $(this).attr('good_id')
		$.ajax({
			type: 'post',
		    url: '/ajax/add_to_cart/',
		    data: {
		    	good_id: good_id,
		    	amount: 1
		    },
		    success: function (oData) {
				$("#cart_info_amount").html(oData.cart_info_amount);
				$("#cart_info_summ").html(oData.cart_info_summ);
				//alert('Товар добавлен в корзину');
				show_msg('Товар добавлен в корзину', 'ok');
	    	}
		});
		
		return false;
	});
	
	$(".buy_wcount").click(function() { 
		var good_id = $(this).attr('good_id');
		var amount = $('#buy_count').val();
		$.ajax({
			type: 'post',
		    url: '/ajax/add_to_cart/',
		    data: {
		    	good_id: good_id,
		    	amount: amount
		    },
		    success: function (oData) {
				$("#cart_info_amount").html(oData.cart_info_amount);
				$("#cart_info_summ").html(oData.cart_info_summ);
				//alert('Товар добавлен в корзину');
                                show_msg('Товар добавлен в корзину', 'ok');
	    	}
		});
		
		return false;
	});
});
