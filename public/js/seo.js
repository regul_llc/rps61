$(function() {
    $('#filter_cat').change(function() {
        get_fields();
    });

    reNums();
});


function get_fields() {
    $('#filters_list').html('');
    $('#filter_order_by').html('');

    $.ajax({
        url: '/ajax/getFields',
        type: 'POST',
        data: {
            cats: $('#filter_cat').val()
        },
        dataType: 'JSON',
        cache: false,
        success: function(oData) {
            if (oData.error) {
//                alert(oData.message);
            } else {
                $('#filter_fields').html('');
                console.log(oData);
                $(oData.fields).each(function(){
                    var i = $(oData.fields).index(this);
                    $('#filter_fields').prepend('<div style="float: right; padding: 5px; margin: 5px; background-color: #dadada;"><input onchange="checkField(\''+this.name+'\')" style="float: left;" type="checkbox" value="'+this.name+'" name="seo_page[filter][fields][]" id="ff'+replaceAll(' ', '_', this.name)+'"/><label style="float: left; margin: 0 0 0 5px; padding: 0;" for="ff'+replaceAll(' ', '_', this.name)+'">'+this.name+'</label></div>');
                    $('#filter_order_by').append('<option>'+this.name+'</option>');
                });
            }
        }
    });
}

function checkField(name) {
    if(!$('#ff'+replaceAll(' ', '_', name)).is(':checked')) {
        $("#filter"+replaceAll(' ', '_', name)).remove();
        reNums();
        updateCondition();
    } else {
        $.ajax({
            url: '/ajax/getFilter',
            type: 'POST',
            data: {
                name: name,
                count: $('.filter_box').length
            },
            dataType: 'JSON',
            cache: false,
            success: function(oData) {
                if (oData.error) {
//                alert(oData.message);
                } else {
                    reNums();
                    $('#filters_list').append(oData.filter);
                    updateCondition();
                }
            }
        });
    }
}


function reNums() {
    var nums = ['А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З', 'И', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Э', 'Ю', 'Я'];

    $('.filter_box').each(function() {
        $(this).find('.num').html(nums[$(this).index()]);
    })
}

function updateCondition() {
    var strCondition = '';

    $('.filter_box').each(function() {
        strCondition += ($(this).index()>0) ? ' AND ' : '';
        strCondition += $(this).find('.num').html();
    });

    $('#filter_condition').val(strCondition);
}

function replaceAll(find, replace, str) {
    return str.replace(new RegExp(find, 'g'), replace);
}

