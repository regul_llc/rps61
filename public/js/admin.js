function waitStart(parange, loader) {
    parange.removeClass('dhidden');
    loader.removeClass('dhidden');
}
function waitStop(parange, loader) {
    parange.addClass('dhidden');
    loader.addClass('dhidden');
}

$(function() {
	var ajaxParange = $('div.ajaxWoiterCover'),
        ajaxLoader = $('div.ajaxWoiter');
        
        $(".phone").mask("+7 (999) 999-99-99");
	
	tinymce.init({
	    selector: "textarea.editor",
	    language: "ru",
	    plugins: "jbimages, fullscreen, image, link, table, preview, code, media, save, contextmenu",
		image_advtab: true	
	});
	
	$('.delete').click(function() { 
		return confirm('Подтверждаете удаление?');
	});

	$('.empty, .fill').click(function() {
		return confirm('Подтверждаете действие?');
	});

        $('.delFile').unbind('click').click(function(){
            var tr = $(this).parent().parent(),
            tbody = tr.parent(),
            table = tbody.parent();
            $.ajax({
                url: '/ajax/deleteGoodsFile',
                type: 'POST',
                data: { fid: this.getAttribute('fid'), gid: this.getAttribute('gid'), fname: this.getAttribute('fname')},
                dataType: 'JSON',
                cache: false,
                success: function(oData) {
                    if (oData.success) {
                        tr.fadeOut(300, function(){
                            tr.remove();
                            if (tbody.find('tr').size() == 0)
                            table.fadeOut(300);
                        });
                    }
                }
            });
            
            return false;
        });
        
        
        $('.deleterFile').unbind('click').click(function(){
            var tr = $(this).parent().parent(),
            tbody = tr.parent(),
            table = tbody.parent();
            
            $.ajax({
                url: '/ajax/deleteFile',
                type: 'POST',
                data: { fid: this.getAttribute('fid'), itid: this.getAttribute('itid'), fname: this.getAttribute('fname'), ittype: this.getAttribute('ittype')},
                dataType: 'JSON',
                cache: false,
                success: function(oData) {
                    if (oData.success) {
                        tr.fadeOut(300, function(){
                            tr.remove();
                            if (tbody.find('tr').size() == 0)
                                table.fadeOut(300);
                        });
                    }
                }
            });
            
            return false;
        });
        
        
        //[поиск товаров по модели]
        if (document.getElementById('searcherGood')) {
            var searcherGood = $('#searcherGood'),
            tabGoodLister = $('#tabGoodLister');
            
            $('#submGoodSearch').click(function(){                
                var catId = searcherGood.attr('cid'),
                sText = $.trim(searcherGood.val());
                
                waitStart(ajaxParange, ajaxLoader);
                $.ajax({
                    url: '/ajax/searchGoods',
                    type: 'POST',
                    data: {
                        catId: catId,
                        fieldval: sText
                    },
                    dataType: 'JSON',
                    cache: false,
                    success: function(oData) {
                        if (!oData.success) {
                            waitStop(ajaxParange, ajaxLoader);
                            tabGoodLister.html('<tr><td colspan="4">По запросу ' + sText + ' ничего не найдено.</td></tr>');
                            return false;
                        }
                        
                        var tabContent = '',
                        goods = oData.goods;
                        for (var i = 0; i < goods.length; ++i) {
                            tabContent+= '<tr>' +
                            '<td><a href="/admin/goods/' + catId + '/' + goods[i].id + '">' + goods[i].title + ' ' + goods[i].brand + ' ' + goods[i].model + '</a></td>' +
                            '<td style="white-space: nowrap;">' + goods[i].date + '</td>' +
                            '<td style="white-space: nowrap;">' + ((goods[i].public == 1)? 'опубликовано' : 'не опубликовано') + '</td>' + 
                            '<td><a class="edit" href="/admin/goods/' + catId + '/' + goods[i].id + '">Редактировать</td>' +
                            '<td><a class="delete" href="/admin/goods/' + catId + '/' + goods[i].id + '/del">Удалить</td></tr>';
                        }
                        tabGoodLister.html(tabContent);
                        waitStop(ajaxParange, ajaxLoader);
                    }
                });
                
                return false;
            });
        }
        //[/поиск товаров по модели]
	
	$('.del_value').click(function() { 
		return confirm('Если удалить это свойство, то удалятся все значения этого свойства у товаров этой категории. Подтверждаете удаление?');
	});
	
	/*$('#file_upload').uploadify({
		'swf'      : '/public/uploadify.swf',
		'uploader' : '/uploadify.php?cat_id=1&type=1',
		'folder'         : '/uploads',
		'SizeLimit': 1000000000,
		'buttonImage': '/public/uploadify.swf',
		'onQueueComplete' : function(event,data) {
            alert('Файлы загружены. Обновите страницу (кнопка справа, внизу).');
        } 
	});*/
        
        currentDate = new Date();


	$.datepicker.regional['ru'] = { 
		closeText: 'Закрыть', 
		prevText: '&#x3c;Пред', 
		nextText: 'След&#x3e;', 
		currentText: 'Сегодня', 
		monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь', 
		'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'], 
		monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн', 
		'Июл','Авг','Сен','Окт','Ноя','Дек'], 
		dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'], 
		dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'], 
		dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'], 
		dateFormat: 'dd.mm.yy', 
		firstDay: 1, 
		isRTL: false 
		}; 
	$.datepicker.setDefaults($.datepicker.regional['ru']); 
    $("#datepick").datepicker({
        dateFormat: 'dd-mm-yy',
        maxDate: 0,
        changeYear: true 
    }).attr('readonly', 'readonly');
    //$("#datepick").datepicker("setDate", currentDate);
	
	//$( ".datepicker" ).datepicker();

        var attendantGoods = $('input.attGood'),
        attGSearch = $('#attGSearch'),
        attGButtSearch = $('#attGButtSearch'),
        attGButtClose = $('#attGButtClose'),
        attGButtAdd = $('#attGButtAdd'),
        popupGTbody = $('#popupGTbody'),
        mainGTbody = $('#mainGTbody'),
        attGoodsForm = $('#attGoodsForm');
        
        //поиск товара по названию
        attGButtSearch.click(function(){
            var sText = $.trim(attGSearch.val());
            
            if (sText.length < 3)
                return false;
            
            waitStart(ajaxParange, ajaxLoader);
            $.ajax({
                url: '/ajax/searchAttendantGoods',
                type: 'POST',
                data: { title: sText},
                dataType: 'JSON',
                cache: false,
                success: function (oData) {
                    if (!oData.success) {
                        waitStop(ajaxParange, ajaxLoader);
                        popupGTbody.html('<tr><td colspan="2">По запросу ' + sText + ' ничего не найдено.</td></tr>');
                        return false;
                    }
                    
                    var tabContent = '',
                    goods = oData.goods;
                    for (var i in goods) {
                        tabContent+= '<tr>' +
                            '<td><input class="attGood" type="checkbox" gid="' + i + '" gtitle="' + goods[i] + '" /></td>' +
                            '<td>' + goods[i] + '</td>' +
                        '</tr>';
                    }
                    
                    popupGTbody.html(tabContent);
                    waitStop(ajaxParange, ajaxLoader);
                }
            });
            
            return false;
        });
        
        attGButtClose.click(function() {
            attGSearch.val();
            popupGTbody.html('');
            attGoodsForm.css('display', 'none');
        });
        
        //добавление выбранных товаров из попапа (т.е. полученных поиском) в основную таблицу
        attGButtAdd.click(function(){
            if ($('input.attGood:checked').size() > 0) {
                var tbodyContent = '';
                
                $('input.attGood:checked').each(function(){
                    tbodyContent+= '<tr>' +
                        '<td>' + this.getAttribute('gtitle') + '<input name="attGood[' + this.getAttribute('gid') + ']" type="hidden" value="' + this.getAttribute('gtitle') + this.getAttribute('gmodel') + '" /></td>' +
                        '<td><a class="delete delAttendantGood" href="#" gid="' + this.getAttribute('gid') + '" gidup="' + attGButtAdd.attr('gidup') + '">удалить</a>' +
                    '</tr>';
                });
                
                mainGTbody.html(mainGTbody.html() + tbodyContent);
            }
            
            attGSearch.val();
            popupGTbody.html('');
            attGoodsForm.css('display', 'none');
            
            //удаление привязанного товара из основной таблицы
            $('.delAttendantGood').unbind('click').on('click', function(){
                var tr = $(this).parent().parent();

                $.ajax({
                    url: '/ajax/deleteAttGood',
                    type: 'POST',
                    data: { gid: this.getAttribute('gid'), gidup: this.getAttribute('gidup')},
                    dataType: 'JSON',
                    cache: false,
                    success: function(oData) {
                        tr.remove();
                    }
                });

                return false;
            });
            
            return false;
        });
        
        //удаление привязанного товара из основной таблицы
        $('.delAttendantGood').unbind('click').on('click', function(){
            var tr = $(this).parent().parent();
            
            $.ajax({
                url: '/ajax/deleteAttGood',
                type: 'POST',
                data: { gid: this.getAttribute('gid'), gidup: this.getAttribute('gidup')},
                dataType: 'JSON',
                cache: false,
                success: function(oData) {
                    tr.remove();
                }
            });
            
            return false;
        });
        
        //отображаем попап
        $('#viewerAttGPopup').click(function(){
            attGoodsForm.css('display', 'block');
            
            return false;
        });
        
        //[ /попап "Сопутствующие товары"]
	
	$('.del_value').click(function() { 
		return confirm('Если удалить это свойство, то удалятся все значения этого свойства у товаров этой категории. Подтверждаете удаление?');
	});
        
        $( ".price_rub" ).keypress(function() {
            $('.price_ue').val('');
        });
   
});