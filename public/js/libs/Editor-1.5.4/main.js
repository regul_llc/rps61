
$(document).ready(function() {

    var table = $('#tbl_catalog').DataTable({
        "bSort": false,
        
        "language": {
                "paginate": {
                        "last": "Последняя стр.",
                        "first": "Последняя стр.",
                        "next": "Следующая стр.",
                        "previous": "Предыдущая стр.",


                },
                "aria": {
                        "sortAscending": " - click/return to sort ascending",
                        "sortDescending": " - click/return to sort descending"				
                },
                "emptyTable": "No data available in table",
                "info": "Показана _PAGE_ страница из _PAGES_ ",
                "infoEmpty": "No entries to show",
                "infoFiltered": " - filtering from _MAX_ records",
                "infoPostFix": "Показаны все записи",//"All records shown are derived from real information.",
                "decimal": ",",
                "thousands": ".",
                "lengthMenu": "Показать _MENU_ записей",
                "loadingRecords": "Please wait - loading...",
                "processing": "DataTables is currently busy",
                "search": "Искать записи:",
                //"url": "http://www.sprymedia.co.uk/dataTables/lang.txt"
                "zeroRecords": "No records to display"
        }
    });

    $('body').on('click', '#save_ajax', function() {
        //alert(123)
        var data = table.$('input, select').serialize();
        var aData = {'cat_id' : $('#cat_id').val(), 'data' : data};
        console.log(data);
        $.post( "/ajax/setDataTabale", aData);

        return false;
    });
});