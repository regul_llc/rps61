<?php
/*
 * jQuery File Upload Plugin PHP Example 5.14
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

define('DR', $_SERVER['DOCUMENT_ROOT'].'/');

include DR.'lib/registry.php';
include DR.'lib/helper.php';
include DR.'lib/db.php';

H::ConnectToDB();
H::incModels();
/**
 * @todo конечно надо будет сделать для разных типов данных свой набор копий, тип кстати приходит в $_GET['item_type']
 */
$aImageVersions = array(    
    
	// Слайдер на главной
    'main' => array(
    	'upload_dir' => $_SERVER['DOCUMENT_ROOT'].'/public/img/upload/main/',
        'max_width' => 748,
        'max_height' => 329,
        'jpeg_quality' => 80,
        'crop' => true
    ),
    
    // превью в списке товаров
    'list' => array(
    	'upload_dir' => $_SERVER['DOCUMENT_ROOT'].'/public/img/upload/list/',
        'max_width' => 216,
        'max_height' => 155,
        'jpeg_quality' => 80,
        'canvas' => array('r' => 255, 'g' => 255, 'b' => 255)
        //'crop' => true
    ),
    
    // увеличенное изображение в галерее товара
    'medium' => array(
    	'upload_dir' => $_SERVER['DOCUMENT_ROOT'].'/public/img/upload/medium/',
        'max_width' => 343,
        'max_height' => 259,
        'jpeg_quality' => 80,
        'canvas' => array('r' => 255, 'g' => 255, 'b' => 255)
        //'crop' => true
    ),
    
    // превью категорий работ
    'pwork' => array(
    	'upload_dir' => $_SERVER['DOCUMENT_ROOT'].'/public/img/upload/pwork/',
        'max_width' => 318,
        'max_height' => 188,
        'jpeg_quality' => 80,
        'crop' => true
    ),
    
    // превью категорий работ
    'big' => array(
    	'upload_dir' => $_SERVER['DOCUMENT_ROOT'].'/public/img/upload/big/',
        'max_width' => 1200,
        'max_height' => 1200,
        'jpeg_quality' => 80,
        'crop' => false
    ),
    
    // превью фоток для страницы "Гарантии"
    'guarantee' => array(
        'upload_dir' => $_SERVER['DOCUMENT_ROOT'].'/public/img/upload/guarantee/',
        'max_width' => 318,
        'max_height' => 188,
        'jpeg_quality' => 80,
        'crop' => true
    ),
    
    // превью фоток для статей
    'article' => array(
        'upload_dir' => $_SERVER['DOCUMENT_ROOT'].'/public/img/upload/article/',
        'max_width' => 318,
        'max_height' => 188,
        'jpeg_quality' => 80,
        'crop' => true
    ),
    
    // уменьшенное изображение в галерее товара
    'small' => array(
    	'upload_dir' => $_SERVER['DOCUMENT_ROOT'].'/public/img/upload/small/',
        'max_width' => 80,
        'max_height' => 68,
        'jpeg_quality' => 80,
        'crop' => true
    ),
    
    'thumbnail' => array(
        // Uncomment the following to use a defined directory for the thumbnails
        // instead of a subdirectory based on the version identifier.
        // Make sure that this directory doesn't allow execution of files if you
        // don't pose any restrictions on the type of uploaded files, e.g. by
        // copying the .htaccess file from the files directory for Apache:
        //'upload_dir' => dirname($this->get_server_var('SCRIPT_FILENAME')).'/thumb/',
        //'upload_url' => $this->get_full_url().'/thumb/',
        // Uncomment the following to force the max
        // dimensions and e.g. create square thumbnails:
        'crop' => true,
        'max_width' => 133,
        'max_height' => 94
    )
    
    // Uncomment the following version to restrict the size of
    // uploaded images:
    /*
    '' => array(
        'max_width' => 1920,
        'max_height' => 1200,
        'jpeg_quality' => 95
    ),
    */
    // Uncomment the following to create medium sized images:
);

error_reporting(E_ALL | E_STRICT);
require('UploadHandler.php');
$upload_handler = new UploadHandler($_POST);
