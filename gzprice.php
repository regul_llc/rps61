<?php

define('DR', $_SERVER['DOCUMENT_ROOT'].'/');

include DR.'lib/registry.php';
include DR.'lib/helper.php';
include DR.'lib/db.php';

H::connectToDB();
H::incModels();
$db = Registry::get('db');

$strSql = 'SELECT DISTINCT g.`id`, g.`title`, g.`model`, g.`cat_id`
    FROM `goods` AS g, `values_tpl` AS vt
    WHERE g.`cat_id` = vt.`cat_id` AND vt.`type`= 6';
$aTempGoods = $db->_getAll($strSql);
if (empty($aTempGoods))
    return FALSE;
    
$aGoods = array();
$output = '<table><tbody>';
foreach ($aTempGoods as $good) {
    $price = (int)goods::getGoodValue($good['id'], VTYPE_PRICE);
    if ($price == 0) {
        $good['brand'] = goods::getGoodValue($good['id'], VTYPE_BRAND);
        //$aGoods[] = $good;
        $output.= '<tr>
                <td style="padding: 10px;">
                    <a target="_blank" href="/good/'.$good['id'].'">'.$good['title'].' '.$good['brand'].' '.$good['model'].'</a>
                    <a target="_blank" style="margin-left: 15px;" href="/admin/goods/'.$good['cat_id'].'/'.$good['id'].'">Редактировать</a>
                </td>
            </tr>';
    }
}

$output.= '</tbody></table>';

echo $output;