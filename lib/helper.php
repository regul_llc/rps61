<?php
class H {
	
	public static function ConnectToDB($configName = '') {
		if (!empty($configName))
                    $strConfName = $configName;
                elseif (preg_match('/((rps\.loc)|(rprofservice.ru))/', $_SERVER['HTTP_HOST'])) {
			$strConfName = 'localhost';
		} elseif (preg_match('/((rps61\.loc))/', $_SERVER['HTTP_HOST'])) {
			$strConfName = 'localhost';
		} elseif (preg_match('/rps61\.ru/', $_SERVER['HTTP_HOST'])) {
			$strConfName = 'production';
		} else {
			$strConfName = 'default';
		}

		include DR.'conf/'.$strConfName.'.inc';
		
		$oDB = new db($aConf['db']);
		$oDB->connect();
		
		Registry::set('db', $oDB);
	}
	
	
	public static function incModels() {
		$strModelsPath = DR.'lib/models';
		
		if (is_dir($strModelsPath)) {
			$strFiles = scandir($strModelsPath);
			$aModels = array();
			
			foreach ($strFiles as $strFile) {
				if (preg_match('/\.php/', $strFile)) {
					$aModels[] = $strFile;
				}
			}		
			
			foreach ($aModels as $aModel)	 {
				include_once($strModelsPath.'/'.$aModel);
			}
		}
	}
	
        /**
         * Сумма прописью
         */
        //Склоняем словоформу
        private static function morph($n, $f1, $f2, $f5) {
            $n = abs(intval($n)) % 100;
            if ($n>10 && $n<20) return $f5;
            $n = $n % 10;
            if ($n>1 && $n<5) return $f2;
            if ($n==1) return $f1;
            
            return $f5;
        }
        //Возвращает сумму прописью
        public static function num2str($num) {
            $nul='ноль';
            $ten=array(
                array('','один','два','три','четыре','пять','шесть','семь', 'восемь','девять'),
                array('','одна','две','три','четыре','пять','шесть','семь', 'восемь','девять'),
            );
            $a20=array('десять','одиннадцать','двенадцать','тринадцать','четырнадцать' ,'пятнадцать','шестнадцать','семнадцать','восемнадцать','девятнадцать');
             $tens=array(2=>'двадцать','тридцать','сорок','пятьдесят','шестьдесят','семьдесят' ,'восемьдесят','девяносто');
            $hundred=array('','сто','двести','триста','четыреста','пятьсот','шестьсот', 'семьсот','восемьсот','девятьсот');
            $unit=array( // Units
                array('копейка' ,'копейки' ,'копеек',	 1),
                array('рубль'   ,'рубля'   ,'рублей'    ,0),
                array('тысяча'  ,'тысячи'  ,'тысяч'     ,1),
                array('миллион' ,'миллиона','миллионов' ,0),
                array('миллиард','милиарда','миллиардов',0),
            );
            //
            list($rub,$kop) = explode('.',sprintf("%015.2f", floatval($num)));
            $out = array();
            if (intval($rub)>0) {
                foreach(str_split($rub,3) as $uk=>$v) { // by 3 symbols
                    if (!intval($v)) continue;
                    $uk = sizeof($unit)-$uk-1; // unit key
                    $gender = $unit[$uk][3];
                    list($i1,$i2,$i3) = array_map('intval',str_split($v,1));
                    // mega-logic
                    $out[] = $hundred[$i1]; # 1xx-9xx
                    if ($i2>1) $out[]= $tens[$i2].' '.$ten[$gender][$i3]; # 20-99
                    else $out[]= $i2>0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
                    // units without rub & kop
                    if ($uk>1) $out[]= self::morph($v,$unit[$uk][0],$unit[$uk][1],$unit[$uk][2]);
                } //foreach
            }
            else
                $out[] = $nul;
            $out[] = self::morph(intval($rub), $unit[1][0],$unit[1][1],$unit[1][2]); // rub
            $out[] = $kop.' '.self::morph($kop,$unit[0][0],$unit[0][1],$unit[0][2]); // kop
            
            return trim(preg_replace('/ {2,}/', ' ', join(' ',$out)));
        }
        
        
	
	public static function calulate_date_from_sec($sec) {
		$siy = 60*60*24*30*12; 		// Секунд в году
		$sim = 60*60*24*30; 		// Секунд в месяце
		$sid = 60*60*24;			// Секунд в дне
		$sih = 60*60;				// Секунд в часу
		$sii = 60;					// Секунд в минуте
		
		$cy = floor($sec/$siy); // Прошло лет
		$cm = floor(($sec-$siy*$cy)/$sim);
		$cd = floor(($sec-$cy*$siy-$cm*$sim)/$sid);
		$ch = floor(($sec-$cy*$siy-$cm*$sim-$cd*$sid)/$sih);
		$ci = floor(($sec-$cy*$siy-$cm*$sim-$cd*$sid-$ch*$sih)/$sii);
		$cs = floor($sec-$cy*$siy-$cm*$sim-$cd*$sid-$ch*$sih-$ci*$sii);
		
		$result = array();
		
		if ($cy>0) $result['y'] = array(
			'count' => $cy,
			'sign' => self::get_sign($cy, 'y')
		);
		if ($cm>0) $result['m'] = array(
			'count' => $cm,
			'sign' => self::get_sign($cm, 'm')
		);
		if ($cd>0) $result['d'] = array(
			'count' => $cd,
			'sign' => self::get_sign($cd, 'd')
		);
		if ($ch>0) $result['h'] = array(
			'count' => $ch,
			'sign' => self::get_sign($ch, 'h')
		);
		if ($ci>0) $result['i'] = array(
			'count' => $ci,
			'sign' => self::get_sign($ci, 'i')
		);
		if ($cs>0) $result['s'] = array(
			'count' => $cs,
			'sign' => self::get_sign($cs, 's')
		);
		
		return $result;
	}
	
	
	/** [T] Подписи к разным еденицам
	 * 
	 * Коды едениц
	 * y - Года
	 * m - Месяцы
	 * d - Дни
	 * h - Часы
	 * i - Минуты
	 * s - Секунды
	 *
	 * @param int $n Число
	 * @param str $code Код еденицы
	 * @return str название еденицы в нужной форме */
	static public function _get_sign($n, $code) {
		$n = (int)$n;
		if ((11>$n)||($n>19)) {
			$n = (string)$n;
			switch (substr($n,-1,1)) {
				case 1: $result = array(
					'y' => 'Год', 
					'm' => 'Месяц', 
					'd' => 'День',
					'h' => 'Час',
					'i' => 'Минута',
					's' => 'Секунда',
				); break;
				case 2: case 3:  case 4: $result = array(
					'y' => 'Года', 
					'm' => 'Месяца', 
					'd' => 'Дня',
					'h' => 'Часа',
					'i' => 'Минута',
					's' => 'Секунды',
				); break;
				default: $result = array(
					'y' => 'Лет', 
					'm' => 'Месяцев', 
					'd' => 'Дней',
					'h' => 'Часов',
					'i' => 'Минут',
					's' => 'Секунд',
				);
			}
		} else {
			$result = array(
				'y' => 'Лет', 
				'm' => 'Месяцев', 
				'd' => 'Дней',
				'h' => 'Часов',
				'i' => 'Минут',
				's' => 'Секунд',
			);
		}
		
		return $result[$code];
	}
	
	public static function isCode($strCode) {
		return preg_match("/^[a-zA-Z0-9_-]+$/", $strCode) > 0;
	}
	
	
	public static function tpl($template, $val_fields = array()) {
		preg_match_all("/{([^}\s]*)}/i",$template,$res);
		
		foreach ($res[0] as $n=>$v)	{
            if (isset($val_fields[$res[1][$n]])) {
			    $template = str_replace($v, $val_fields[$res[1][$n]], $template);
            }
		}
		
		return $template; 
	}
	
	/** w3captcha - php-скрипт для генерации изображений CAPTCHA
	 * версия: 1.1 от 08.02.2008
	 * разработчики: http://w3box.ru
	 * тип лицензии: freeware
	 * w3box.ru © 2008 */
	public static function capthca() {
		
		session_start();
		
		$count=5;	/* количество символов */
		$width=100; /* ширина картинки */
		$height=48; /* высота картинки */
		$font_size_min=32; /* минимальная высота символа */
		$font_size_max=32; /* максимальная высота символа */
		$font_file=DR."/public/fonts/Comic_Sans_MS.ttf"; /* путь к файлу относительно w3captcha.php */
		$char_angle_min=-10; /* максимальный наклон символа влево */
		$char_angle_max=10;	/* максимальный наклон символа вправо */
		$char_angle_shadow=5;	/* размер тени */
		$char_align=40;	/* выравнивание символа по-вертикали */
		$start=5;	/* позиция первого символа по-горизонтали */
		$interval=16;	/* интервал между началами символов */
		$chars="0123456789"; /* набор символов */
//		$chars = join('', array_merge(range('a','z'), range('A','Z'), range('1','9')));	

		$noise=10; /* уровень шума */
		
		$image=imagecreatetruecolor($width, $height);
		
		$background_color=imagecolorallocate($image, 255, 255, 255); /* rbg-цвет фона */
		$font_color=imagecolorallocate($image, 32, 64, 96); /* rbg-цвет тени */
		
		imagefill($image, 0, 0, $background_color);
		
		$str="";
		
		$num_chars=strlen($chars);
		for ($i=0; $i<$count; $i++)
		{
			$char=$chars[rand(0, $num_chars-1)];
			$font_size=rand($font_size_min, $font_size_max);
			$char_angle=rand($char_angle_min, $char_angle_max);
			imagettftext($image, $font_size, $char_angle, $start, $char_align, $font_color, $font_file, $char);
			imagettftext($image, $font_size, $char_angle+$char_angle_shadow*(rand(0, 1)*2-1), $start, $char_align, $background_color, $font_file, $char);
			$start+=$interval;
			$str.=$char;
		}
		
		if ($noise)
		{
			for ($i=0; $i<$width; $i++)
			{
				for ($j=0; $j<$height; $j++)
				{
					$rgb=imagecolorat($image, $i, $j);
					$r=($rgb>>16) & 0xFF;
					$g=($rgb>>8) & 0xFF;
					$b=$rgb & 0xFF;
					$k=rand(-$noise, $noise);
					$rn=$r+255*$k/100;
					$gn=$g+255*$k/100;		
					$bn=$b+255*$k/100;
					if ($rn<0) $rn=0;
					if ($gn<0) $gn=0;
					if ($bn<0) $bn=0;
					if ($rn>255) $rn=255;
					if ($gn>255) $gn=255;
					if ($bn>255) $bn=255;
					$color=imagecolorallocate($image, $rn, $gn, $bn);
					imagesetpixel($image, $i, $j , $color);					
				}
			}
		}
		
		$_SESSION["captcha"] = md5($str);
		
		if (function_exists("imagepng"))
		{
			header("Content-type: image/png");
			imagepng($image);
		}
		elseif (function_exists("imagegif"))
		{
			header("Content-type: image/gif");
			imagegif($image);
		}
		elseif (function_exists("imagejpeg"))
		{
			header("Content-type: image/jpeg");
			imagejpeg($image);
		}
		
		imagedestroy($image);
	}

    public static function isEmail($strEmail) {
        $strEmail = trim($strEmail);
        return filter_var($strEmail, FILTER_VALIDATE_EMAIL);
    }

	public static function mail($strFromName, $strToEmail, $strToName, $strSubject, $strBody, $aDopEmails, $isHtml=true) {
        if (self::isEmail($strToEmail)) {
            $isHtml = (bool)$isHtml;
            $oMail = new PHPMailer;
            $oMail->IsMail();
            $oMail->AddReplyTo($strToEmail);
            $oMail->From = 'info@rps61.ru';
            $oMail->FromName = $strFromName;
            $oMail->AddAddress($strToEmail,$strToName);

            foreach($aDopEmails as $strDopEmail) {
                if (self::isEmail($strDopEmail)) {
                    $oMail->AddBCC(trim($strDopEmail));
                }
            }

            $oMail->CharSet = 'utf-8';
            $oMail->Subject = $strSubject;
            $oMail->Body = $strBody;
            if($isHtml)  $oMail->MsgHTML($strBody);

            $oMail->IsHTML($isHtml);

            if(!$oMail->Send()) {
                exit('Извините, невозможно отправить письмо для '.$strToEmail.'. Пожалуйста попробуйте позже... error = '.$oMail->ErrorInfo);
            }
        }

        else exit('не валидный email');
	}
	
	
	public static function get_sign($n, $f1, $f2, $f3) {
		$n = (int)$n;
		if ((11>$n)||($n>19)) {
			$n = (string)$n;
			switch (substr($n,-1,1)) {
				case 1: return $f1; break;
				case 2: case 3:  case 4: return $f2; break;
				default: return $f3;
			}
		} else {
			return $f3;
		}
	}


    /** Возвращает масиав страниц для постраничного навигатора
     *
     * @param int $nElements - Количество элементов
     * @param string $strPath - Путь до страницы
     * @param int $nPerPage - Количество элементов на страницу
     * @param bool|int $bOnliPageInLink
     * @return array
     */
	public static function get_pnavigator_params($nElements, $strPath, $nPerPage, $bOnliPageInLink=false) {
		$nHand = 7;	// крыло
		$aResult = array();

        if (isset($_GET['page']) && $_GET['page']==1) {
            header('Location: '.$strPath);
            exit();
        }

        if (!isset($_GET['page'])) $_GET['page']=1;

		$nPages = ceil($nElements / $nPerPage);

		$aResult['pages'] = array();
		if ($bOnliPageInLink) {
			$aGet = array();
			$aGet['page'] = $aGet['page'];
		} else {
			$aGet = $_GET;
		}
		
		$nPageFrom = ($_GET['page']-$nHand > 0) ? $_GET['page']-$nHand : 1;
		$nPageTo = ($_GET['page'] + $nHand <= $nPages) ? $_GET['page'] + $nHand : $nPages;
		
		for ($i=$nPageFrom; $i<=$nPageTo; $i++) {
			$aGet['page'] = $i;
			$aResult['pages'][] = array(
				'title' => $i,
				'url' => ($_GET['page'] != $i) ? $strPath.(($i!=1) ? '?'.http_build_query($aGet) : '') : ''
			);
		}

        $aResult['next'] = $aResult['prev'] = $aResult['first'] = $aResult['last'] = $strPath;

		$aGet['page'] = (($_GET['page'] + 1) > $nPages) ? 1 : ($_GET['page'] + 1);
        if ($aGet['page']!=1) $aResult['next'] .= '?'.http_build_query($aGet);

		$aGet['page'] = (($_GET['page'] - 1) < 1) ? $nPages : ($_GET['page'] - 1);
        if ($aGet['page']!=1) $aResult['prev'] .= '?'.http_build_query($aGet);

/*//		$aGet['page'] = 1;
        if ($aGet['page']!=1) $aResult['first'] .= '?'.http_build_query($aGet);*/

		$aGet['page'] = $nPages;
        if ($aGet['page']!=1) $aResult['last'] .= '?'.http_build_query($aGet);
		/*echo '<pre>';
		print_r($nElements);
		die;*/
		return $aResult;
	}
        
        public static function translite($str) {
            $converter = array(

                'а' => 'a',   'б' => 'b',   'в' => 'v',

                'г' => 'g',   'д' => 'd',   'е' => 'e',

                'ё' => 'e',   'ж' => 'zh',  'з' => 'z',

                'и' => 'i',   'й' => 'y',   'к' => 'k',

                'л' => 'l',   'м' => 'm',   'н' => 'n',

                'о' => 'o',   'п' => 'p',   'р' => 'r',

                'с' => 's',   'т' => 't',   'у' => 'u',

                'ф' => 'f',   'х' => 'h',   'ц' => 'c',

                'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',

                'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',

                'э' => 'e',   'ю' => 'yu',  'я' => 'ya',



                'А' => 'A',   'Б' => 'B',   'В' => 'V',

                'Г' => 'G',   'Д' => 'D',   'Е' => 'E',

                'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',

                'И' => 'I',   'Й' => 'Y',   'К' => 'K',

                'Л' => 'L',   'М' => 'M',   'Н' => 'N',

                'О' => 'O',   'П' => 'P',   'Р' => 'R',

                'С' => 'S',   'Т' => 'T',   'У' => 'U',

                'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',

                'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',

                'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',

                'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',

            );

            return strtr($str, $converter);

        }
        
        public static function downloadFile ($file, $redirectPath, $type = 'good') {
            $file = trim($file);
            $file = stripslashes($file);
            $file = htmlspecialchars($file, ENT_QUOTES);
            
            $filePath = DR.'data/'.$type.'_files/'.$file;
            $file = substr($file, mb_strpos($file, '/') + 1);
            
            if (!is_file($filePath)) {
                clearstatcache();
                header('Location: '.$redirectPath);
            }
            else {
                header("Content-Type: application/octet-stream");
                header("Accept-Ranges: bytes");
                header("Content-Length: ".  filesize($filePath));
                header("Content-Disposition: attachment; filename=".$file);
                readfile($filePath);
            }
        }
        
        public static function debug_info($var, $detailed = FALSE, $die = TRUE)
        {
            $function = (!$detailed)? 'print_r' : 'var_dump';
            echo '<pre>';
            $function($var);
            echo '</pre>';
            if($die)
                exit;
        }
}