<?php

class user {
	const TABLE = 'users';
	const FIELD_FOR_IP_BINDING = 'binding_to_ip';
	const MULTY_ACCESS = true; // Можно ли сидеть под одним доступом в несколько лиц
	
	protected $oDb = null;
	
	protected $aErrors = array();	
	
	protected $aErrorsLib = array(
		1 => 'Логин может состоять только из букв английского алфавита и цифр',
		2 => 'Логин должен быть не меньше 3-х символов и не больше 10',
		3 => 'Пользователь с таким логином уже существует',
		4 => 'Не правильный формат EMail',
		5 => 'Вы ввели неправильный логин/пароль',
		6 => 'Произшла ошибка при обращении к DB'
	);
	
	public function __construct() {
		$this->oDb = Registry::get('db');
	}
	
	public function add($aParams) {
		$oDb = Registry::get('db');
		
		$strLogin = db::clear($aParams['login']);
		$strPassWord = db::clear($aParams['password']);
		$strName = db::clear($aParams['name']);
		$strEmail = db::clear($aParams['email']);
		
		if (!preg_match("/^[a-zA-Z0-9]+$/", $strLogin)) $this->e(1); // Проверка логина на символы			
		if (strlen($strLogin) < 3 || strlen($strLogin) > 10) $this->e(2);
		if (!$this->isValidEmail($strEmail)) $this->e(4);
		
		$nCount = $oDb->getOne(self::TABLE, 'COUNT(id)', db::p("login = '?s'", $strLogin));
	    if ($nCount>0) $this->e(3);
	    
	    if (!$this->isError()) {
	    	$strPassWord = md5(md5($strPassWord));      
	    	  
	    	$nUser = $oDb->add(self::TABLE, array(
	    		'login' => $strLogin,
	    		'password' => $strPassWord,
	    		'name' => $strName,
	    		'email' => $strEmail
	    	));
			
	    	if ($nUser) {
	    		$this->nId = $nUser;
	    		return true;
	    	} else return false;
	    } else return false;
	}
	
	
	public function login($aParams, $strPath) {
		$strLogin = db::clear($aParams['login']);
		$strPassWord = db::clear($aParams['password']);
		$bBindingToIp = (isset($aParams[self::FIELD_FOR_IP_BINDING]) && $aParams[self::FIELD_FOR_IP_BINDING]) ? true : false;

		$aUser = $this->oDb->getRow(self::TABLE, db::p("login = '?s'", $strLogin));

		if (isset($aUser['id'])) {
			if ($aUser['password'] === md5(md5($strPassWord))) {
				$strHash = md5($this->getRandHash(10));
				
				$aUpdate = array(
					'hash' => $strHash
				);
				
				if ($bBindingToIp) {
					$aUpdate['ip'] = db::p("INET_ATON('?s')", $_SERVER['REMOTE_ADDR']);
				}
				
				$bResult = $this->oDb->update(self::TABLE, $aUpdate, db::p("id=?i", $aUser['id']));
				if (!$bResult) $this->e(6);
				else { 
					setcookie('id', $aUser['id'], time()+60*60*240, $strPath, '.'.$_SERVER['SERVER_NAME']);	
	        		setcookie('hash', $strHash, time()+60*60*240, $strPath, '.'.$_SERVER['SERVER_NAME']);
					return true;
				}
			} else $this->e(5);
		} else $this->e(5);
		
		return false;
	}
	
	
	static public function logout($strPath) {
		setcookie('id', '', time()-1, $strPath, '.'.$_SERVER['SERVER_NAME']);		
		setcookie('hash', '', time()-1, $strPath, '.'.$_SERVER['SERVER_NAME']);
	}
	
	
	static public function getCurrentUser() {
		if (isset($_COOKIE['id'])) {
			$oDb = Registry::get('db');
			
			return $oDb->getRowById(self::TABLE, $_COOKIE['id']);
		} else return false;
	}
	
	
	static public function isLogin($strPath) {
		if (isset($_COOKIE['id']) and isset($_COOKIE['hash'])) {   
			$oDb = Registry::get('db');
			
			$aUser = $oDb->_get(db::p("SELECT *, INET_NTOA(ip) FROM ?n WHERE id = ?i", self::TABLE, $_COOKIE['id']));		
		
			$bIsHash = !(self::MULTY_ACCESS || (!self::MULTY_ACCESS && ($aUser['hash'] == $_COOKIE['hash'])));
			
		    if(
		    	($bIsHash) or 
		    	($aUser['id'] !== $_COOKIE['id']) or 
		    	(
		    		($aUser['ip'] !== $_SERVER['REMOTE_ADDR'])  and 
		    		($aUser['ip'] !== "0")
		    	)
		    ) {		
		        setcookie('id', '', time()-1, $strPath, '.'.$_SERVER['SERVER_NAME']);		
				setcookie('hash', '', time()-1, $strPath, '.'.$_SERVER['SERVER_NAME']);
		
		        return false;		
		    } else return true;
		} else return false;
	}
	
	
	public function isValidEmail($estrEail) {
		if ($estrEail=="") return false;
		return (!eregi('|([a-z0-9_\.\-]{1,20})@([a-z0-9\.\-]{1,20})\.([a-z]{2,4})|is',$estrEail)) ? false : true ;
	}
	
	public function e($nError) {
		$this->aErrors[] = $this->aErrorsLib[$nError];
	}
	
	public function getErrors() {
		return $this->aErrors;
	}
	
	public function isError() {
		return count($this->aErrors)>0;
	}
	
	
	public function getRandHash($nLength=6) {
		$strChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHI JKLMNOPRQSTUVWXYZ0123456789";
	    $strCode = "";
	
	    $nCountChars = strlen($strChars) - 1;  
	    while (strlen($strCode) < $nLength) {	
	            $strCode .= $strChars[mt_rand(0,$nCountChars)];  
	    }
	
	    return $strCode;
	}
	
}
