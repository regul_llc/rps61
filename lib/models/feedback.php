<?php

class feedback {
    const TABLE = 'feedback';
    
    static public function _getAll() {
        $db = Registry::get('db');
        
        return $db->getAll(self::TABLE, '1 ORDER BY date_add DESC');
    }
    
    static public function del($nId) {
        if ($nId > 0) {
            $db = Registry::get('db');
            
            return $db->delete(self::TABLE, db::p('id = ?i', $nId));
        }
        
        return FALSE;
    }
    
    static public function get($nId) {
        $db = Registry::get('db');
        return $db->getRow(self::TABLE, db::p('id = ?i', $nId));
    }
    
    static public function set($aParams) {
        $db = Registry::get('db');

        $strMessageTpl = '
			<html>
			    <head>
			        <title>Новое сообщение из обратной связи</title>
			    </head>
			    <body>
			        <h4>Новое сообщение из обратной связи</h4>
			        от: <b>{name}</b><br>
			        телефон: <b>{phone}</b><br><br>
                    {text}
			    </body>
			</html>
		';

        $aDopEmails = explode(',', options::get(26));

        H::mail(options::get(1), options::get(2), $_POST['name'], 'Новое сообщение из обратной связи', H::tpl($strMessageTpl, $_POST), $aDopEmails);
        
        $aParams['date_add'] = time();
        return $db->add(self::TABLE, $aParams);
    }
}