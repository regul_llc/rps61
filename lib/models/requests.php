<?php

class requests {
	const TABLE = 'requests';
	
	static public function getAll() {
		$db = Registry::get('db');
		return $db->getAll(self::TABLE, '1 ORDER BY date_add');
	}
	
	
	static public function set($aParams, $nId=0) {		
		$db = Registry::get('db');
		
		$aParams['status'] = (isset($aParams['status']) && $aParams['status']=='on') ? 1 : 0;

		if ($nId > 0) {
			return $db->update(self::TABLE, $aParams, db::p('id = ?i', $nId));
		} else {
			$aParams['date_add'] = time();
			$aParams['status'] = 0;
			return $db->add(self::TABLE, $aParams);
		}
		
		return false;
	}	
	
	
	static public function get($nId) {
		$db = Registry::get('db');
		return $db->getRow(self::TABLE, db::p('id = ?i', $nId));
	}
	
	static public function del($nId) {
		$db = Registry::get('db');
		return $db->delete(self::TABLE, db::p('id = ?i', $nId));
	}
	
	
	static public function send_mail($strEmailTo, $strNameTo, $strSubject, $strText) {
	
		$strMessageTpl = '
			<html>
			    <head>
			        <title>{title}</title>
			    </head>
			    <body>
			        <p>{text}</p>
			    </body>
			</html>
		';
		
		H::mail('example@potolki.com', $strEmailTo, $strNameTo, $strSubject, H::tpl($strMessageTpl, array(
			'title' => $strSubject,
			'text' => $strText
		)));
	}
}