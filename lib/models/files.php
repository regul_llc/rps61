<?php

class files {
    const TABLE = 'files';
    
    /**
     * Получаем список файлов в директории товара
     * @param (integer) $nItemId - идетификатор объекта, для которого загружается файл
     * @param (string) $itemType - тип объекта, для которого загружается файл
     */
    static public function getList($nItemId, $itemType) {
        $path = DR.'data/'.$itemType.'_files/'.$nItemId;
        $files = glob($path.'/*.{xlsx,xls,csv,pdf,txt,docx,doc}', GLOB_BRACE);
        return (!empty($files))? $files : FALSE;
    }
    
    
    /**
     * Удаляем 1 прикрепленный файл
     * @param (integer) $nId - идентификатор
     * @param (string) $filename - имя файла (с его расширением)
     * @param (intger) $nItemId - идетификатор объекта, для которого загружается файл
     * @param (string) $itemType - тип объекта, для которого загружается файл
     */
    static public function delete($nId, $filename, $nItemId, $itemType) {
        $db = Registry::get('db');
        
        $filename = db::clear($filename);
        $pathFile = DR.'data/'.$itemType.'_files/'.$nItemId.'/'.$filename;

        return ($db->delete(self::TABLE, db::p('id = ?i', $nId)) && unlink($pathFile));
    }
    
    /**
     * Удаляем все прикрепленные файлы у конкретного объекта, для которого загружается файл
     * @param (intger) $nItemId - идетификатор объекта, для которого загружается файл
     * @param (string) $itemType - тип объекта, для которого загружается файл
     */
    static public function deleteAll($nItemId, $itemType) {
        $filesPath = self::getList($nItemId, $itemType);
            if (!$filesPath)
                return $filesPath;
            foreach ($filesPath as $fpath)
                unlink($fpath);
            rmdir(DR.'data/'.$itemType.'_files/'.$nItemId);
            
            $db = Registry::get('db');
            $db->delete(self::TABLE, "item_id = ".$nItemId." AND item_type = '".$itemType."'");
    }
    
    
    /**
     * Получаем прикрепленные файлы объекта, для которого загружается файл
     * @param (intger) $nItemId - идетификатор объекта, для которого загружается файл
     * @param (string) $itemType - тип объекта, для которого загружается файл
     */
    static public function get($nItemId, $itemType) {
        $db = Registry::get('db');
        $strSql = "SELECT * FROM `".self::TABLE."` WHERE ".db::p('item_id = ?i', $nItemId)." AND `item_type` = '".htmlspecialchars($itemType, ENT_QUOTES)."'";
        $res = $db->_getAll($strSql);

        if (!empty($res)) {
            $result = array();
            foreach ($res as $file)
                $result[] = array('id' => $file['id'], 'filename' => $file['filename'], 'name' => $file['name'], 'icon' => $file['icon']);

            return $result;
        }

        return FALSE;
    }
    
    
    /**
     * Cмена параметров в БД для загруженных файлов
     * @param array $aParams - массив, содержащие перечень массивов с обновляемыми параметрами (1 вложенные массив = масиву парамтров для 1 файла)
     */
    
    static public function set($aParams) {
        if (empty($aParams))
            return FALSE;

        $successStart = TRUE;
        $db = Registry::get('db');
        foreach ($aParams as $nId => $param) {
            $param['name'] = db::clear($param['name']);
            $success = $db->update(self::TABLE, $param, db::p('id = ?i', $nId));
            if ($successStart && !$success)
                $successStart = FALSE;
        }

        return $successStart;
    }
    
    /**
     * Загрузка файлов
     * @param (array) $aFiles - массив загружаемых файлов
     * @param (integer) $nItemId - идетификатор объекта, для которого загружается файл
     * @param (string) $itemType - тип объекта, для которого загружается файл
     */
    static public function upload($aFiles, $nItemId, $itemType) {
        //dwdwdw
        if (empty($aFiles['additFiles']['name']))
            return FALSE;
        set_time_limit(1200);

        //разрешенные типы файлов
        $mimeTypes = array(
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', //файлы .xlsx
            'application/vnd.ms-excel', //файлы .xls
            'text/csv', //файлы .csv
            'application/pdf', //файлы.pdf,
            'text/plain', //обычные текстовые файлы
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document', //файлы .docx
            'application/msword', //файлы .doc
            'application/zip'

        );
        //разрешенные расширения файлов
        $aExts = array ('xlsx', 'xls', 'csv', 'pdf', 'txt', 'docx', 'doc');

        $db = Registry::get('db');
        $pathUploadDir = DR.'data/'.$itemType.'_files/'.$nItemId;
        
        foreach ($aFiles['additFiles']['name'] as $kfile => $file) {
            //имя загружаемого файла
            $fName = H::translite($file);
            $fName = str_replace(' ', '-', $fName);
            $fName = preg_replace('/[^-a-zа-я_0-9\.]+/i', '', $fName);
            if (empty($fName))
                $fName = 'file'.$kfile;
            //расширение загружаемого файла
            $fExt = pathinfo($file, PATHINFO_EXTENSION);

            $fid = finfo_open(FILEINFO_MIME_TYPE);
            $aFiles['additFiles']['type'][$kfile] = @finfo_file($fid, $aFiles['additFiles']['tmp_name'][$kfile]);
            finfo_close($fid);

            if (
                    array_search($aFiles['additFiles']['type'][$kfile], $mimeTypes) !== FALSE
                    && array_search($fExt, $aExts) !== FALSE
                ) 
                {
                    if (!is_dir($pathUploadDir))
                        mkdir($pathUploadDir, 0777);

                    if (move_uploaded_file($aFiles['additFiles']['tmp_name'][$kfile], $pathUploadDir.'/'.$fName)) {
                        //заносим имя файла в таблицу если его не существует в БД файл с именем $fName
                        $isCopyFile = $db->getAll(self::TABLE, "`filename` = '".$fName."' AND item_id = ".$nItemId." AND `item_type` = '".$itemType."'");
                        $isCopyFile = (!empty($isCopyFile[0]['filename']))? TRUE : FALSE;
                        if (array_search($fExt, array($aExts[0], $aExts[1], $aExts[2])) !== FALSE && array_search($aFiles['additFiles']['type'][$kfile], array($mimeTypes[0], $mimeTypes[1], $mimeTypes[2], $mimeTypes[4], $mimeTypes[7])) !== FALSE)
                            $iconType = 'xls';
                        elseif ($fExt == $aExts[3] && $aFiles['additFiles']['type'][$kfile] == $mimeTypes[3])
                            $iconType = 'pdf';
                        elseif ($fExt == $aExts[4] && $aFiles['additFiles']['type'][$kfile] == $mimeTypes[4])
                            $iconType = 'txt';
                        else
                            $iconType = 'doc';
                        if (!$isCopyFile)
                            $db->add(self::TABLE, array('item_id' => $nItemId, 'item_type' => $itemType, 'filename' => $fName, 'icon' => $iconType));
                    }
                }
        }
    }
}