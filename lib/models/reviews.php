<?php

class reviews {
	const TABLE = 'reviews';
	
	static public function getAll($bAll=true) {
		$db = Registry::get('db');
		
		$strWhere = ($bAll) ? '1' : 'public = 1';
		
		return $db->getAll(self::TABLE, $strWhere.' ORDER BY date DESC');
	}
	
	
	static public function set($aParams, $nId=0) {		
		$db = Registry::get('db');
		
		$aParams['public'] = (isset($aParams['public']) && $aParams['public']=='on') ? 1 : 0;

		if ($nId > 0) {
			return $db->update(self::TABLE, $aParams, db::p('id = ?i', $nId));
		} else {
			$aParams['image'] = self::loadImage();
			return $db->add(self::TABLE, $aParams);
		}
		
		return false;
	}	
	
	
	static public function get($nId) {
		$db = Registry::get('db');
		return $db->getRow(self::TABLE, db::p('id = ?i', $nId));
	}
	
	static public function del($nId) {
		$db = Registry::get('db');
		$aR = self::get($nId);
		
		if ($aR['image'] && is_file(DR.'public/img/reviews/'.$aR['image'])) {
			unlink(DR.'public/img/reviews/'.$aR['image']);
		}

		return $db->delete(self::TABLE, db::p('id = ?i', $nId));
	}
	
	
	static public function loadImage() {
		$aValidExts = array('png', 'jpg', 'jpeg', 'gif');
		
		if (isset($_FILES['image'])) {
			
			$strExt = substr(strrchr($_FILES['image']['name'], '.'), 1);
			
			if (in_array($strExt, $aValidExts)) {
				include (DR.'lib/wideimage/WideImage.php');
	
				$image = WideImage::loadFromUpload("image");
				$image = $image->resize(130, 129);
		
				$strFileName = md5(time()).'.'.$strExt;
				$a = $image->saveToFile(DR.'public/img/reviews/'.$strFileName);

				return $strFileName;
			}
		} 
		
		return false;
	}
}