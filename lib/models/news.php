<?php

class news {
	const TABLE = 'news';
	
	public static $upload_folder = 'public/img/news/';
	
	static public function getAll($nLimit=0) {
		$db = Registry::get('db');
		return $db->getAll(self::TABLE, ($nLimit>0) ? db::p('1 ORDER BY date DESC LIMIT ?i', $nLimit) : '');
	}
	
	
	static public function set($aParams, $nId=0) {		
		if (isset($aParams['title'])) {
			$db = Registry::get('db');
			
			if (isset($_FILES['image']) && $_FILES['image']['name']!='') {
				include DR.'lib/wideimage/WideImage.php';
		
				$strImageName = $_FILES['image']['name'];

				WideImage::load($_FILES['image']['tmp_name'])->resize(195)->saveToFile(DR.self::$upload_folder.$strImageName);
				$aParams['image'] = $strImageName;
			}
                        if (isset($aParams['date'])){
                                $aDate = explode('-', $aParams['date']);
                                $nDate = mktime(0,0,0,$aDate[1],$aDate[0],$aDate[2]);
                        }else{
                                $nDate = time();
                        }
			if ($nId > 0) {
				$aParams['date'] = $nDate;
                                return $db->update(self::TABLE, $aParams, db::p('id = ?i', $nId));
			} else {
				$aParams['date'] = $nDate;
				return $db->add(self::TABLE, $aParams);
			}
		}
		
		return false;
	}	
	
	
	static public function get($nId) {
		$db = Registry::get('db');
		return $db->getRow(self::TABLE, db::p('id = ?i', $nId));
	}
	
	static public function del($nId) {
		$aNews = self::get($nId);
		
		if (is_file(DR.self::$upload_folder.$aNews['image'])) {
			unlink(DR.self::$upload_folder.$aNews['image']);
		}

		$db = Registry::get('db');
		return $db->delete(self::TABLE, db::p('id = ?i', $nId));
	}
}