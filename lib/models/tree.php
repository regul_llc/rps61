<?php
/** 
 * Класс для работы с деревьями
 * 
 * @author Dmitry Ivlev
 * @version 0.2  
 * 
 * Начало работы: Mon Jul 09 18:16:46 GMT 2012
 * Окончание работы: в работе...
 */
class Tree {
	
	/** Имя таблицы **/
	protected $table = '';
	
	/** Объект базы данных **/
	protected $db = null;
	
	/** Объект шаблонизатора **/
	protected $tpl = null;
	
	/** Сколько уровней показывать **/
	protected $levels = 10;
	
	/** Поле сортировки **/
	protected $orderby = 'order';
	
	/** Порядок осртировки **/
	protected $order = 'ASC';
	
	/** Количество уровней **/
	protected $count_levels = 0;
	
	/** Максимальное количество уровней **/
	protected $count_levels_max = 50;
	
	public $type_names = array(
		'element' => 'Элемент',
		'folder' => 'Папка',
		'none' => 'Не выбран'
	);
	
	
	/** Конструктор
	 * @param object:DB $db - Объект базы данных
	 * @param string $code - код */
	public function __construct($table = 'structure') {
		$this->db = Registry::get('db');
		$this->table = $table;
		$this->count_levels = $this->getCountLevels();
	}
	
	
	/** Возвращает всех потомков кусок дерева в виде одномерного массива 
	 *
	 * @param int $from - Чьих потомков выбирать
	 * @param int $levels - Сколько уровней выбирать
	 * @param string $orderby - По какому полю сортировать
	 * @param 'ASC'|'DESC' $order - В каком порядке сортировать
	 * @return array - Одномерный массив */
	public function get($from, $levels, $orderby='id', $order='ASC', $filter='') {
		$this->levels = $levels;
		$this->orderby = $orderby;
		$this->order = $order;
		$this->filter = $filter;
		$result = array();
		$this->getRecurse($from, 1, $result);
		return $result;
	}
	
	/** Возвращает всех потомков кусок дерева в виде многомерного массива 
	 *
	 * @param int $from - Чьих потомков выбирать
	 * @param int $levels - Сколько уровней выбирать
	 * @param string $orderby - По какому полю сортировать
	 * @param 'ASC'|'DESC' $order - В каком порядке сортировать
	 * @return array - Одномерный массив */
	public function get2($from, $levels, $orderby='id', $order='ASC', $filter='') {
		$this->levels = $levels;
		$this->orderby = $orderby;
		$this->order = $order;
		$this->filter = $filter;
		$result = $this->getRecurse2($from, 1);
		return $result;
	}
	
	
	/** Возвращает количество уровней, 
	 * которое может храниться в в базе без модификации
	 *
	 * @return int */
	public function getCountLevels() {
		$fields = $this->db->getRealFieldsName($this->table);
		$count = 0;
		foreach ($fields as $field) {
			if (strstr($field, 'level_')) $count++;
		}
		return $count;
	}
	
	
	/** Возвращает максимально возможное количество уровней в дереве
	 *
	 * @return int */
	public function getCountLevelsMax() {
		return $this->count_levels_max;
	}
	
	
	/**
	 * Задает максимально возможное количество уровней в дереве
	 *
	 * @param int $count - Кол-во уровней
	 * @return boolean */
	public function setCountLevelsMax($count) {
		if ((int)$count>0) {
			$this->count_levels_max = (int)$count;
			return true;
		} else return false;
	}
	
	
	/** Вспомогательная функция для вывода деревьев
	 *
	 * @param int $parent_id - ID родительского элемента
	 * @param int $level - Текущий уровень
	 * @param array $result - Массив, куда собирается дерево
	 * @return array - Собранный массив */
	public function getRecurse($parent_id, $level, &$result) {
		if ($level<=$this->levels) {
			$filter = '';
			if ($this->filter) $filter = ' AND '.$this->filter;
			$elements = $this->db->getAll($this->table, '`parent_id` = '.$parent_id.$filter.' ORDER BY `'.$this->orderby.'` '.$this->order);

			foreach ($elements as $element) {
				$element['level'] = $level;
				$result[] = $element;
				$this->getRecurse($element['id'], $level+1, $result);
			}
		} else return false;
	}
	
	
	/** Вспомогательная функция для вывода деревьев в виде многомерного массива
	 *
	 * @param int $parent_id - ID родительского элемента
	 * @param int $level - Текущий уровень
	 * @param array $result - Массив, куда собирается дерево
	 * @return array - Собранный массив */
	public function getRecurse2($parent_id, $level) {
		if ($level<=$this->levels) {
			$filter = '';
			if ($this->filter) $filter = ' AND '.$this->filter;
			$elements = $this->db->getAll($this->table, '`parent_id` = '.$parent_id.$filter.' ORDER BY `'.$this->orderby.'` '.$this->order);

			$result = array();
			foreach ($elements as $element) {
				$element['level'] = $level;
				$element['childs'] = $this->getRecurse2($element['id'], $level+1);
				$result[] = $element;
			}
			
			return $result;
		};
	}
	
	
	/** Заворачивает дерево в шаблон для вывода
	 *
	 * @param array $tpls - Массив шаблонов для всех уровней, включая нулевой
	 * @return html */
	public function show($elements, $tpls, $active_id=0) {
		$result = '';
		$adress = Registry::get('adress');		

		foreach ($elements as $element) {
			if ($active_id==$element['id']) {
				$tpl = 'active';
				if ($element['type']=='folder') $element['type'] = 'folder_open';
			} else {
				$tpl = 'no_active';
			}
			$element['about'] = $element['id'].' - '.$element['name'];
			$element['padding_left'] = $element['level'] * 15;
			$element['page'] =  Registry::get('page');
			$element['path'] = $this->tpl->getLocalPath();
			$result .= $this->tpl->fill($tpls[$tpl], $element);
		}
		
		$current_element = $this->getElementInfo($active_id);

		return $this->tpl->fill($tpls['index'], array(
			'tree' => $result,
			'page' => $adress[2],
			'id' => ($current_element['type']=='folder') ? $active_id : $current_element['parent_id']
		));
	}
	
	

	public function showBreadCrumbs($element) {
		$level = $this->getLevel($element);
		$result = '';
//		$adress = Registry::get('adress');
		for ($i=$level; $i>=1; $i--) {
			$parent_element = $this->getElementInfo($element['level_'.$i]);
//			$parent_element['page'] = $adress['2'];
			$result[] = $parent_element['title'];
		}
		$result = array_reverse($result);
		
		return join(' :: ', $result);
	}
	
	
	/** Возвращает информацию об элементе
	 *
	 * @param int $id - ID элемента
	 * @return array - Массив свойств элемента */
	public function getElementInfo($id, $bAll = TRUE) {
                $where = (!$bAll)? ' AND `visible` = 1' : '';
		return $this->db->get($this->table, '`id`='.((int)$id).$where);
	}
	
	/** Возвращает информацию об элементе
	 *
	 * @param int $id - ID элемента
	 * @return array - Массив свойств элемента */
	public function getElementInfoByCode($code) {
		return $this->db->get($this->table, '`code`='.(mysql_real_escape_string($code)));
	}
	
	
	/** Возвращает уровень элемента
	 * Использует массив его свойств
	 * Без запроса к базе
	 *
	 * @param array $element - Массив свойств элемента
	 * @return int - Уровень */
	public function getLevel($element) {
		for ($i=1; $i<=$this->count_levels_max; $i++) {
			if ($element['id'] == $element['level_'.$i]) return $i;
		}
		return false;
	}
	
	
	/** Добавляет элемент дерева
	 * Если указан parent_id то добавит в него
	 *
	 * @param array $params - Массив свойств */
	public function add($params){
		if (isset($params['parent_id']) && $params['parent_id']!=0) {
			$parent = $this->getElementInfo($params['parent_id']);
			$level = $this->getLevel($parent) + 1;
			for ($i=1; $i<$level; $i++) {
				$params['level_'.$i] = $parent['level_'.$i];
			}
		} else {
			$level = 1;
		}
		
		if ($level > $this->getCountLevels()) {
			/** @todo Реализовать добавление уровня (поле в таблицу) если полей уровней в таблице не хватает */
		}

		$id = $this->db->add($this->table, $params);
		$this->db->update($this->table, array('level_'.$level => $id), '`id` = '.$id);
		return $id;
	}
	
	
	
	public function save($params) {
		$id = $params['id'];
		unset($params['id']);
		$element = $this->getElementInfo($id);

		if (count($element)>0) {
			/*$parent = $this->getElementInfo($params['parent_id']);
			if ($id==$params['parent_id'] || !$parent) unset($params['parent_id']);*/

			$res = $this->db->update($this->table, $params, '`id` = '.(int)$id);			
			/*$this->delLevels();
			$this->updateLevels();*/
			return $res;
		}
	}
	

	public function updateLevels() {
		$elements = $this->db->getAll($this->table);

		foreach ($elements as $element) {
			$parents = array();
			$parents[] = $element['id'];
			if ($element['parent_id']) {
				$parents[] = $element['parent_id'];
				$this->getParents($element['parent_id'], $parents);
			}
			$parents = array_reverse($parents);
			
			
			
			$params = array();
			foreach ($parents as  $num=>$val) {
				$params['level_'.($num+1)] = $val;
			}

			$this->db->update($this->table, $params, '`id` = '.$element['id']);
		}
	}
	
	public function getParents($id, &$parents) {
		$parent = $this->db->get($this->table, "`id` = ".(int)$id);

		if ($parent['parent_id']) {
			$parents[] = $parent['parent_id'];
			$this->getParents($parent['parent_id'], $parents);
		}
	}
	
	public function delLevels() {
		$elements = $this->db->getAll($this->table);
		$count_levels = $this->getCountLevels();

		$params = array();
		for ($i=1; $i<=$count_levels; $i++) {
			$params['level_'.$i] = 0;
		}

		$this->db->update($this->table, $params);
	}

	
	public function delete($id) {
		$element = $this->getElementInfo($id);
		$level = $this->getLevel($element);
		return $this->db->delete($this->table, "`level_".$level."` = ".$id);
	}
	
	
	public function getTypes() {
		$fields = $this->db->_getAll('SHOW COLUMNS FROM `'.$this->db->getPrefix().$this->table.'` ');
		foreach ($fields as $val) {
			if ($val['Field']=='type') { 
				$val['Type'] = str_replace("'", '', $val['Type']);
				$types = substr($val['Type'], 5, (strlen($val['Type'])-6));			
				return explode(',', $types);
			}
		}		
	}
}

/*$elements = $db->getAll('categories');

foreach ($elements as $element) {
	if ($element['id']==$element['level_1']) $parent_id = 0;
	if ($element['id']==$element['level_2']) $parent_id = $element['level_1'];
	if ($element['id']==$element['level_3']) $parent_id = $element['level_2'];
	if ($element['id']==$element['level_4']) $parent_id = $element['level_3'];
	
	$db->update('categories', array('parent_id' => $parent_id), '`id` = '.$element['id']);
}*/

?>