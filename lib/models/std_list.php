<?php

class std_list {
	public $strTable = '';


	public static function getAll($nLimit=0) {
		$db = Registry::get('db');
		
		$strLimit = ($nLimit>0) ? db::p('LIMIT ?i', $nLimit) : '';
		
		$strSql = db::p("
			SELECT * FROM ?n ?s
		", self::$strTable, $strLimit);
        
		return $db->_getAll($strSql);
	}
	
	
	public static function set($aParams, $nId=0) {
        $db = Registry::get('db');

        if (isset($aParams['order'])) $aParams['order'] = intval($aParams['order']);
        if (isset($aParams['date'])) $aParams['date'] = strtotime($aParams['date']);

        if ($nId > 0) {
            return $db->update(self::$strTable, $aParams, db::p('id = ?i', $nId));
        } else {
            $aParams['date'] = time();
            return $db->add(self::$strTable, $aParams);
        }
		
		return false;
	}
	
	
	public static function get($nId) {
		$db = Registry::get('db');
		return $db->getRow(self::$strTable, db::p('id = ?i', $nId));
	}
	
	public static function getBy($strField, $strValue) {
		$db = Registry::get('db');
		return $db->getRow(self::$strTable, db::p("?n LIKE '%?s%'", $strField, $strValue));
	}
	
	public static function del($nId) {
		$db = Registry::get('db');
		return $db->delete(self::$strTable, db::p('id = ?i', $nId));
	}
    
    public static function checkTable() {
        die;

        $db = Registry::get('db');

        if (!$db->isTable(self::$strTable)) {
            $strSql = db::p("
                CREATE TABLE IF NOT EXISTS ?n (
                    `id` int(11) NOT NULL AUTO_INCREMENT,
                    `title` varchar(255) NOT NULL,
                    `text` text NOT NULL,
                    PRIMARY KEY (`id`)
                ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
            ", self::$strTable);
            
            mysql_query($strSql);
        }
    }
}