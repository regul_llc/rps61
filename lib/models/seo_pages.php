<?php

class seo_pages {

    public static $strTable = 'seo_pages';


    /** Возвращает список общих свойств для входного списка категорий
     * @param array $aCats - массив категорий
     * @return array
     */
    public static function getSameFields($aCats) {

        $aCatsFields = array();
        foreach($aCats as $aCat) {
            $aCatsFields[] = goods::getValuesTpl($aCat);
        }

        if (count($aCatsFields)>0) {
            $aSameFields = $aCatsFields[0];
            unset($aCatsFields[0]);

            // по умолчанию берем список полей первой категории
            // проходим по каждому полю первой категории
            foreach($aSameFields as $nSF=>$aSameField) {
                $bIsSame = true;

                // цикл по остальным категориям
                foreach($aCatsFields as $aCatFields) {
                    $bIsFined = false;

                    // цикл по полям категории
                    foreach($aCatFields as $aCatField) {
                        if (
                            (trim($aSameField['name']) == trim($aCatField['name']))
//                            && (trim($aSameField['type']) == trim($aCatField['type']))
//                            && (trim($aSameField['units'])  == trim($aCatField['units']))
                        ) {
                            $bIsFined = true;
                        }
                    }

                    if (!$bIsFined) {
                        $bIsSame = false;
                    }
                }

                if (!$bIsSame) {
                    unset($aSameFields[$nSF]);
                }
            }

            $aSameFields = array_values($aSameFields);

            return (count($aSameFields)>0) ? $aSameFields : array();
        }
        else return array();
    }

    public static function getAll($nLimit=0) {
        $db = Registry::get('db');

        $strLimit = ($nLimit>0) ? db::p('LIMIT ?i', $nLimit) : '';

        $strSql = db::p("
			SELECT * FROM ?n ?s
		", self::$strTable, $strLimit);

        return $db->_getAll($strSql);
    }


    public static function set($aParams, $nId=0) {


        $db = Registry::get('db');

        if (!isset($aParams['url']) || $aParams['url']=='') {
            $aParams['url'] = H::translite($aParams['title']);
            $aParams['url'] = strtolower($aParams['url']);
            $aParams['url'] = str_replace(' ', '-', $aParams['url']);
            $aParams['url'] = str_replace(array('«', '»', '"', "'", '$'), '', $aParams['url']);
//            $aParams['url'] = preg_replace('/[^-a-zа-я_0-9\.]+/i', '', $aParams['url']);
        }

        $aParams['filter'] = self::s($aParams['filter']);

        if ($nId > 0) {
            return $db->update(self::$strTable, $aParams, db::p('id = ?i', $nId));
        } else {
            return $db->add(self::$strTable, $aParams);
        }

        return false;
    }


    public static function getBy($strField, $strValue) {
        $db = Registry::get('db');
        return $db->getRow(self::$strTable, db::p("?n LIKE '%?s%'", $strField, $strValue));
    }

    public static function del($nId) {
        $db = Registry::get('db');
        return $db->delete(self::$strTable, db::p('id = ?i', $nId));
    }

    public static function checkTable() {
        die;

        $db = Registry::get('db');

        if (!$db->isTable(self::$strTable)) {
            $strSql = db::p("
                CREATE TABLE IF NOT EXISTS ?n (
                    `id` int(11) NOT NULL AUTO_INCREMENT,
                    `title` varchar(255) NOT NULL,
                    `text` text NOT NULL,
                    PRIMARY KEY (`id`)
                ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
            ", self::$strTable);

            mysql_query($strSql);
        }
    }


    public static function get($nId) {
        $db = Registry::get('db');
        $aParams = $db->getRow(self::$strTable, db::p('id = ?i', $nId));
        $aParams['filter'] = self::us($aParams['filter']);
        $aParams['filters'] = array();

        if (isset($aParams['filter']['filter'])) {
            foreach($aParams['filter']['filter'] as $strName=>$aItem) {
                $aParams['filters'][$strName] = goods::getFilterByName($strName);
            }
        }

        return $aParams;
    }


    public static function us($mData) {
        return unserialize(stripslashes($mData));
    }

    public static function s($mData) {
        return mysql_real_escape_string(serialize($mData));
    }

    public static function search($strCode) {
        $db = Registry::get('db');
        $aResult = $db->getRow(self::$strTable, db::p("url = '?s'", $strCode));
        if($aResult) {
            $aResult['filter'] = self::us($aResult['filter']);
        }

        return $aResult;
    }

}