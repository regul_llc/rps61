<?php

class articles {
	const TABLE = 'articles';
	
	static public function getAll() {
		$db = Registry::get('db');
		return $db->getAll(self::TABLE);
	}
	
	
	static public function set($aParams, $nId=0) {		
            if (isset($aParams['title'])) {
                $db = Registry::get('db');
				
				$aParams['meta'] = json_encode($aParams['meta']);

                if ($nId > 0) {
                    files::upload($_FILES, $nId, 'article');
                    return $db->update(self::TABLE, $aParams, db::p('id = ?i', $nId));
                } else {
                    $artId = $db->add(self::TABLE, $aParams);
                    if ($artId)
                        files::upload($_FILES, $artId, 'article');

                    return $artId;
                }
            }

            return false;
	}	
	
	
	static public function get($nId) {
		$db = Registry::get('db');
                
		$aArticle = $db->getRow(self::TABLE, db::p('id = ?i', $nId));
		if (!empty($aArticle))
			$aArticle['meta'] = json_decode($aArticle['meta'], TRUE);
		$aArticle['additFiles'] = files::get($nId, 'article');

		return $aArticle;
	}
	
	static public function del($nId) {
		$db = Registry::get('db');
                
                files::deleteAll($nId, 'article');
                
		return $db->delete(self::TABLE, db::p('id = ?i', $nId));
	}
}