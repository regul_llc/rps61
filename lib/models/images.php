<?php

class images {
	const TABLE = 'images';
	
	static public function getAll($nItemId, $strType='goods') {
		$db = Registry::get('db');
		return $db->getAll(self::TABLE, db::p('item_id = ?i AND item_type = "?s" ORDER BY priority DESC', $nItemId, $strType));
	}
	
	
	static public function set($aParams, $nId=0) {		
		$db = Registry::get('db');

		if ($nId > 0) {
			return $db->update(self::TABLE, $aParams, db::p('id = ?i', $nId));
		} else {
			return $db->add(self::TABLE, $aParams);
		}
		
		return false;
	}	
	
	
	static public function get($nId) {
		$db = Registry::get('db');
		return $db->getRow(self::TABLE, db::p('id = ?i', $nId));
	}
	
	
	static public function getByFileName($strFileName) {
		$db = Registry::get('db');
		return $db->getAll(self::TABLE, db::p('file_name LIKE "?s"', $strFileName));
	}
	
	static public function del($nId) {
		$db = Registry::get('db');
		$aImage = self::get($nId);

		$db->update(goods::TABLE, array(
			'cover' => ''
		), db::p('cover LIKE "?s"', $aImage['file_name']));
		
		return $db->delete(self::TABLE, db::p('id = ?i', $nId));
	}
	
	static private function getFolders($mainFolder) {
            $folders = glob($mainFolder.'*', GLOB_ONLYDIR);
            return (!empty($folders))? $folders : FALSE;
        }
        
        static public function delAll ($mainFolder, $nItemId, $imgType) {
            $folders = self::getFolders($mainFolder);
            if (!$folders)
                return $folders;
            
            $db = Registry::get('db');
            $images = self::getAll($nItemId, $imgType);
            if (empty($images))
                return FALSE;
            
            foreach ($images as $img) {
                $resDel = $db->delete(self::TABLE, db::p('id = ?i', $img['id']));
                if (!$resDel) {
                    return FALSE;
                }
                foreach ($folders as $folder) {
                    if (is_file($folder.'/'.$img['file_name']))
                        unlink($folder.'/'.$img['file_name']);
                }
                unlink($mainFolder.$img['file_name']);
            }
            
            return TRUE;
        }
        
	static public function setTitle($strTitle, $nId) {
		$db = Registry::get('db');
		
		if ($nId > 0) {
			return $db->update(self::TABLE, array(
				'title' => $strTitle
			), db::p('id = ?i', $nId));
		}
		
		return false;
	}
	
	static public function setPriority($nPriority, $nId) {
		$db = Registry::get('db');
		
		if ($nId > 0) {
			return $db->update(self::TABLE, array(
				'priority' => $nPriority,
			), db::p('id = ?i', $nId));
		}
		
		return FALSE;
	}
	
	static public function getImagesForSlider() {
		$aImages = images::getAll(1, 'main');
		
		$aTransitions = array('fade', 'slide');
		$aAligns = array('right,bottom', 'right,top', 'left,bottom', 'right,top');
		$aZooms = array('in', 'out');
		
		
		foreach ($aImages as $n => $aImage) {
			$aImages[$n]['transition'] = $aTransitions[array_rand($aTransitions)];
			$aImages[$n]['startalign'] = $aAligns[array_rand($aAligns)];
			$aImages[$n]['zoom'] = $aZooms[array_rand($aZooms)];
			$aImages[$n]['zoomfact'] = rand(10, 30) / 10;
			$aImages[$n]['endalign'] = $aAligns[array_rand($aAligns)];
			$aImages[$n]['panduration'] = rand(8, 12);
			$aImages[$n]['colortransition'] = 4;
		}
		
		return $aImages;
	}
	
	
	public static function getGalleryMain() {
		$db = Registry::get('db');
		$strSql = db::p("
			SELECT 
				DISTINCT(i.id),
				i.* 
			FROM 
				images as i, 
				goods as w 
			WHERE 
				i.file_name = w.cover AND
				w.public = 1
			GROUP BY
				i.id
		");
		return $db->_getAll($strSql);
	}
}