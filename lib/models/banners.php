<?php

class banners {
	const TABLE = 'banners';
	
	public static $upload_folder = 'public/img/banners/';
	
	static public function getAll($nLimit=0) {
		$db = Registry::get('db');
		return $db->getAll(self::TABLE, ($nLimit>0) ? db::p('1 LIMIT ?i', $nLimit) : '');
	}
	
	
	static public function set($aParams, $nId=0) {		
		
		if (isset($aParams['title'])) {
			$db = Registry::get('db');

			if (isset($_FILES['image']) && $_FILES['image']['name']!='') {
				include DR.'lib/wideimage/WideImage.php';
		
				$strImageName = $_FILES['image']['name'];

				WideImage::load($_FILES['image']['tmp_name'])->saveToFile(DR.self::$upload_folder.$strImageName);
				$aParams['image'] = $strImageName;
			}

			if ($nId > 0) {
				return $db->update(self::TABLE, $aParams, db::p('id = ?i', $nId));
			} else {
				$aParams['date'] = time();
				return $db->add(self::TABLE, $aParams);
			}
		}
		
		return false;
	}
	
	
	static public function get($nId) {
		$db = Registry::get('db');
		return $db->getRow(self::TABLE, db::p('id = ?i', $nId));
	}
	
	static public function del($nId) {
		$aBanner = self::get($nId);
		
		if (is_file(DR.self::$upload_folder.$aBanner['image'])) {
			unlink(DR.self::$upload_folder.$aBanner['image']);
		}
		
		$db = Registry::get('db');
		return $db->delete(self::TABLE, db::p('id = ?i', $nId));
	}
}