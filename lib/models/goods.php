<?php

define('VTYPE_STRING', 1);
define('VTYPE_TEXT', 2);
define('VTYPE_TOGGLE', 3);
define('VTYPE_NUMBER', 4);
define('VTYPE_PHONE', 5);
define('VTYPE_PRICE', 6);
define('VTYPE_BRAND', 7);
define('VTYPE_LIST', 8);
define('VTYPE_PRICE_USD', 9);
define('VTYPE_PRICE_EURO', 10);
define('GOODFILE_DIR', DR.'data/good_files');

class goods {
	const TABLE = 'goods';
	const TABLE_CATEGORIES = 'goods_categories';
	const TABLE_VALUES_TPL = 'values_tpl';
	const TABLE_VALUES = 'values';	
        const TABLE_FILES = 'good_files';
        const TABLE_ICONS = 'icon_options';
        const TABLE_STRUCTURE = 'structure';
	
	const GOODS_PER_PAGE = 12;
	
	public static $field_types = array(
		VTYPE_STRING => 'Строка',
		VTYPE_TEXT => 'Текст (WYSIWYG-редактор)',
		VTYPE_TOGGLE => 'Переключатель ДА/НЕТ',
		VTYPE_NUMBER => 'Число',
		VTYPE_PHONE => 'Номер телефона',
		VTYPE_PRICE => 'Цена',
		VTYPE_BRAND => 'Бренд',
		VTYPE_LIST => 'Выпадающий список',
                VTYPE_PRICE_USD => 'Цена в долларах',
                VTYPE_PRICE_EURO => 'Цена в евро'
	);
	
	
	static public function getValueTypesForFilter($aFilters, $bFilter = FALSE) {
		$aTypeIds = array();
        $isEmpty = FALSE;

		foreach ($aFilters as $nVTypeId=>$mFilter) {
            if (!$bFilter)
                $aTypeIds[] = $nVTypeId;
            else {
                if (!is_array($mFilter))
                    $isEmpty = empty($mFilter);
                else {
                    foreach ($mFilter as $kfilter => $filter) {
                        if (!empty($mFilter[$kfilter]))
                            $isEmpty = FALSE;
                    }
                }

                if (!$isEmpty)
                    $aTypeIds[] = $nVTypeId;
            }
		}

		$aResult = array();
		if (count($aTypeIds)>0) {		
			$db = Registry::get('db');
			$aTypes = $db->getAll(self::TABLE_VALUES_TPL, db::p('id in (?s)', join(',', $aTypeIds)));
			
			foreach ($aTypes as $aType) {
				$aResult[$aType['id']] = $aType['type'];
			}
		}
		
		return $aResult;
	}
	
	
	public static function getFiltersArray($nCat) {

		if (isset($_POST['filter'])) { 
			$_GET['page'] = 1;
			$_SESSION['filter'][$nCat] = $_POST['filter'];
		}
		
		return (isset($_SESSION['filter'][$nCat])) ? $_SESSION['filter'][$nCat] : array();		
	}
	
	
	static public function getAll($nCatId, $bAll=true, $bFilter=false, $strOrder = '') {
		$db = Registry::get('db');
		
		$strWhereGoods = ($bAll) ? '' : ' AND public = 1';		
		$aGoods = $db->getAll(self::TABLE, db::p("cat_id = ?i ?s ?s", $nCatId, $strWhereGoods, $strOrder));
		
		$aGoodsId = array();
		foreach ($aGoods as $aGood) $aGoodsId[] = $aGood['id'];		
		

		$aFilters = self::getFiltersArray($nCatId);
                $aVTypes = self::getValueTypesForFilter($aFilters, TRUE);

                if (!empty($aVTypes)) {
                    $i=1;
                    foreach ($aFilters as $nVTypeId=>$mFilter) {
                            $strWhere = db::p(' AND good_id in (?s)', join(',', $aGoodsId));
                            if (!isset($aVTypes[$nVTypeId]))
                                continue;

                            switch ($aVTypes[$nVTypeId]) {
                                    case 4: case 6: // диапазон
                                            $aRange = array();
                                            if (isset($mFilter['from']) && intval($mFilter['from'])>0) {
                                                    $aRange[] = db::p("value >= ?i", intval($mFilter['from']));
                                            }
                                            if (isset($mFilter['to']) && intval($mFilter['to'])>0) {
                                                    $aRange[] = db::p("value <= ?i", intval($mFilter['to']));
                                            }

                                            if (count($aRange)>0) $strWhere .= ' AND ('.join(' AND ', $aRange).')';
                                    break;

                                    case 7: case 8:
                                            if ($mFilter!='') {
                                                    $strWhere .= db::p(" AND value LIKE '%?s%'", $mFilter);
                                            }
                                    break;
                            }

                            $strSql = db::p("
                                    SELECT 
                                            * 
                                    FROM 
                                            ?n
                                    WHERE
                                            value_tpl_id = ?i
                                            ".$strWhere."
                            ", self::TABLE_VALUES, $nVTypeId);

                            $aResult = $db->_getAll($strSql);

                            if (count($aResult)==0) return array(
                                    'list' => array(),
                                    'count' => 0
                            );

                            $aGoodsId = array();
                            foreach ($aResult as $aItem) $aGoodsId[] = $aItem['good_id'];

                            $i++;
                    }
                }
		
		if (count($aGoodsId)==0) return array(
			'list' => array(),
			'count' => 0
		);

        $nPage = isset($_GET['page']) ? $_GET['page'] : 1;

		$nLimitFrom = ($nPage - 1) * self::GOODS_PER_PAGE;
		$nLimit = self::GOODS_PER_PAGE;
		
		$strSql = db::p("
			SELECT 
				* 
			FROM
				?n
			WHERE
				id in (?s)
			ORDER BY priority DESC
			LIMIT ?i, ?i
		", self::TABLE, join(',', $aGoodsId), $nLimitFrom, $nLimit);

		$aGoods = $db->_getAll($strSql);
		
		foreach ($aGoods as &$aGood) {
			$aGood['price'] = self::getGoodValue($aGood['id'], VTYPE_PRICE);
			$aGood['brand'] = self::getGoodValue($aGood['id'], VTYPE_BRAND);
		}
		
		return array(
			'list' => $aGoods,
			'count' => count($aGoodsId)
		);
	}


	static public function getAllForSeo($aFilters) {
		$db = Registry::get('db');

        if (empty($aFilters['cat']) || count($aFilters['cat'])==0) return array();
        if (empty($aFilters['fields']) || count($aFilters['fields'])==0) return array();

		$aGoods = $db->getAll(self::TABLE, db::p("cat_id in (?a) AND public = 1", $aFilters['cat']));

		$aGoodsId = array();
		foreach ($aGoods as $aGood) $aGoodsId[] = $aGood['id'];

        $_aVTypes = array();
        foreach ($aFilters['fields'] as $strKeyName) {
            $_aVTypes[] = $db->getRow(self::TABLE_VALUES_TPL, db::p("name = '?s'", $strKeyName));
        }

        $aVTypes = array();
        foreach ($_aVTypes as $_aVType) {
            $aVTypes[$_aVType['name']] = $_aVType['type'];
        }

        if (!empty($aVTypes)) {
            $i = 1;
            $strNumOrderedField = -1;
            $aTablesWhere = $aVTplWhere = $aLinkingWhere = $aWhere = array();
            foreach ($aFilters['filter'] as $strName=>$mFilter) {

                if ($aFilters['orderby']==$strName) {
                    $strNumOrderedField = $i;
                }

                if (!isset($aVTypes[$strName])) continue;
                $aTablesWhere[] = db::p("`values` as t?i", $i);

                if ($i>1) $aLinkingWhere[] = db::p("t1.good_id = t?i.good_id", $i);

                $aVTplWhere[] = self::getVTplsId($strName, $aFilters['cat'], $i);

                switch ($aVTypes[$strName]) {
                    case 9:case 10: case 4: case 6: // диапазон
                            $aRange = array();
                            if (isset($mFilter['from']) && intval($mFilter['from'])>0) {
                                    $aRange[] = db::p("t?i.value >= ?i", $i, intval($mFilter['from']));
                            }
                            if (isset($mFilter['to']) && intval($mFilter['to'])>0) {
                                    $aRange[] = db::p("t?i.value <= ?i", $i, intval($mFilter['to']));
                            }

                            if (count($aRange)>0) $aWhere[] = '('.join(' AND ', $aRange).')';
                    break;

                    case 1:
                        if ($mFilter!='') {
                            $aWhere[] = db::p("t?i.value LIKE '?s'", $i, $mFilter);
                        }
                    break;
                    case 7: case 8:
                            if ($mFilter!='') {
                                $aWhere[] = db::p("t?i.value in (?a)", $i, $mFilter);
                            }
                    break;
                }

                $i++;
            }
        }

        if ($strNumOrderedField == -1) {
            $strNumOrderedField = $i;

            $aTablesWhere[] = db::p("`values` as t?i", $i);
            $aLinkingWhere[] = db::p("t1.good_id = t?i.good_id", $i);
            $aVTplWhere[] = self::getVTplsId($aFilters['orderby'], $aFilters['cat'], $i);
        }

        $aNums = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З', 'И', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Э', 'Ю', 'Я');

        $aWhere2 = array();
        if ($aFilters['condition']!='') {
            foreach($aNums as $k=>$strNum) {
                if(isset($aWhere[$k])) {
                    $aWhere2[$strNum] = $aWhere[$k];
                    $aFilters['condition'] = str_replace($strNum, '{'.$strNum.'}', $aFilters['condition']);
                }
            }

            $strWhere = H::tpl($aFilters['condition'], $aWhere2);
        } else {
            $strWhere = join(' AND ', $aWhere);
        }

        $nPage = isset($_GET['page']) ? $_GET['page'] : 1;

        if (isset($aFilters['nav']) && $aFilters['nav'] && ($nPerPage = intval($aFilters['ppage']))>0) {
            $strLimit = db::p("LIMIT ?i, ?i", ($nPage - 1) * $nPerPage, $nPerPage);
        } else {
            $strLimit = '';
        }


        $strSqlCount = "
            SELECT COUNT(DISTINCT(t1.good_id)) as count
            FROM goods as g, ".join(', ', $aTablesWhere)."
            WHERE ".join(' AND ', $aVTplWhere)."
            AND g.cat_id in (".join(', ', $aFilters['cat']).")
            AND ".join(' AND ', $aLinkingWhere)."
            ".((count($aLinkingWhere)>0) ? 'AND' : '')." (".$strWhere.")";
       $aCount = $db->_getAll($strSqlCount);


        if ($aFilters['order']=='RAND()') {
            $strOrder = 'ORDER BY RAND()';
        } else {
            $strOrder = "ORDER BY t".$strNumOrderedField.".value ".$aFilters['order'];
        }

        $strSqlSelect = "
            SELECT DISTINCT(t".$strNumOrderedField.".good_id), g.*
            FROM goods as g, ".join(', ', $aTablesWhere)."
            WHERE ".join(' AND ', $aVTplWhere)."
            AND g.cat_id in (".join(', ', $aFilters['cat']).")
            AND g.id = t1.good_id
            AND ".join(' AND ', $aLinkingWhere)."
            ".((count($aLinkingWhere)>0) ? 'AND' : '')." (".$strWhere.")
            ".$strOrder." ".$strLimit;

        $aGoods = $db->_getAll($strSqlSelect);

        /*$aGoodsId = array();
        foreach ($aResult as $aItem) $aGoodsId[] = $aItem['id'];

		$aGoods = $db->getAll(self::TABLE, db::p("id in (?a)", $aGoodsId));*/

		foreach ($aGoods as &$aGood) {
			$aGood['price'] = self::getGoodValue($aGood['id'], VTYPE_PRICE);
			$aGood['brand'] = self::getGoodValue($aGood['id'], VTYPE_BRAND);
		}

//        usort($aGoods, 'self::sortGoodsByPrice');

		return array(
			'list' => $aGoods,
			'count' => $aCount[0]['count']
		);
	}


    static protected function getVTplsId($strName, $nCat, $i) {
        $db = Registry::get('db');

        $aVTpls = $db->_getAll(
            db::p("SELECT DISTINCT(id) FROM ?n WHERE name = '?s' AND cat_id in (?a)",
            self::TABLE_VALUES_TPL, $strName, $nCat
        ));

        $aVTplsId = array();
        foreach ($aVTpls as $aVTpl) $aVTplsId[] = $aVTpl['id'];

        return db::p("t?i.value_tpl_id in (?a)", $i, $aVTplsId);
    }

    static public function setExistenceForCat ($nId, $nValue) {
        $db = Registry::get('db');

        $db->update(self::TABLE, array(
            'existence' => intval($nValue)
        ), db::p("cat_id = ?i", $nId));
    }


    static protected function sortGoodsByPrice($a, $b) {
        $a = intval($a['price']);
        $b = intval($b['price']);

        if ($a == $b) return 0;
        return ($a > $b) ? -1 : 1;
    }
	
	
	static public function getRandForIndex($nCount=9) {
		$db = Registry::get('db');
		$strSql = db::p("SELECT * FROM ?n WHERE public=1 ORDER BY RAND() LIMIT ?i", self::TABLE, $nCount);
		
		$aGoods = $db->_getAll($strSql);
		
		foreach ($aGoods as &$aGood) {
			$aGood['price'] = self::getGoodValue($aGood['id'], VTYPE_PRICE);
			$aGood['brand'] = self::getGoodValue($aGood['id'], VTYPE_BRAND);
		}
		
		return $aGoods;
	}
        
        /**
         * для админки
         * @param int $nCatId - id категории
         * @param type $field - название поля
         * @param type $fieldVal - значение поля
         */
        static public function _getForSearch($nCatId, $field, $fieldVal) {
            
            if (!empty($fieldVal)) {
                $fieldVal = trim($fieldVal);
                $fieldVal = htmlspecialchars($fieldVal, ENT_QUOTES);
                $where = "`cat_id` = ".intval($nCatId)." AND `".$field."` LIKE '%".$fieldVal."%' ";
            }
            else
                $where = '`cat_id` = '.intval($nCatId).' ';
            
            $where.= 'ORDER BY `priority` DESC';
            $db = Registry::get('db');
            $aGoods = $db->getAll(self::TABLE, $where);
            
            if (!empty($aGoods)) {
                foreach ($aGoods as $kgood => $good) {
                    $aGoods[$kgood]['brand'] = self::getGoodValue($good['id'], VTYPE_BRAND);
                    $aGoods[$kgood]['date'] = strftime("%m/%d/%y %H:%M", intval($good['date']));
                }
            }
            
            return $aGoods;
        }
	
	/**
         * для админки
         * (удаляем добавленный сопутствующий товар)
         * @param (integer) $nGAttId - id удаляемого сопутствующего товара
         * @param integer) $nGId - id товара, которого удаляется сопутствующий товар
         */
        static public function deleteAttGood($nGAttId, $nGId) {
            $aAttGood = self::get($nGId, TRUE);
            $aAttGood = $aAttGood['attendant_goods'];
            if (isset($aAttGood[$nGAttId])) {
                unset($aAttGood[$nGAttId]);
                $db = Registry::get('db');
                $success = $db->update(self::TABLE, array('attendant_goods' => json_encode($aAttGood)), db::p('id = ?i', $nGId));
                
                return $success;
            }
            
            return TRUE;
        }
        
        
        /**
         * для админки
         * (добавляем к товару сопутствующие товары)
         */
        static public function addAttGood($aParams, $nGId) {
            $aParams = json_encode($aParams);
            $db = Registry::get('db');
            
            return $db->update(self::TABLE, array('attendant_goods' => $aParams), db::p('id = ?i', $nGId));
        }
        
        
        /**
         * для админки
         * (вытаскиваем id товаров по введенному пользователем названию)
         */
        static public function _getForAttendant($strReq) {
            $db = Registry::get('db');
            $strSql = db::p("
                SELECT
                    g.`id`, g.`model`, (SELECT `model` FROM ?n WHERE `id` = g.`cat_id` LIMIT 1) AS brand
                FROM
                    ?n AS g
                WHERE
                    `model` LIKE '%?s%'
                ORDER BY 
                    `priority` DESC
            ", self::TABLE_CATEGORIES, self::TABLE, $strReq);
            $aGIds = $db->_getAll($strSql);
			$aGoods = array();
            
            if (!empty($aGIds)) {
                foreach ($aGIds as $kid => $gid)
					$aGoods[$gid['id']] = $gid['model'].' '.$gid['brand'];
            }
            unset($aGIds);
			
            return $aGoods;
        }
        
        
        
        static public function getAttGoods($strGoods, $nLimit = 4) {
            $aAttGoods = json_decode($strGoods, TRUE);
            if (empty($aAttGoods))
                return FALSE;
            
            $aAttGIds = implode(',', array_keys($aAttGoods));
            $db = Registry::get('db');
            $aAttGoods = $db->getAll(self::TABLE, db::p('id IN (?s) LIMIT ?i', $aAttGIds, $nLimit));
            
            if (!empty($aAttGoods)) {
                foreach ($aAttGoods as $kgood => $good) {
                    $aAttGoods[$kgood]['brand'] = self::getGoodValue($good['id'], VTYPE_BRAND);
                    $aAttGoods[$kgood]['maincat'] = self::getCat($good['cat_id'], FALSE, TRUE);
                    $aAttGoods[$kgood]['maincat'] = $aAttGoods[$kgood]['maincat']['title'];
					//$aAttGoods[$kgood]['mpack'] = self::getGoodValue($good['id'], VTYPE_PACK_AMOUNT);
					
					
                    $aAttGoods[$kgood]['code'] = json_encode(array(
                        'id' => $aAttGoods[$kgood]['id'],
						'cover' => $aAttGoods[$kgood]['cover'],
                        'title' => $aAttGoods[$kgood]['maincat'].' '.$aAttGoods[$kgood]['brand'].' '.$aAttGoods[$kgood]['title'],
                        'title_short' => $aAttGoods[$kgood]['title'],
						
                    ));
                }
            }
            
            return $aAttGoods;
        }
        
	/**
	 * для админки
	 *
	 * @param unknown_type $nCatId
	 * @param unknown_type $bAll
	 * @param unknown_type $bFilter
	 * @return unknown
	 */
	static public function _getAll($nCatId, $bAll=true) {
		$db = Registry::get('db');
		
		$aWhere  = array();
		
		if ($bAll) $aWhere[] = 'public = 1';
		$aWhere[] = db::p("cat_id = ?i", $nCatId);
		
		$strWhere = join(' AND ', $aWhere);
		
		
		$strWhere = ($bAll) ? '1' : ' AND public = 1';
		$strWhere .=  db::p(" AND cat_id = ?i", $nCatId);
				
		$strSql = db::p("
			SELECT 
				* 
			FROM
				?n
			WHERE
				".$strWhere."
			ORDER BY priority DESC
		", self::TABLE);		

		$aGoods = $db->_getAll($strSql);
		
		foreach ($aGoods as &$aGood) {
			$aGood['price'] = self::getGoodValue($aGood['id'], VTYPE_PRICE);
                        //$aGood['price_usd'] = self::getGoodValue($aGood['id'], VTYPE_PRICE_USD);
                        //$aGood['price_euro'] = self::getGoodValue($aGood['id'], VTYPE_PRICE_EURO);
			$aGood['brand'] = self::getGoodValue($aGood['id'], VTYPE_BRAND);
		}
		
		return $aGoods;
	}
	
	static public function getGoodValue($nGood, $nValueType) {
		$strSql = db::p("
			SELECT 
				v.value as value
			FROM
				?n as v,
				?n as t
			WHERE
				v.good_id = ?i AND
				v.value_tpl_id = t.id AND
				t.type = ?i
		", self::TABLE_VALUES, self::TABLE_VALUES_TPL, $nGood, $nValueType);
		
		$db = Registry::get('db');
		$aValues = $db->_getAll($strSql);

		return (isset($aValues[0]['value'])) ? $aValues[0]['value'] : 0;
	}
	
	static public function search($strReq, $bAll=true) {
		$db = Registry::get('db');
		
		/*$strSql = db::p("
			SELECT
				DISTINCT g.id, 
				g.*
			FROM
				?n as g,
				?n as v
			WHERE
				g.public = 1 AND
				(
					g.title LIKE '%?s%' OR
					g.model LIKE '%?s%' OR
					g.text LIKE '%?s%' OR
					(
						v.value LIKE '%?s%' AND
						v.good_id = g.id
					)
				)
			ORDER BY g.priority DESC
		", self::TABLE, self::TABLE_VALUES, $strReq, $strReq, $strReq, $strReq);
		
		return $db->_getAll($strSql);*/
                
                $strSql = db::p("SELECT * 
                    FROM ?n 
                    WHERE `public` = 1 AND (
                        `title` LIKE '%?s%' OR
                        `model` LIKE '%?s%'
                    )
                    ORDER BY `priority` DESC"
                , self::TABLE, $strReq, $strReq);
                
                $aGoods = $db->_getAll($strSql);
                if (!empty($aGoods)) {
                    foreach ($aGoods as $kgood => $aGood) {
                        $aGoods[$kgood]['price'] = self::getGoodValue($aGood['id'], VTYPE_PRICE);
                        $aGoods[$kgood]['brand'] = self::getGoodValue($aGood['id'], VTYPE_BRAND);
                    }
                }
                
                return $aGoods;
	}
	
	
	static public function set($aParams, $nCatId, $nId=0) {		
		if (isset($aParams['title'])) {
			$db = Registry::get('db');
			
			$aParams['public'] = (isset($aParams['public'])) ? 1 : 0;
			$aParams['existence'] = (isset($aParams['existence']))? 1 : 0;
			$aParams['cat_id'] = $nCatId;
			$aParams['priority'] = intval($aParams['priority']);
			$aParams['options_list'] = (!isset($aParams['options_list']))? self::getIconsForSave() : self::getIconsForSave($aParams['options_list']);
			$aParams['meta'] = json_encode($aParams['meta']);

			if ($nId > 0) {
				self::uploadAdditFiles($_FILES, $nId);
				return $db->update(self::TABLE, $aParams, db::p('id = ?i', $nId));
			} else {
				$aParams['date'] = time();
				$gId = $db->add(self::TABLE, $aParams);
				if ($gId)
					self::uploadAdditFiles($_FILES, $gId);

				return $gId;
			}
		}
		
		return false;
	}
        
        static public function setAjaxDataTable($aParams, $nCatId, $nId=0) {
            $db = Registry::get('db');
            return $db->update(self::TABLE, $aParams, db::p('id = ?i', $nId));
	}
        
        static public function setAjaxDataTablePrice($aParams, $nId) {
            $db = Registry::get('db');
            return goods::setValues($aParams, $nId);
            //return $db->update(self::TABLE_VALUES, $aParams, db::p('good_id = ?i', $nId));
	}
        
        //загрузка прикрепленных файлов для товара с id = $nGoodId
        static private function uploadAdditFiles($aFiles, $nGoodId) {
            if (empty($aFiles['additFiles']['name']))
                return FALSE;
            set_time_limit(1200);
            
            //разрешенные типы файлов
            $mimeTypes = array(
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', //файлы .xlsx
                'application/vnd.ms-excel', //файлы .xls
                'text/csv', //файлы .csv
                'application/pdf', //файлы.pdf,
                'text/plain', //обычные текстовые файлы
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document', //файлы .docx
                'application/msword' //файлы .doc
                
            );
            //разрешенные расширения файлов
            $aExts = array ('xlsx', 'xls', 'csv', 'pdf', 'txt', 'docx', 'doc');
            
            $db = Registry::get('db');
            foreach ($aFiles['additFiles']['name'] as $kfile => $file) {
                //имя загружаемого файла
                $fName = H::translite($file);
                $fName = str_replace(' ', '-', $fName);
                $fName = preg_replace('/[^-a-zа-я_0-9\.]+/i', '', $fName);
                if (empty($fName))
                    $fName = 'file'.$kfile;
                //расширение загружаемого файла
                $fExt = pathinfo($file, PATHINFO_EXTENSION);
                
                if (
                        array_search($aFiles['additFiles']['type'][$kfile], $mimeTypes) !== FALSE
                        && array_search($fExt, $aExts) !== FALSE
                    ) 
                    {
                        if (!is_dir(GOODFILE_DIR.'/'.$nGoodId))
                            mkdir(GOODFILE_DIR.'/'.$nGoodId, 0777);
                        
                        if (move_uploaded_file($aFiles['additFiles']['tmp_name'][$kfile], GOODFILE_DIR.'/'.$nGoodId.'/'.$fName)) {
                            //заносим имя файла в таблицу если его не существует в БД файл с именем $fName
                            $isCopyFile = $db->getAll(self::TABLE_FILES, "`filename` = '".$fName."' AND good_id = ".$nGoodId);
                            $isCopyFile = (!empty($isCopyFile[0]['filename']))? TRUE : FALSE;
                            if (array_search($fExt, array($aExts[0], $aExts[1], $aExts[2])) !== FALSE && array_search($aFiles['additFiles']['type'][$kfile], array($mimeTypes[0], $mimeTypes[1], $mimeTypes[2])) !== FALSE)
                                $iconType = 'xls';
                            elseif ($fExt == $aExts[3] && $aFiles['additFiles']['type'][$kfile] == $mimeTypes[3])
                                $iconType = 'pdf';
                            elseif ($fExt == $aExts[4] && $aFiles['additFiles']['type'][$kfile] == $mimeTypes[4])
                                $iconType = 'txt';
                            else
                                $iconType = 'doc';
                            if (!$isCopyFile)
                                $db->add(self::TABLE_FILES, array('good_id' => $nGoodId, 'filename' => $fName, 'icon' => $iconType));
                        }
                    }
            }
        }
        
        //смена параметров в БД для прикрепленных файлов
        static public function setAdditFiles($aParams) {
            if (empty($aParams))
                return FALSE;
            
            $successStart = TRUE;
            $db = Registry::get('db');
            foreach ($aParams as $nId => $param) {
                $param['name'] = db::clear($param['name']);
                $success = $db->update(self::TABLE_FILES, $param, db::p('id = ?i', $nId));
                if ($successStart && !$success)
                    $successStart = FALSE;
            }
            
            return $successStart;
        }
	
        //получаем прикрепленные файлы по id товара
        static public function getAdditFiles($nGoodId) {
            $db = Registry::get('db');
            $res = $db->getAll(self::TABLE_FILES, db::p('good_id = ?i', $nGoodId));
            
            if (!empty($res)) {
                $result = array();
                foreach ($res as $file)
                    $result[] = array('id' => $file['id'], 'filename' => $file['filename'], 'name' => $file['name'], 'icon' => $file['icon']);
                
                return $result;
            }
            
            return FALSE;
        }
        
        
        //получаем список файлов в директории товара
        static public function getAdditFilesList($nGoodId) {
            $files = glob(GOODFILE_DIR.'/'.$nGoodId.'/*.{xlsx,xls,csv,pdf,txt,docx,doc}', GLOB_BRACE);
            return (!empty($files))? $files : FALSE;
        }
        
        //удаляем прикрепленный файл
        static public function deleteAdditFile($nFileId, $filename, $nGoodId = 0) {
            $db = Registry::get('db');
            
            if ($nGoodId == 0) {
                $nGoodId = $db->getOne('good_files', 'good_id', db::p('id = ?i', $nFileId));
            }
            
            $filename = db::clear($filename);
            
            return ($db->delete('good_files', db::p('id = ?i', $nFileId)) && unlink(GOODFILE_DIR.'/'.$nGoodId.'/'.$filename));
        }      
        
        //удаляем все прикрепленные файлы у конкретного товара
        static public function deleteAdditFilesAll($nGoodId) {
            $filesPath = self::getAdditFilesList($nGoodId);
            if (!$filesPath)
                return $filesPath;
            foreach ($filesPath as $fpath)
                unlink($fpath);
            rmdir(GOODFILE_DIR.'/'.$nGoodId);
        }
        
        //получаем иконки для товаров (пока тока для кондеров и сплитов)
        static public function getIcons($nIconsId = array()) {
            $db = Registry::get('db');
            $aIcons = $db->getAll(self::TABLE_ICONS, '');
            
            if (!is_array($nIconsId))
                $nIconsId = array();
            
            if (!empty($aIcons)) {
                foreach ($aIcons as $kicon => $icon) {
                    $gIconKey = array_search($icon['id'], $nIconsId);
                    $aIcons[$kicon]['checked'] = ($gIconKey !== FALSE)? 'checked' : '';
                }
            }
            
            return $aIcons;
        }
        
        //получаем массив иконок для сохранения
        static public function getIconsForSave($nIconsId = array()) {
            if (!empty($nIconsId)) {
                foreach ($nIconsId as $kicon => $icon)
                    $nIconsId[$kicon]= intval($icon);
            }
            
            return json_encode($nIconsId);
        }
        
        
	
	static public function getValuesTpl($nCatId) {
		$db = Registry::get('db');
		$aValues = $db->getAll(self::TABLE_VALUES_TPL,  db::p('cat_id = ?i ORDER BY `priority` DESC', $nCatId));
		
		foreach ($aValues as &$aValue) {
			if ($aValue['type']==8) { 
				$aValue['values'] = explode(';', $aValue['values']);
				foreach ($aValue['values'] as &$v) {
					$v = trim($v);
				}
			}
		}
		
		return $aValues;
	}
	
	static public function getValuesTplSimple($nCatId) {
		$db = Registry::get('db');
		return $db->getAll(self::TABLE_VALUES_TPL,  db::p('cat_id = ?i ORDER BY `priority` DESC', $nCatId));
	}
	
	
	static public function addValueTpl($aParams, $nCatId) {
		$db = Registry::get('db');
		$aParams['cat_id'] = $nCatId;
		$aParams['filtered'] = (isset($aParams['filtered']) && $aParams['filtered']) ? 1 : 0;
		return $db->add(self::TABLE_VALUES_TPL, $aParams);
	}
	
	static public function setValueTpl($aParams, $nId) {
		$db = Registry::get('db');
		$aParams['cat_id'] = $nCatId;
		return $db->add(self::TABLE_VALUES_TPL, $aParams);
	}
	
	
	static public function getValueTpl($nId) {
		$db = Registry::get('db');
		return $db->getRowById(self::TABLE_VALUES_TPL, $nId);
	}
	
	static public function delValueTpl($nId) {
		$db = Registry::get('db');
		$db->delete(self::TABLE_VALUES_TPL, db::p('id = ?i', $nId));
		$db->delete(self::TABLE_VALUES, db::p('value_tpl_id = ?i', $nId));
	}
	
	static public function setValuesTpl($aValues, $nCatId) {
		$db = Registry::get('db');

		foreach ($aValues as $id=>$a) {
			$db->update(self::TABLE_VALUES_TPL, array(
				'name' => $a['name'],
				'units' => $a['units'],
				'values' => (isset($a['values'])) ? $a['values'] : '',
				'filtered' => (isset($a['filtered']) && $a['filtered']) ? 1 : 0,
                                'priority' => $a['priority']
			), db::p('id = ?i', $id));
		}		
	}
	
	
	static public function get($nId, $for_admin = FALSE) {
            
            
		$db = Registry::get('db');
		$aGood = $db->getRow(self::TABLE, db::p('id = ?i', $nId));
		$aGood['meta'] = json_decode($aGood['meta'], TRUE);
		$aGood['brand'] = self::getGoodValue($aGood['id'], VTYPE_BRAND);
		$aGood['price'] = self::getGoodValue($aGood['id'], VTYPE_PRICE);
		$aGood['icons'] = self::getIcons(json_decode($aGood['options_list'], TRUE));
		$aGood['additFiles'] = self::getAdditFiles($nId);
                $aGood['attendant_goods'] = (!$for_admin)? self::getAttGoods($aGood['attendant_goods']) : json_decode($aGood['attendant_goods'], TRUE);
		if (!$aGood['price'])
			$aGood['price'] = 0;
		
		return $aGood;
	}
	
	
	static public function del($nId) {
		$db = Registry::get('db');
                
                images::delAll(DR.'public/img/upload/', $nId, 'goods');
                self::deleteAdditFilesAll($nId);
                
		return $db->delete(self::TABLE, db::p('id = ?i', $nId));
	}
	
	
	static public function getValues($nId, $bPrice=true, $for_convvalutes = FALSE, $for_admin = FALSE) {
		$aGood = self::get($nId);
		$aValuesTpl = self::getValuesTpl($aGood['cat_id']);
		
		$aValues = array();
		foreach ($aValuesTpl as $k=>$v) {
			if (/*$v['type']==6*/in_array($v['type'], array(VTYPE_PRICE, VTYPE_PRICE_USD, VTYPE_PRICE_EURO)) && !$bPrice) {
			} else {
				$aValue = self::getValue($v['id'], $aGood['id']);
				if ($for_convvalutes && in_array($v['type'], array(VTYPE_PRICE, VTYPE_PRICE_USD, VTYPE_PRICE_EURO))) {
                                    $aValues[$v['id']] = (isset($aValue['value'])) ? $aValue['value'] : '';
                                }
                                elseif ((!$for_convvalutes && !empty($aValue['value'])) || $for_admin) {
                                    $aValues[] = array(
                                            'name' => $v['name'],
                                            'value' => (isset($aValue['value'])) ? $aValue['value'] : '',
                                            'type' => $v['type'],
                                            'tpl_id' => $v['id'],
                                            'values' => $v['values'],
                                            'units' => $v['units'],
                                            'filtered' => $v['filtered'],
                                    );
                                }
			}
		}

		return $aValues;
	}
	
	
	static public function getFilters($nCat) {
		$db = Registry::get('db');
		$aFilters = $db->getAll(self::TABLE_VALUES_TPL, db::p('cat_id = ?i AND filtered = 1 ORDER BY `priority` DESC', $nCat));
                
		foreach ($aFilters as &$aFilter) {
			if ($aFilter['type']==VTYPE_LIST) {
				$aFilter['values'] = explode(';', $aFilter['values']);
				
				foreach ($aFilter['values'] as &$v) $v = trim($v);
			}
			
			if ($aFilter['type']==VTYPE_BRAND) {
				$aFilter['values'] = self::getAllBrands($nCat);
			}
		}
                
		return $aFilters;
	}

	static public function getFilterByName($strName) {
		$db = Registry::get('db');
		$aFilter = $db->getRow(self::TABLE_VALUES_TPL, db::p("name = '?s'", $strName));

        if ($aFilter['type']==VTYPE_LIST) {
            $aFilter['values'] = explode(';', $aFilter['values']);

            foreach ($aFilter['values'] as &$v) $v = trim($v);
        }

        if ($aFilter['type']==VTYPE_BRAND) {
            $aFilter['values'] = self::getAllBrands2();
        }

		return $aFilter;
	}


	
	
	static public function getAllBrands($nCat) {
		$db = Registry::get('db');
		$strSql = db::p("
			SELECT DISTINCT(v.value) as value
			FROM 
				?n as v, 
				?n as t,
                                ?n as s
			WHERE 
				v.value_tpl_id = t.id AND
				t.type = 7 AND
                                v.good_id IN (SELECT id FROM goods WHERE goods.public = 1 AND goods.cat_id = t.cat_id )
                AND t.cat_id = ?i
		", self::TABLE_VALUES, self::TABLE_VALUES_TPL, self::TABLE_STRUCTURE, $nCat);
                
		$aValues = $db->_getAll($strSql);
		$aResult = array();
		
		foreach ($aValues as $key => &$aValue) {
			if (strtolower($aValue['value'] == 'Филипс') || strtolower($aValue['value'] == 'Интел') || strtolower($aValue['value'] == 'Panasonic') || strtolower($aValue['value'] == 'Hitachi'))
                            unset($aValues[$key]);
                        else
                            $aValue = $aValue['value'];
		}
		
		return array_values($aValues);
	}


	static public function getAllBrands2() {
		$db = Registry::get('db');
		$strSql = db::p("
			SELECT DISTINCT(v.value) as value
			FROM
				?n as v,
				?n as t
			WHERE
				v.value_tpl_id = t.id AND
				t.type = 7
		", self::TABLE_VALUES, self::TABLE_VALUES_TPL);

		$aValues = $db->_getAll($strSql);
		$aResult = array();

		foreach ($aValues as $key => &$aValue) {
			if (strtolower($aValue['value'] == 'Филипс') || strtolower($aValue['value'] == 'Интел') || strtolower($aValue['value'] == 'Panasonic') || strtolower($aValue['value'] == 'Hitachi'))
                            unset($aValues[$key]);
                        else
                            $aValue = $aValue['value'];
		}

		return array_values($aValues);
	}
		
	
        static private function convertPriceValute($aPrices) {
            $exrate = settings::getExchangeValutes();
            
            if ($aPrices[1] > 0) {
                $aPrices[0] = $exrate[0] * $aPrices[1];
                $aPrices[2] = $aPrices[0] / $exrate[1];
            }
            elseif($aPrices[2] > 0) {
                $aPrices[0] = $exrate[1] * $aPrices[2];
                $aPrices[1] = $aPrices[0] / $exrate[0];
            }
//            else {
//                $aPrices[0] = ($aPrices[0] > 0)? $aPrices[0] : 0;
//                $aPrices[1] = $aPrices[0] / $exrate[0];
//                $aPrices[2] = $aPrices[0] / $exrate[1];
//            }
            
            return $aPrices;
        }
        
	static public function setValues($aValues, $nGood) {
		$db = Registry::get('db');
		$aTypes = self::getValueTypesForFilter($aValues);
                
                $priceForValutes = array();
                foreach ($aValues as $nValueTpl => $strValue) {
                    /*if (in_array($aTypes[$nValueTpl], array(VTYPE_PRICE, VTYPE_PRICE_USD, VTYPE_PRICE_EURO))) {
                        $strValue = floatval($strValue);
                        $priceForValutes[] = $strValue;
                    }*/
                    switch ($aTypes[$nValueTpl]) {
                        case VTYPE_PRICE:
                            $strValue = floatval($strValue);
                            $priceForValutes[0] = $strValue;
                            break;
                        case VTYPE_PRICE_USD:
                            $strValue = floatval($strValue);
                            $priceForValutes[1] = $strValue;
                            break;
                        case VTYPE_PRICE_EURO:
                            $strValue = floatval($strValue);
                            $priceForValutes[2] = $strValue;
                    }
                }
                ksort($priceForValutes);
                $priceForValutes = self::convertPriceValute($priceForValutes);

		foreach ($aValues as $nValueTpl => $strValue) {
                        if ($aTypes[$nValueTpl] == VTYPE_PRICE)
                            $strValue = $priceForValutes[0];
                        elseif ($aTypes[$nValueTpl] == VTYPE_PRICE_USD)
                            $strValue = $priceForValutes[1];
                        elseif ($aTypes[$nValueTpl] == VTYPE_PRICE_EURO)
                            $strValue = $priceForValutes[2];
                    
			if ($aOldValue = self::getValue($nValueTpl, $nGood)) {
				if (in_array($aTypes[$nValueTpl], array(VTYPE_NUMBER, VTYPE_PRICE))) $strValue = floatval($strValue);
				
				$db->update(self::TABLE_VALUES, array(
					'value' => $strValue
				), db::p('id = ?i', $aOldValue['id']));
			} else {
				if (in_array($aTypes[$nValueTpl], array(VTYPE_NUMBER, VTYPE_PRICE))) $strValue = floatval($strValue);
				
				$db->add(self::TABLE_VALUES, array(
					'value_tpl_id' => $nValueTpl,
					'good_id' => $nGood,
					'value' => $strValue
				));
			}
		}
	}
	
	
	static public function getValue($nValueTpl, $nGood) {
		$db = Registry::get('db');
		return $db->getRow(self::TABLE_VALUES, db::p('value_tpl_id = ?i AND good_id = ?i', $nValueTpl, $nGood));
	}
	
	
	static public function getAllCats($bAll=true) {
		$strWherePublic = ($bAll) ? '' : 'visible = 1';
		
		$tree = new Tree();
		$aCats = $tree->get(0, 2, 'id', 'DESC', $strWherePublic);

        foreach($aCats as &$aCat) {
            $aCat['count_goods'] = ($aCat['level']==2) ? self::getCountGoods($aCat['id']) : '';
        }

        return $aCats;
		$db = Registry::get('db');

		return $db->getAll(self::TABLE_CATEGORIES, $strWherePublic);
	}


    static public function getCountGoods($nCatId) {
        $db = Registry::get('db');

        $aResult = $db->_getAll(db::p("SELECT COUNT(id) as count FROM ?n WHERE cat_id = ?i", self::TABLE, $nCatId));
        return $aResult[0]['count'];
    }


	
	static public function getCatsMenu($bAll=true) {
		$strWherePublic = ($bAll) ? '' : 'visible = 1';
		
		$tree = new Tree();
		$aElements = $tree->get2(0, 2, 'id', 'ASC', $strWherePublic);

		return $aElements;
	}	
	
	
	static public function setCat($aParams, $nId=0) {	
		if (isset($aParams['title'])) {
			$tree = new Tree();
		
			$db = Registry::get('db');
			
			$aParams['visible'] = (isset($aParams['visible'])) ? 1 : 0;
			$aParams['meta'] = json_encode($aParams['meta']);

			if ($nId > 0) {
				$aParams['id'] = $nId;
				return $tree->save($aParams);				
				//				return $db->update(self::TABLE_CATEGORIES, $aParams, db::p('id = ?i', $nId));
			} else {				
				$aParams['parent_id'] = (isset($aParams['parent_id'])) ? $aParams['parent_id'] : 0;
				return $tree->add($aParams);
//				return $db->add(self::TABLE_CATEGORIES, $aParams);
			}
		}
		
		return false;
	}	
		
	static public function getBreadCrumbs($nId) {
		$tree = new Tree();
		return $tree->showBreadCrumbs($tree->getElementInfo($nId));
	}
	static public function getCat($nId, $bAll = TRUE) {
		$tree = new Tree();
		$aCat = $tree->getElementInfo($nId, $bAll);
		if (!empty($aCat))
			$aCat['meta'] = json_decode($aCat['meta'], TRUE);
		
		return $aCat;
	}
	
	
	static public function delCat($nId) {
//		$db = Registry::get('db');
		$tree = new Tree();
		$tree->delete($nId);
//		return $db->delete(self::TABLE_CATEGORIES, db::p('id = ?i', $nId));
	}
	
	
	static public function setCover($nId, $nImgId) {
		if ($nId > 0 && $nImgId > 0) {
			$aImage = images::get($nImgId);
			
			$db = Registry::get('db');
			return $db->update(self::TABLE, array(
				'cover' => $aImage['file_name']
			), db::p('id = ?i', $nId));
		}
	}
	
	
	static public function getLastGoods($nLimit) {
		$db = Registry::get('db');
		return $db->getAll(self::TABLE, db::p("public=1 ORDER BY date DESC LIMIT ?i", $nLimit));
	}
}