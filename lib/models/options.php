<?php

class options {
	const TABLE = 'options';
	
	static public function getAll($strCategory='general') {
		$db = Registry::get('db');
		return $db->getAll(self::TABLE, db::p('category = "?s" ORDER BY `order`', $strCategory));
	}
	
	
	static public function set($aOptions) {		
		if (is_array($aOptions)) {
			$db = Registry::get('db');
			
			foreach ($aOptions as $nId=>$strValue) {
				$db->update(self::TABLE, array(
					'value' => $strValue
				), db::p('id = ?i', $nId));
			}
			
			return true;
		}
		
		return false;
	}	
	
	
	static public function get($nId) {
		$db = Registry::get('db');
		return $db->getOne(self::TABLE, 'value', db::p('id = ?i', $nId));
	}
	
	
	static public function getAllOptions($strCategory='all') {
		if ($strCategory=='all') {
			$strWhere = '';
		} else {
			$strWhere = db::p('category = "?s"', $strCategory);
		}
		
		$db = Registry::get('db');
		$aRows = $db->getAll(self::TABLE, $strWhere);
		
		$aOptions = array();
		
		foreach ($aRows as $aRow) {
			$aOptions[$aRow['id']] = $aRow['value'];
		}		
		
		return $aOptions;
	}
}