<?php

class pages {
	const TABLE = 'pages';
	
	static public function getAll() {
		$db = Registry::get('db');
		
		$aPages = $db->getAll(self::TABLE);
		if (!empty($aPages)) {
			foreach ($aPages as $kpage => $page)
				$aPages[$kpage]['meta'] = json_decode($page['meta'], TRUE);
		}
		
		return $aPages;
	}
	
	
	static public function set($aParams, $nId=0) {		
            if (isset($aParams['title'])) {
                $db = Registry::get('db');
				
				$aParams['meta'] = json_encode($aParams['meta']);
                if ($nId > 0) {
                    return $db->update(self::TABLE, $aParams, db::p('id = ?i', $nId));
                } else {
                    $artId = $db->add(self::TABLE, $aParams);
                    return $artId;
                }
            }

            return false;
	}	
	
	
	static public function get($nId) {
		$db = Registry::get('db');
                
                $aArticle = $db->getRow(self::TABLE, db::p('id = ?i', $nId));
				if (isset($aArticle['meta']))
					$aArticle['meta'] = json_decode($aArticle['meta'], TRUE);
		
                return $aArticle;
	}
        
        static public function getByTitle($title) {
            $db = Registry::get('db');
            
            $aArticle = $db->getRow(self::TABLE, db::p("title LIKE '?s'", $title));
			if (isset($aArticle['meta']))
				$aArticle['meta'] = json_decode($aArticle['meta'], TRUE);
            
            return $aArticle;
        }
	
	static public function del($nId) {
		$db = Registry::get('db');
                
		return $db->delete(self::TABLE, db::p('id = ?i', $nId));
	}
}