<?php

class cart {
	const TABLE_CART = 'carts';
	const TABLE_ORDER = 'orders';
	
	protected $cookie_id_name = 'prs_cart_id';
	protected $expiration_deys = 7; // срок жизни корзины в днях
	protected $db;
	protected $id;
	
	protected $_errors_lib = array(
		1 => 'Не определен ID корзины'
	);
	
	public function __construct() {
		$this->db = Registry::get('db');
		$this->id = $this->getId();
	}
	
	
	public function getId() {
		if (!isset($_COOKIE[$this->cookie_id_name])) {
			$strId = md5(time().rand(1111, 9999));
			setcookie($this->cookie_id_name, $strId, time()+60*60*240, "/", '.'.$_SERVER['SERVER_NAME']);	
			return $strId;
		} else return $_COOKIE[$this->cookie_id_name];
	}
	
	
	/** Добавление в корзину
	 *
	 * @param int $nGoodId - ID товара
	 * @param int $nAmount - Количество */
	public function add($nGoodId, $nAmount) {
		$this->cleanOld();
		
		if ($this->id) {
			
			if ($aItem = $this->db->getRow(self::TABLE_CART, db::p("good_id = ?i AND cart_id = '?s'", $nGoodId, $this->id))) {
				return $this->db->update(self::TABLE_CART, array(
					'amount' => $nAmount + $aItem['amount'],
					'date_update' => time()
				), db::p("id = ?i", $aItem['id']));
			} else {
				return $this->db->add(self::TABLE_CART, array(
					'good_id' => $nGoodId,
					'amount' => $nAmount,
					'cart_id' => $this->id,
					'date_add' => time(),
					'date_update' => time()
				));
			}
		}
	}
	
	
	public function get() {
		if ($this->id) {
			return $this->db->getAll(self::TABLE_CART, db::p("cart_id = '?s'", $this->id));
		}
	}
	
	public function getCart() {
		if ($this->id) {
			$aItems = $this->db->getAll(self::TABLE_CART, db::p("cart_id = '?s'", $this->id));
			foreach ($aItems as $k=>$aItem) {
				$aItems[$k]['good'] = goods::get($aItem['good_id']);
			}
			
			return $aItems;
		}
	}
	
	
	public function update($aParams) {
		if (is_array($aParams) && count($aParams)>0 && $this->id) {
			foreach ($aParams as $id=>$amount) {
				$this->db->update(self::TABLE_CART, array(
					'amount' => intval($amount),
					'date_update' => time()
				), db::p("id = ?i", $id));
			}
			
			return true;
		}		
	}
	
	
	public function delete($nId) {
		return $this->db->delete(self::TABLE_CART, db::p("id = ?i", $nId));
	}
	
	
	public function order($aUserInfo) {
		if ($this->id) {
			$aItems = $this->get();

            $tplGood = '                                                                            <tr>
                <td valign="top" style="padding-top: 10px;">
                    <b style="margin: 0;">{title}</b><br><small>{model}</small></td>
                    <td>{price} руб.</td>
                    <td>{amount}</td>
                    <td>{summ} руб.</td>
                </tr>
            ';

            $tplMessage = '
                <html>
                    <head>
                        <title>Новый заказ</title>
                    </head>
                    <body>
                        <h4>Новый заказ</h4>
                        Имя: <b>{name}</b><br>
                        Телефон: <b>{phone}</b><br>
                        Адрес: <b>{phone}</b><br>
                        Комментарий: <b>{comment}</b><br><br>
                        <table width="100%">
                        <tbody><tr>
                        <th align="left">Наименование</th>
                        <th align="left">Цена</th>
                        <th align="left">Кол-во</th>
                        <th align="left">Сумма</th>
                        <td></td>
                        </tr>

                        {goods}

                        <tr><td style="height: 30px;" colspan="5"></td></tr>

                        <tr>
                        <td align="left" colspan="2"><b>Всего:</b></td>
                        <td>{amount}</td>
                        <td>{summ} руб.</td>
                        <td></td>
                        </tr>

                        <tr><td style="height: 30px;" colspan="5"></td></tr>

                        </tbody></table>

                    </body>
                </html>
            ';
//            rostovps@mail.ru
            $aUserInfo['amount'] = 0;
            $aUserInfo['summ'] = 0;
            $aUserInfo['goods'] = '';

            foreach($aItems as $aItem) {
                $aGood = goods::get($aItem['good_id']);
                $aGood['amount'] = $aItem['amount'];
                $aGood['summ'] = $aItem['amount'] * $aGood['price'];

                $aUserInfo['summ'] += $aGood['summ'];
                $aUserInfo['amount'] += $aItem['amount'];

                $aUserInfo['goods'] .= H::tpl($tplGood, $aGood);
            }

            $aDopEmails = explode(',', options::get(26));

            H::mail('Сайт '.options::get(1), options::get(2), options::get(1), 'Новый заказ', H::tpl($tplMessage, $aUserInfo), $aDopEmails);
			
			$nOrder = $this->db->add(self::TABLE_ORDER, array(
				'user_name' => $aUserInfo['name'],
				'user_lastname' => $aUserInfo['lastname'],
				'user_phone' => $aUserInfo['phone'],
				'user_address' => $aUserInfo['address'],
				'user_comment' => $aUserInfo['comment'],
				'goods' => serialize($this->get()),
				'status' => 0,
				'date_add' => time(),
				'date_processed' => 0
			));
			
			// удаляем корзину
			$this->deleteAll();
			
			return $nOrder;
		}
	}
	
	
	public function getOrders() {
		$aOrders = $this->db->getAll(self::TABLE_ORDER, '1 ORDER BY status ASC, date_add DESC');
		foreach ($aOrders as &$aOrder) {
			$aGoods = unserialize($aOrder['goods']);
			
			$aOrder['goods'] = array();
			$nSumm = 0;
			
			foreach ($aGoods as $nItem) {
				$aGood = goods::get($nItem['good_id']);
				$aOrder['goods'][] = $aGood;
				$nSumm += $aGood['price'] * $nItem['amount'];
			}
			$aOrder['summ'] = $nSumm;
		}
		
		return $aOrders;
	}
	
	
	public function setOrder($aParams, $nId) {
		return $this->db->update(self::TABLE_ORDER, $aParams, db::p("id = ?i", $nId));
	}
	
	
	public function getOrder($nId, $secretkey = FALSE) {
		if (!$secretkey)
                    $aOrder = $this->db->getRowById(self::TABLE_ORDER, $nId);
                else {
                    $where = "MD5(CONCAT(`id`,'".SECRET_KEY."')) = '".$nId."'";
                    $aOrder = $this->db->getRow(self::TABLE_ORDER, $where);
                }
		
		$aGoods = unserialize($aOrder['goods']);
                if (!$aGoods)
                    return FALSE;
			
		$aOrder['goods'] = array();
		$aOrder['summ'] = 0;
		foreach ($aGoods as $nItem) {
			$aGood = goods::get($nItem['good_id']);
			$aGood['amount'] = $nItem['amount'];
//			$aGood['values'] = goods::getValues($nItem['good_id']);
			$aOrder['summ'] += $aGood['amount'] * $aGood['price'];
			$aOrder['goods'][] = $aGood;
		}

		return $aOrder;
	}
	
	
	public function deleteOrder($nId) {
		return $this->db->delete(self::TABLE_ORDER, db::p("id = ?i", $nId));
	}
	
	
	public function deleteAll() {
		if ($this->id) {
			return $this->db->delete(self::TABLE_CART, db::p("cart_id = '?s'", $this->id));
		}
	}
	
	
	public function cleanOld() {
		return $this->db->delete(self::TABLE_CART, db::p("date_update < ?i", time()-(60*60*24*$this->expiration_deys)));
	}
	
	
	public function getInfo() {
		if ($this->id) {
			$aItems = $this->get();

			$result['amount'] = count($aItems);
			$result['summ'] = 0;
			if (count($aItems) > 0) {
				foreach ($aItems as $aItem) {
					$aGood = goods::get($aItem['good_id']);				
					$result['summ'] += $aItem['amount'] * $aGood['price'];
				}
			}
			$result['amount'] = $result['amount'].' '.H::get_sign($result['amount'], 'продукт', 'продукта', 'продуктов');
			return $result;
		}
	}
}

?>