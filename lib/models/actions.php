<?php

class actions {
	const TABLE = 'actions';
	
	public static $upload_folder = 'public/img/actions/';
	
	static public function getAll($bAll = TRUE, $nLimit = 0) {
		$db = Registry::get('db');
		
		$strLimit = ($nLimit > 0)? db::p(' ORDER BY date DESC LIMIT ?i', $nLimit) : ' ORDER BY date DESC';
		$strWhere = (!$bAll)? 'public = 1' : '1';
		return $db->getAll(self::TABLE, $strWhere.$strLimit);
	}
	
	static public function set($aParams, $nId = 0) {
		if (!empty($aParams['title'])) {
			$db = Registry::get('db');
			
			$aParams['public'] = (isset($aParams['public'])) ? 1 : 0;
			$aParams['meta'] = json_encode($aParams['meta']);
			if (isset($_FILES['image']) && $_FILES['image']['name']!='') {
				include DR.'lib/wideimage/WideImage.php';
		
				$strImageName = H::translite($_FILES['image']['name']);
				$strImageName = str_replace(' ', '_', $strImageName);
				
				$photoBase = WideImage::load($_FILES['image']['tmp_name']);
				$photoBase->resize(759, NULL, 'inside', 'down')->saveToFile(DR.self::$upload_folder.$strImageName);
				$photoBase->resize(195, NULL)->saveToFile(DR.self::$upload_folder.'preview/'.$strImageName);
				
				$aParams['image'] = $strImageName;
			}
		}
		if (isset($aParams['date'])){
                        $aDate = explode('-', $aParams['date']);
                        $nDate = mktime(0,0,0,$aDate[1],$aDate[0],$aDate[2]);
                }else{
                        $nDate = time();
                }
		if ($nId > 0) {
                        $aParams['date'] = $nDate;
			return $db->update(self::TABLE, $aParams, db::p('id = ?i', $nId));
		}
		else {
                        
			$aParams['date'] = $nDate;
			return $db->add(self::TABLE, $aParams);
		}
		
		return FALSE;
	}
	
	static public function get($nId) {
		$db = Registry::get('db');
		
		$aAction = $db->getRow(self::TABLE, db::p('id = ?i', $nId));
		if (!empty($aAction))
			$aAction['meta'] = json_decode($aAction['meta'], TRUE);
		
		return $aAction;
	}
	
	static public function del($nId) {
		$db = Registry::get('db');
		$aAction = self::get($nId);
		
		if (is_file(DR.self::$upload_folder.$aAction['image']))
			unlink(DR.self::$upload_folder.$aAction['image']);
		if (is_file(DR.self::$upload_folder.'preview/'.$aAction['image']))
			unlink(DR.self::$upload_folder.'preview/'.$aAction['image']);
		clearstatcache();
		
		return $db->delete(self::TABLE, db::p('id = ?i', $nId));
	}
}