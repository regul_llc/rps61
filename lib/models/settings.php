<?php

class settings {
    const TABLE = 'valutes';
    
    static public function getAllValutes() {
        $db = Registry::get('db');
        
        return $db->getAll(self::TABLE, 'id != 3');
    }
    
    // получаем курс валют
    static public function getExchangeValutes() {
        $db = Registry::get('db');
        
        $valutesTmp = self::getAllValutes();
        $resValutes = array();
        if (!empty($valutesTmp)) {
            foreach ($valutesTmp as $valute)
                $resValutes[] = $valute['exrate'];
        }
        
        return $resValutes;
    }
    
    static public function setValutes($aValutes) {
        if (is_array($aValutes)) {
            $db = Registry::get('db');
            
            foreach ($aValutes as $kvalute => $valute) {
                if (!preg_match('/^[\d]+((\.|,)[\d]+)?$/', $valute)) {
                    return FALSE;
                    break;
                }
                $valute = trim($valute);
                $valute = str_replace(',', '.', $valute);
                $valute = (float)$valute;
                if ($valute == 0)
                    $valute = 1;
                $success = $db->update(self::TABLE, array('exrate' => $valute, 'date_upd' => time()), db::p('id = ?i', $kvalute));
                if (!$success) {
                    return FALSE;
                    break;
                }
            }
            
            return TRUE;
        }
        else
            return FALSE;
    }
}