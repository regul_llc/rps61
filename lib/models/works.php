<?php

class works {
	const TABLE = 'works';
	const TABLE_CATEGORIES = 'works_categories';
	
	
	static public function getAll($nCatId, $bAll=true) {
		$db = Registry::get('db');
		$strWherePublic = ($bAll) ? '' : 'AND public = 1';
		return $db->getAll(self::TABLE, db::p("cat_id = ?i ?s", $nCatId, $strWherePublic));
	}
	
	
	static public function set($aParams, $nCatId, $nId=0) {		
		if (isset($aParams['title'])) {
			$db = Registry::get('db');
			
			$aParams['public'] = (isset($aParams['public'])) ? 1 : 0;
			$aParams['cat_id'] = $nCatId;

			if ($nId > 0) {
				return $db->update(self::TABLE, $aParams, db::p('id = ?i', $nId));
			} else {
				$aParams['date'] = time();
				return $db->add(self::TABLE, $aParams);
			}
		}
		
		return false;
	}	
	
	
	static public function get($nId) {
		$db = Registry::get('db');
		return $db->getRow(self::TABLE, db::p('id = ?i', $nId));
	}
	
	
	static public function del($nId) {
		$db = Registry::get('db');
		return $db->delete(self::TABLE, db::p('id = ?i', $nId));
	}
	
	
	static public function getAllCats($bAll=true, $nLimit=0) {
		$db = Registry::get('db');
		
		$strWherePublic = ($bAll) ? '1' : 'public = 1';
		$strLimit = ($nLimit==0) ? '' : db::p('LIMIT ?i', $nLimit);

		$aCats = $db->getAll(self::TABLE_CATEGORIES, $strWherePublic.' '.$strLimit);

		foreach ($aCats as &$aCat) {
			$aCat['cover'] = self::getCoverCat($aCat['id']);
		}
		
		return $aCats;
	}	
	
	
	static protected function getCoverCat($nCat) {
		$db = Registry::get('db');
		$aItem = $db->getRow(self::TABLE, db::p("cat_id = ?i AND cover <> ''", $nCat));
		return (isset($aItem['cover'])) ? $aItem['cover'] : '';
	}
	
	
	static public function setCat($aParams, $nId=0) {		
		if (isset($aParams['title'])) {
			$db = Registry::get('db');
			
			$aParams['public'] = (isset($aParams['public'])) ? 1 : 0;

			if ($nId > 0) {
				return $db->update(self::TABLE_CATEGORIES, $aParams, db::p('id = ?i', $nId));
			} else {
				return $db->add(self::TABLE_CATEGORIES, $aParams);
			}
		}
		
		return false;
	}	
		
	
	static public function getCat($nId) {
		$db = Registry::get('db');
		return $db->getRow(self::TABLE_CATEGORIES, db::p('id = ?i', $nId));
	}
	
	
	static public function delCat($nId) {
		$db = Registry::get('db');
		return $db->delete(self::TABLE_CATEGORIES, db::p('id = ?i', $nId));
	}
	
	
	static public function setCover($nId, $nImgId) {
		if ($nId > 0 && $nImgId > 0) {
			$aImage = images::get($nImgId);
			
			$db = Registry::get('db');
			return $db->update(self::TABLE, array(
				'cover' => $aImage['file_name']
			), db::p('id = ?i', $nId));
		}
	}
	
	
	static public function getLastWorks($nLimit) {
		$db = Registry::get('db');
		return $db->getAll(self::TABLE, db::p("public=1 ORDER BY date DESC LIMIT ?i", $nLimit));
	}
}